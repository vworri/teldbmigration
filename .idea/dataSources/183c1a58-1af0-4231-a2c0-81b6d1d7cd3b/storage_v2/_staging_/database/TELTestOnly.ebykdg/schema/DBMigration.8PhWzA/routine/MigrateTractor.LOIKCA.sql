

CREATE PROCEDURE DBMigration.MigrateTractor
AS BEGIN
  DELETE FROM DBMigration.TractorCleanUpException
  create table #TractorCleanUp
  (
    TractorVIN         varchar(17),
    TMake              VARCHAR(75),
    TModel             VARCHAR(75),
    TractorYear        int,
    Mileage            int,
    CreateDt           DATETIMEOFFSET,
    UpdateDt           DATETIMEOFFSET,
    TractorMakeModelID INTEGER,
    UpdatedBy          VARCHAR(75),
    CreatedBy          VARCHAR(75),
    UpID               INTEGER,
    CreID              INTEGER
  )


  create table #TractorCleanUpException
  (
    TractorVIN         char(20),
    TMake              VARCHAR(75),
    TModel             VARCHAR(75),
    TractorYear        int,
    Mileage            int,
    CreateDt           DATETIMEOFFSET,
    UpdateDt           DATETIMEOFFSET,
    TractorMakeModelID INTEGER,
    UpdatedBy          VARCHAR(75),
    CreateedBy         VARCHAR(75),
    ExecptionCode      VARCHAR(60)
  )


  INSERT INTO #TractorCleanUp (
    TractorVIN,
    tMake,
    tModel
  )
    SELECT
      DISTINCT
      (left(cast(lw.[Serial Number] as varchar(50)), 17)),
      cast(lw.Make as varchar(50)),
      cast(lw.Model as varchar(50))
    FROM TELCustom.dbo.Contracts c
      LEFT JOIN DBMigration.LeaseWorksAssetInfo lw ON c.ContractNumber = lw.[Contract Number]


  UPDATE #TractorCleanUp
  SET UpdatedBy = t.UpdatedBy, UpdateDt = t.UpdateDate, CreatedBy = t.CreatedBy, CreateDt = t.CreateDate
  FROM TELCustom.dbo.Tractors t
  WHERE TractorVIN = VIN

  INSERT INTO #TractorCleanUpExCeption (
    TractorVIN,
    TMake,
    TModel,
    UpdatedBy,
    UpdateDt,
    CreateDt,
    CreateedBy,
    ExecptionCode
  )
    SELECT
      LTRIM(RTRIM(TractorVIN)),
      tMake,
      tModel,
      UpdatedBy,
      TODATETIMEOFFSET(UpdateDt, '-04:00'),
      TODATETIMEOFFSET(CreateDt, '-04:00'),
      CreatedBy,
      'INCOMPLETE VIN NUMBER'
    FROM #TractorCleanUp
    WHERE LEN(LTRIM(RTRIM(TractorVIN))) < 17 or TractorVIN IS NULL
    ORDER BY TractorVIN

  delete from #TractorCleanUp
  where TractorVIN is null


  UPDATE #TractorCleanUp
  SET TractorYear = vy.TractorYear
  from TELCustom.dbo.VinYear vy
  WHERE SUBSTRING(TractorVIN, 10, 1) = VINCharacter


  update #TractorCleanUp
  set TractorMakeModelID = tmm.TractorMakeModelID
  from dbo.TractorMakeModel TMM
  WHERE
    CHARINDEX(SUBSTRING(REPLACE(TModel, 'new ', ''), 1, 2), TMM.Model) > 0
    AND TMM.Make = TMake


  update #TractorCleanUp
  set Mileage =
  CAST(M.LASTRDING
       AS INTEGER)
  FROM TFW.dbo.UNITS U
    JOIN TFW.dbo.UNITMTR M ON U.UNITID = M.UNITID AND M.METERDEFID = 33 -- ECM Meter
  WHERE u.SERIALNO = TractorVIN


  UPDATE #TractorCleanUp
  set UpID = TELEscrowId
  from TELEscrow.DBMigration.AccountMapping
  where TelCustom = UpdatedBy


  UPDATE #TractorCleanUp
  set CreID = TELEscrowId
  from TELEscrow.DBMigration.AccountMapping
  where TelCustom = CreatedBy

  INSERT INTO #TractorCleanUpException (
    TractorVIN,
    TMake,
    TModel,
    UpdatedBy,
    UpdateDt,
    CreateDt,
    CreateedBy,
    ExecptionCode
  )
    SELECT
      TFX.TractorVIN,
      TFX.tMake,
      TFX.tModel,
      TFX.UpdatedBy,
      TFX.UpdateDt,
      TFX.CreateDt,
      TFX.CreatedBy,
      'Make and model not found'
    FROM #TractorCleanUp TFX
    where TractorMakeModelID is null


  delete #TractorCleanUp
  where TractorMakeModelID is null;





  MERGE dbo.Tractor as t
  using #TractorCleanUp as s
  on t.TractorVIN = s.TractorVIN
  when not matched by target
  then insert (TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage)
  VALUES (s.TractorMakeModelID, s.TractorVIN, s.TractorYear, ISNULL(s.CreID, 1), ISNULL(S.CreateDt, GETDATE()),
          ISNULL(s.UpID, 1), ISNULL(s.UpdateDt, GETDATE()), s.Mileage)
  when matched  and s.TractorMakeModelID = t.TractorMakeModelID then
    update
    set UpdatedDate = isnull(UpdateDt, getdate()), UpdatedBy = isnull(UpID, 1)
  ;

  merge TELEscrow.DBMigration.TractorCleanUpException as t
  using #TractorCleanUpException as s
  on t.TractorVIN = s.TractorVIN
  when not matched then
    INSERT (TractorVIN, TMake, TModel, TractorYear, Mileage, CreateDt, UpdateDt, TractorMakeModelID, UpdatedBy, CreateedBy, ExecptionCode)
    VALUES
      (TractorVIN, TMake, TModel, TractorYear, Mileage, CreateDt, UpdateDt, TractorMakeModelID, UpdatedBy, CreateedBy,
        ExecptionCode);


  drop table #TractorCleanUp

  drop table #TractorCleanUpException

end
go

