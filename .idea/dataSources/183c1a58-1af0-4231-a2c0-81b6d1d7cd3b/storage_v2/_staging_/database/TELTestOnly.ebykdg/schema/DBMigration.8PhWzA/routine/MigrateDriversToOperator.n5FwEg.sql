

CREATE PROCEDURE DBMigration.MigrateDriversToOperator
  as begin

  delete from  DBMigration.TelCustomDriversException
CREATE TABLE #TelCustomDrivers (
  Ranking             INTEGER,
  FirstName           nvarchar(50)   not null,
  LastName            nvarchar(50)   not null,
  StreetAddress       nvarchar(100),
  OperatorBusinessName nvarchar(50),
  PhoneNumber         varchar(50),
  City                nvarchar(50),
  State               nvarchar(50),
  Zip                 char(5),
  CreatedBy            nvarchar(50)           ,
  CreatedDate         datetimeoffset,
  UpdatedBy            nvarchar(50)   ,
  UpdatedDate         datetimeoffset not null,
  FederalIdentificationNumber    varchar(4),
  UPid        integer,
  creid INTEGER
)




insert into #TelCustomDrivers (Ranking,
                               FirstName,
                               LastName,
                               StreetAddress,
                               OperatorBusinessName,
                               PhoneNumber,
                               City,
                               State,
                               Zip,
                               CreatedBy,
                               CreatedDate,
                               UpdatedBy,
                               UpdatedDate, FederalIdentificationNumber)
  SELECT
    ROW_NUMBER() OVER(PARTITION BY   FirstName,LastName ORDER BY CreateDate asc ),
    FirstName,
    LastName,
    Address,
    DriversCompany,
    PhoneNumber,
    City,
    State,
    Zip,
    case when CreatedBy is null
      then 'SYSTEM'
    else
    CreatedBy
      END ,
    TODATETIMEOFFSET(CreateDate, '-04:00'),
    case when UpdatedBy is null
      then 'SYSTEM'
    else
    UpdatedBy
      END ,
    TODATETIMEOFFSET(Updatedate, '-04:00'),
    right(SSN , 4)
  FROM TELCustom.dbo.Drivers
  where FirstName NOT LIKE '% and %' and FirstName not LIKE '% & %' and left(FirstName, 4) <> 'and '
        and LastName not LIKE '%and %' and LastName not LIKE '% & %'and left(LastName, 4) <> 'and '
 AND FirstName <> 'NULL' AND LastName <> 'NULL'


delete from #TelCustomDrivers where Ranking > 1



UPDATE #TelCustomDrivers SET UPid = TelEscrowID
FROM DBMigration.AccountMapping WHERE TelCustom = UpdatedBy


UPDATE #TelCustomDrivers SET creid = TelEscrowID
FROM DBMigration.AccountMapping WHERE TelCustom = CreatedBy


MERGE dbo.Operator as t
  using  #TelCustomDrivers as s
on t.FirstName = s.FirstName AND T.LastName = S.LastName
when not matched by target
then insert (FirstName, LastName, StreetAddress, OperatorBusinessName, PhoneNumber, City, Zip, State, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, FederalIdentificationNumber)
VALUES (FirstName, LastName, StreetAddress, OperatorBusinessName, PhoneNumber, City, Zip,State, creid, CreatedDate, UPid, UpdatedDate, FederalIdentificationNumber)



insert into DBMigration.TelCustomDriversException (
                               FirstName,
                               LastName,
                               StreetAddress,
                               PhoneNumber,
                               City,
                               State,
                               Zip,
                               CreatedBy,
                               CreatedDate,
                               UpdatedBy,
                               UpdatedDate, FederalIdentificationNumber, ExceptionDescription)
  SELECT
    FirstName,
    LastName,
    Address,
    PhoneNumber,
    City,
    State,
    Zip,
    case when CreatedBy is null
      then 'SYSTEM'
    else
    CreatedBy
      END ,
    TODATETIMEOFFSET(CreateDate, '-04:00'),
    case when UpdatedBy is null
      then 'SYSTEM'
    else
    UpdatedBy
      END ,
    TODATETIMEOFFSET(Updatedate, '-04:00'),
    right(SSN , 4),
    'Name format not proper. (Probably a joint contract)'
  FROM TELCustom.dbo.Drivers
  where FirstName  LIKE '% and %' or FirstName  LIKE '% & %' or left(FirstName, 4) = 'and '
        or LastName  LIKE '%and %' or LastName  LIKE '% & %'or left(LastName, 4) = 'and '
 or FirstName = 'NULL' or LastName = 'NULL'

DROP TABLE #TelCustomDrivers
END
go

