

ALTER  PROCEDURE DBMigration.MigrateContracts
  as begin
delete  from DBMigration.ContractExceptions

create table #Contract
(
  Ranking INTEGER,
	Contract# int not null,
	ContractStatusID int ,
  TelEscrowTractorId int,
  TVIN VARCHAR(20),
	ContractEffectiveDate datetimeoffset ,
	MaturityDate datetimeoffset ,
	ContractTerminatedDate datetimeoffset,
	RelatedContract int,
	CreatedBy varchar(50) ,
	CreatedDate datetimeoffset,
	UpdatedBy varchar(50),
	UpdatedDate datetimeoffset,
  creid INTEGER,
  UPID integer,
  isTerm BIT
)



INSERT INTO #Contract( Contract#,
                      TVIN,  ContractEffectiveDate, MaturityDate)

SELECT  con.ContractNumber, LEFT(cast(L.[Serial Number] as varchar(19)), 17),
  CON.EffectiveDate, con.ExpirationDate
FROM TELCustom.dbo.Contracts con
LEFT JOIN DBMigration.LeaseWorksAssetInfo L
ON "Contract Number" = ContractNumber



 update #Contract  set #Contract.CreatedBy = c.CreatedBy, #Contract.UpdatedBy = c.UpdatedBy, #Contract.CreatedDate = TODATETIMEOFFSET(c.CreateDate, '-04:00'), #Contract.UpdatedDate = TODATETIMEOFFSET(c.UpdateDate, '-04:00')
 from TELCustom.dbo.Contracts c where Contract# = ContractNumber

update #Contract set TVIN = left(t.VIN, 17)
  from TELCustom.dbo.Contracts con
  inner join TELCustom.dbo.Tractors t on con.TractorID = t.TractorID
  where Contract# = con.ContractNumber
  AND TVIN IS NULL



update #Contract set TelEscrowTractorId = TractorID
from dbo.Tractor where CHARINDEX(TVIN, TractorVIN) > 0
  OR CHARINDEX( TractorVIN, TVIN) > 0

update #Contract set creid = TelEscrowID
from DBMigration.AccountMapping where CreatedBy = TelCustom


update #Contract set UPID = TelEscrowID
from DBMigration.AccountMapping where UpdatedBy = TelCustom


  update #Contract set CreatedDate  = ContractEffectiveDate
where CreatedDate is null






INSERT INTO DBMigration.ContractExceptions(Ranking, ContractNumber,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, ExceptionMessage)
  SELECT Ranking, Contract#,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, 'No Vin number in telcustom OR leaseworks DB' FROM #Contract WHERE TVIN IS NULL

delete  from #Contract where TVIN IS NULL


------------------------------------------------------------------------------------------------------------------------
  Create Table #MissingTractors(
    Tmake varchar(50),
    Tmodel varchar(50),
    tvin varchar(50),
    tyear   INTEGER,
    Mileage INTEGER,
    MileageDate DATETIMEOFFSET,
    WarrantyDate  DATETIMEOFFSET,
    InServiceDate DATETIMEOFFSET,
    mmyID       INTEGER
  )

  INSERT INTO #MissingTractors(Tmake, Tmodel, tvin, tyear, Mileage, MileageDate, WarrantyDate, InServiceDate)
    select
    CASE WHEN MAKE = 'FRGHT'
      THEN 'Freightliner'
      WHEN MAKE = 'PTRBL'
        then
          'Peterbilt'
       WHEN MAKE = 'KNWRT'
        then
          'Kenworth'
       WHEN MAKE = 'INTNL'
        then
          'International'
      else MAKE
      end,
       MODEL, LEFT(SERIALNO, 17), MODELYEAR,0, SYSDATETIMEOFFSET(), WARRANTYINSERVICE, INSERVICE
  from #Contract
  left join TFW.dbo.UNITS on (
      CHARINDEX(TVIN, SERIALNO) > 0
  OR CHARINDEX( SERIALNO, TVIN) > 0
      )
  where TYPE = 'Tractor' and TelEscrowTractorId is null


update #MissingTractors set mmyID = tmm.TractorMakeModelID
from dbo.TractorMakeModel TMM WHERE
Tmake = make and charindex(Make, Tmake) > 0


  delete #MissingTractors
  where mmyID is null;

MERGE dbo.Tractor as t
  using #MissingTractors as s
on t.TractorVIN = s.tvin
when not matched by target
 then insert (TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage)
VALUES (s.mmyID, s.tvin, s.tyear, 1,sysdatetimeoffset(), 1 , sysdatetimeoffset(), s.Mileage)

;
drop table #MissingTractors
------------------------------------------------------------------------------------------------------------------------


update #Contract set TelEscrowTractorId = TractorID
from dbo.Tractor where CHARINDEX(TVIN, TractorVIN) > 0
  OR CHARINDEX( TractorVIN, TVIN) > 0


INSERT INTO DBMigration.ContractExceptions(Ranking, ContractNumber,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, ExceptionMessage)
  SELECT Ranking, Contract#,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, 'No Tractor id in TelEscrow DB: Check tractor exceptions table' FROM #Contract WHERE
 TelEscrowTractorId is null


DELETE  FROM #Contract WHERE
 TelEscrowTractorId is null






UPDATE #Contract SET ContractStatusID = case when isTerm = 1 and ContractTerminatedDate is not null
  then 3
  when isTerm = 1 and ContractTerminatedDate is  null
  then 2
  else 1
  end


update #Contract set MaturityDate = [Maturity Date], ContractEffectiveDate= [Commencement Date], ContractTerminatedDate = CASE WHEN [Termination Date] = '01/01/1800'
  THEN NULL
  ELSE [Termination Date]
  end
  from DBMigration.[LeaseWorksAccountInfo]
where [Contract Number] = Contract#


MERGE dbo.Contract AS T
  USING #Contract as s
on T.ContractNumber = S.Contract#
when not matched by target then
insert
(ContractNumber, ContractStatusID, TractorID,
 ContractEffectiveDate, MaturityDate,
 ContractTerminatedDate, CreatedBy, CreatedDate,
UpdatedBy, UpdatedDate, EscrowRate)
VALUES (s.Contract#, s.ContractStatusID, s.TelEscrowTractorId,
 s.ContractEffectiveDate,  s.MaturityDate,
 s.ContractTerminatedDate, isnull(s.creid,1 ),  isnull(s.CreatedDate, getdate()),
 isnull(s.UPID,1 ),  isnull(s.UpdatedDate, getdate()), 0.07)
;

DROP TABLE #Contract
  end
go

