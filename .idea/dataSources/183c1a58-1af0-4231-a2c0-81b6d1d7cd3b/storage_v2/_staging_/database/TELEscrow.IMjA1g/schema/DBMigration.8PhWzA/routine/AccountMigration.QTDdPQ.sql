CREATE PROCEDURE DBMigration.AccountMigration
As BEGIN
  Create Table #AccountS (
    AccountName Varchar(100),
    AccountKey  VARCHAR(100),
    Email       VARCHAR(50),
    FirstName   Varchar(50),
    LastName    Varchar(50),
    Timezoneid  INTEGER DEFAULT 1,
    CreateDt    DATETIMEOFFSET,
    UpdateDt    DATETIMEOFFSET
  )

  Create Table #CleanedAccount (
    AccountName Varchar(100),
    Email       VARCHAR(50),
    FirstName   Varchar(50),
    LastName    Varchar(50),
    Timezoneid  INTEGER DEFAULT 1,
    CreateDt    DATETIMEOFFSET,
    UpdateDt    DATETIMEOFFSET
  )



  INSERT INTO #Accounts (
    AccountName,
    AccountKey)
    select DISTINCT TelCustom, TELESscrow
    from DBMigration.AccountMapping

  UPDATE #Accounts
  SET  Email = ed.Email, FirstName = ed.[First Name], LastName = ed.[Last Name], AccountName = ed.Username,
    Timezoneid  = 1
  FROM CIP.DBO.AD_users ed
  where SUBSTRING(AccountKey, 1, 6) = ed.UserName

  Update #Accounts
  set FirstName = AccountKey , LastName = AccountKey, Email = AccountKey + '@badData.com'
  where FirstName is Null;


  update #Accounts
  set CreateDt = TODATETIMEOFFSET(ud.CreateDate, '-04:00'), UpdateDt = TODATETIMEOFFSET(ud.UpdateDate, '-04:00')
  from TELCustom.dbo.EscrowUserData ud
  Where AccountKey = ud.UserName;

UPDATE #Accounts SET UpdateDt = CreateDt WHERE UpdateDt IS NULL

  UPDATE #Accounts SET CreateDt = SYSDATETIMEOFFSET ( ) , UpdateDt = SYSDATETIMEOFFSET ( )  where CreateDt IS NULL

UPDATE #AccountS set AccountName = 'SYSTEM' where AccountKey in ('SYSTEM', 'MANUAL')

insert into #CleanedAccount(AccountName, Email, FirstName, LastName, CreateDt, UpdateDt)
    select distinct AccountName, Email, FirstName, LastName, CreateDt, UpdateDt from #Accounts





  MERGE dbo.Account as target
  USING #CleanedAccount as source
  on target.AccountName = source.AccountName
  WHEN not matched  then
    INSERT (AccountName, FirstName, LastName, Email, Timezoneid, CreatedDate, UpdatedDate)
    values (source.AccountName, source.FirstName, source.LastName, source.Email, source.Timezoneid, source.CreateDt,
            source.UpdateDt)
  WHEN MATCHED THEN
    update
  set
    FirstName = source.FirstName,
    LastName = source.LastName,
    Email = source.Email,
    Timezoneid = source.TimezoneID,
    CreatedDate = source.CreateDt,
    UpdatedDate = source.UpdateDt;


DROP TABLE #Accounts
DROP TABLE #CleanedAccount




END
go

