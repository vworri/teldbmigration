ALTER PROCEDURE DBMigration.ContractCarrierUnitRelationship
as begin



  delete from DBMigration.TractorRelExceptions

  CREATE TABLE #TractorRel (
    UNIT#                  VARCHAR(580),
    Con#                   integer,
    TelEscrowCarrierNumber INTEGER,
    isTermiated            bit,
    isCurrent              bit,
    telcustCarrId          integer
  )


  CREATE TABLE #Ranked_TractorRel (
    row_id                 integer,
    UNIT#                  VARCHAR(50),
    Con#                   integer,
    TelEscrowCarrierNumber INTEGER,
    isTermiated            bit,
    isCurrent              bit,
    ConId                  INTEGER
  )

  CREATE TABLE #Ranked_TractorRelID (
    row_id                 integer,
    UNIT#                  VARCHAR(50),
    Con#                   integer,
    TelEscrowCarrierNumber INTEGER,
    isTermiated            bit,
    isCurrent              bit,
    ConId                  INTEGER
  )


  create table #current_unit_owners (
    unit      varchar(51),
    contract# INTEGER
  )


  insert into #TractorRel (
    Con#, UNIT#, TelEscrowCarrierNumber, isTermiated, isCurrent, telcustCarrId
  )
    SELECT
      ContractNumber,
      UnitNumber,
       CASE WHEN
        LEFT(UNITNUMBER, 1) = 'T' and isnumeric(substring(UNITNUMBER, 2, 4)) = 1 and len(UNITNUMBER) = 5
        then 5
      when LEFT(UNITNUMBER, 1) = 'T' and len(UNITNUMBER) = 6 and isnumeric(substring(UNITNUMBER, 2, 5)) = 1
        then 11
      when LEFT(UNITNUMBER, 2) = '11' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
           substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7)
        then 1
      when (left(UNITNUMBER, 2) = '23' or left(UNITNUMBER, 2) = '24') and len(UNITNUMBER) = 5
        then 1
      when (len(UNITNUMBER) = 4) or (left(UNITNUMBER, 3) = '159' and len(UNITNUMBER) = 6) or (
        LEFT(UNITNUMBER, 2) = '21' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
        substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) = 7)
        then 2
      when (LEFT(UNITNUMBER, 2) = '31' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
            substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) = 7)
        then 3
      when len(UNITNUMBER) = 6 and left(UNITNUMBER, 3) = '395'
        then 15
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 2)) = 1 and len(UNITNUMBER) = 4
        then 16
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 4)) = 1 and len(UNITNUMBER) = 6
        then 7
      when left(UNITNUMBER, 2) = '91'  and len(UNITNUMBER) = 5
        then 10
      end,
      IsTerminated,
      case when IsTerminated = 0
        then 1
      else 0
      end,
      Carrier
    FROM TELCustom.dbo.drivers


  insert into #TractorRel (Con#, TelEscrowCarrierNumber, UNIT#)
    SELECT
      CUSTOMERNAME,
       CASE WHEN
        LEFT(UNITNUMBER, 1) = 'T' and isnumeric(substring(UNITNUMBER, 2, 4)) = 1 and len(UNITNUMBER) = 5
        then 5
      when LEFT(UNITNUMBER, 1) = 'T' and len(UNITNUMBER) = 6 and isnumeric(substring(UNITNUMBER, 2, 5)) = 1
        then 11
      when LEFT(UNITNUMBER, 2) = '11' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
           substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7)
        then 1
      when (left(UNITNUMBER, 2) = '23' or left(UNITNUMBER, 2) = '24') and len(UNITNUMBER) = 5
        then 1
      when (len(UNITNUMBER) = 4) or (left(UNITNUMBER, 3) = '159' and len(UNITNUMBER) = 6) or (
        LEFT(UNITNUMBER, 2) = '21' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
        substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7))
        then 2
      when (LEFT(UNITNUMBER, 2) = '31' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
            substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7))
        then 3
      when len(UNITNUMBER) = 6 and left(UNITNUMBER, 3) = '395'
        then 15
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 2)) = 1 and len(UNITNUMBER) = 4
        then 16
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 4)) = 1 and len(UNITNUMBER) = 6
        then 7
      when left(UNITNUMBER, 2) = '91'  and len(UNITNUMBER) = 5
        then 10
      end
        as Carrier,
      UNITNUMBER
     FROM
      TFW.[dbo].[INVORDER] I ( NOLOCK )
      JOIN TFW.[dbo].[ORDERS] O ( NOLOCK ) ON
                                         O.[ORDERID] = I.[ORDERID] AND
                                         O.[ORDERTYPE] = 'INVOICE'
      JOIN  TFW.[dbo].[VIEW_INVOICE_TOTALS] V ( NOLOCK ) ON
                                                      V.[ORDERID] = I.[ORDERID]
      LEFT JOIN  TFW.[dbo].[UNITS] U ( NOLOCK ) ON U.UNITID = O.UNITID
      LEFT JOIN TFW.[dbo].[CUSTOMERS] C ( NOLOCK ) ON C.CUSTID = O.CUSTID
    WHERE
      I.[INVTYPE] IN ('REPAIR', 'DIRECTSALE', 'CREDIT', 'FUEL TICKET') and
     left(CUSTOMERNAME, 8) in (select cast(ContractNumber as char(12))
                           from TELEscrow.dbo.Contract) and UNITNUMBER is not null
    GROUP BY left(CUSTOMERNAME, 8) , UNITNUMBER, UNITNUMBER


  insert into #TractorRel (Con#, TelEscrowCarrierNumber, UNIT#)
    SELECT
      Drivers.ContractNumber,
      TELESC,
      COALESCE(Payments.UnitNumber, Drivers.UnitNumber)
    FROM TELCustom.dbo.Payments
      JOIN TELCustom.dbo.Drivers ON Drivers.DriversID = Payments.DriverID
      JOIN TELCustom.dbo.PaymentTypes ON Payments.PaymentTypeID = PaymentTypes.PaymentTypeId
      left join DBMigration.CarrierMapping on Payments.CarrierID = TELESC
    WHERE ISNULL(Payments.IsRemoved, 0) = 0
          and
          FirstName NOT LIKE '% and %' and FirstName not LIKE '% & %' and left(FirstName, 4) <> 'and '
          and LastName not LIKE '%and %' and LastName not LIKE '% & %' and left(LastName, 4) <> 'and '
          AND FirstName <> 'NULL' AND LastName <> 'NULL'


  update #TractorRel
  set TelEscrowCarrierNumber = TELESC
  from TELEscrow.DBMigration.CarrierMapping
  where TELCUSTID = telcustCarrId
        and TelEscrowCarrierNumber is null;

  insert into #Ranked_TractorRel (
    row_id, UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent
  ) select
      row_number()
      over ( partition by UNIT#, Con#, TelEscrowCarrierNumber
        order by isTermiated ),
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent
    from #TractorRel


  DELETE FROM #Ranked_TractorRel
  WHERE row_id > 1

  insert into #current_unit_owners
  (unit, contract#)
    select
      UNITNUMBER,
      left(CUSTOMERNAME, 8)
    from tfw.dbo.UNITS u
      join TFW.dbo.CUSTOMERS c on u.CUSTID = c.CUSTID
    where isnumeric(left(CUSTOMERNAME, 8) ) = 1

  update #Ranked_TractorRel
  set isTermiated = CASE
                    WHEN COALESCE(ContractTerminatedDate, MaturityDate) < getdate()
                      THEN 1
                    ELSE 0
                    END
  from TELEscrow.dbo.Contract
  where Con# = ContractNumber


  update #Ranked_TractorRel
  set isCurrent = CASE WHEN UNIT# = UNIT
    THEN 1
                  ELSE 0
                  END
  from #current_unit_owners
  where contract# = Con#

  UPDATE #Ranked_TractorRel
  SET isCurrent = 0
  WHERE isCurrent IS NULL


  UPDATE #Ranked_TractorRel
  set ConId = c.contractID
  from TELEscrow.dbo.Contract c
  where ContractNumber = Con#


  update #Ranked_TractorRel
  set TelEscrowCarrierNumber = CASE WHEN
        LEFT(UNIT#, 1) = 'T' and isnumeric(substring(UNIT#, 2, 4)) = 1 and len(UNIT#) = 5
        then 5
      when LEFT(UNIT#, 1) = 'T' and len(UNIT#) = 6 and isnumeric(substring(UNIT#, 2, 5)) = 1
        then 11
      when LEFT(UNIT#, 2) = '11' and substring(UNIT#, 3, 1) in ('7', '8', '9') and
           substring(UNIT#, 4, 1) = '9' and len(UNIT#) in (6, 7)
        then 1
      when (left(UNIT#, 2) = '23' or left(UNIT#, 2) = '24') and len(UNIT#) = 5
        then 1
      when (len(UNIT#) = 4) or (left(UNIT#, 3) = '159' and len(UNIT#) = 6) or (
        LEFT(UNIT#, 2) = '21' and substring(UNIT#, 3, 1) in ('7', '8', '9') and
        substring(UNIT#, 4, 1) = '9' and len(UNIT#) in (6, 7))
        then 2
      when (LEFT(UNIT#, 2) = '31' and substring(UNIT#, 3, 1) in ('7', '8', '9') and
            substring(UNIT#, 4, 1) = '9'  and len(UNIT#) in (6, 7))
        then 3
      when len(UNIT#) = 6 and left(UNIT#, 3) = '395'
        then 15
      when isnumeric(left(UNIT#, 2)) = 0 and isnumeric(right(UNIT#, 2)) = 1 and len(UNIT#) = 4
        then 16
      when isnumeric(left(UNIT#, 2)) = 0 and isnumeric(right(UNIT#, 4)) = 1 and len(UNIT#) = 6
        then 7
      when left(UNIT#, 2) = '91'  and len(UNIT#) = 5
        then 10
      end


  INSERT INTO #Ranked_TractorRelID (row_id, UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, ConId)
    SELECT
      ROW_NUMBER()
      OVER ( PARTITION BY UNIT#, ConID, TelEscrowCarrierNumber
        ORDER BY isCurrent ),
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      Conid
    FROM #Ranked_TractorRel

  DELETE FROM #Ranked_TractorRelID
  where row_id > 1


  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'Contract in Exceptions table'
    from #Ranked_TractorRelid
    where Con# in (select contractNumber
                   from DBMigration.ContractExceptions)
          and Con# is not null

  delete from #Ranked_TractorRelid
  where Con# in (select con#
                 from DBMigration.TractorRelExceptions)


  delete from #Ranked_TractorRelID
  where con# is null

  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'No unit number associated with contract'
    from #Ranked_TractorRelID
    where UNIT# is null

  delete from #Ranked_TractorRelid
  where UNIT# is null


  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'Carrier unknown'
    from #Ranked_TractorRelid
    where TelEscrowCarrierNumber is null

  delete from #Ranked_TractorRelid
  where TelEscrowCarrierNumber is null

  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'Contract to Migrated'
    from #Ranked_TractorRelid
    where ConId is null

delete from #Ranked_TractorRelID where ConId is null

  merge TELEscrow.dbo.ContractCarrierUnitNumber as t
  using #Ranked_TractorRelid as s
  on t.UnitNumber = s.UNIT# and t.ContractID = s.ConId and t.CarrierID = s.TelEscrowCarrierNumber
  when matched then update
  set t.CurrentRelationship = s.isCurrent, UnitNumber = s.UNIT#
  when not matched
  then insert (ContractID, CarrierID, CurrentRelationship, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, UnitNumber)
  values (s.ConId, s.TelEscrowCarrierNumber, s.isCurrent, 1, sysdatetimeoffset(), 1, sysdatetimeoffset(), s.UNIT#);


  drop table #TractorRel

  drop table #Ranked_TractorRel

  drop table #current_unit_owners
  drop table #Ranked_TractorRelID
end
go

