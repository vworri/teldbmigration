ALTER PROCEDURE InterestCredit @MonthPeriodID INTEGER = NULL, @ContractID INTEGER = Null
  AS BEGIN

------------------------------------------------------------------------------------------------------------------------
-- Get the monthID of interest if none is given
------------------------------------------------------------------------------------------------------------------------

IF @MonthPeriodID IS NULL
    select  @MonthPeriodID = MAX(MonthEndPeriodID) FROM dbo.TransactionCloseOfMonth
    WHERE PeriodClosed = 0 AND MonthEndPeriodDate < getdate();

------------------------------------------------------------------------------------------------------------------------
-- check to see if the month is closed
------------------------------------------------------------------------------------------------------------------------
  DECLARE @MonthClosedStatus VARCHAR(3)
   select @MonthClosedStatus = PeriodClosed FROM dbo.TransactionCloseOfMonth WHERE MonthEndPeriodID = @MonthPeriodID

------------------------------------------------------------------------------------------------------------------------
-- Get the month end date and interest rate for the monthid of interest
------------------------------------------------------------------------------------------------------------------------
  DECLARE @InterestRate NUMERIC(9,8)
  DECLARE @RUNDATE DATETIME
   select @RUNDATE = MonthEndPeriodDate, @InterestRate = (InterestCreditRate / 12) from dbo.TransactionCloseOfMonth WHERE MonthEndPeriodID = @MonthPeriodID

------------------------------------------------------------------------------------------------------------------------
-- create custom transaction message for interest run
------------------------------------------------------------------------------------------------------------------------
  declare @CustomMessage NVARCHAR(max)
   SET @CustomMessage = 'Interest credit run for Month of '+ CAST(DATENAME(m,@RUNDATE) AS VARCHAR(10)) + ' ,' + CAST(datepart(YEAR, @RUNDATE) AS VARCHAR(4)) + ' on ' + CAST(GETDATE() AS VARCHAR(9)) + ' at an interest rate of ' + CAST(@InterestRate AS VARCHAR(10))
------------------------------------------------------------------------------------------------------------------------
-- create needed temp tables
------------------------------------------------------------------------------------------------------------------------

    CREATE TABLE #ContractIDs(
    contract_id  integer,
    Con# INTEGER,

  )
    CREATE TABLE #ActiveContracts(
    conID   INTEGER,
    Con# INTEGER,
    BalanceDate DATETIME
  )


  CREATE TABLE #TransactionSummary(
    conID   INTEGER,
    Contract# INTEGER,
    RollingBalance NUMERIC(9,2),
    Day     DATE,
    CCUID           INTEGER,
  )

  CREATE TABLE #DaysInMonth(
    BalanceDate DATETIME
  )


  CREATE TABLE #InterestCalc(
    AvgDailyBalance NUMERIC(9,2),
    Contract#        INTEGER,
    IntCred         NUMERIC(9,2),
    CCUID           INTEGER,
    TransType       INTEGER,
    MonthID         INTEGER,
  )
------------------------------------------------------------------------------------------------------------------------
-- Get all days in the month of interest
------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #DaysInMonth(BalanceDate)
    EXEC dbo.GetDaysInMonth_Helper  @RUNDATE

------------------------------------------------------------------------------------------------------------------------
-- get all active and pending final accounting contracts or for a specific contract
------------------------------------------------------------------------------------------------------------------------
if @ContractID is null
  INSERT  INTO #ActiveContracts( Con#, BalanceDate)
    SELECT  ContractNumber, BalanceDate FROM dbo.Contract
    left join  #DaysInMonth ON ContractID > 0
    WHERE  ContractStatusID in (1, 2)

else
  INSERT  INTO #ActiveContracts( Con#, BalanceDate)
     SELECT  ContractNumber, BalanceDate FROM dbo.Contract
    left join  #DaysInMonth ON ContractID > 0
    WHERE  ContractID = @ContractID



------------------------------------------------------------------------------------------------------------------------
  -- GET recent contract id
------------------------------------------------------------------------------------------------------------------------
  insert into #ContractIDs(contract_id, con#)
    select Max(ContractID), con#
  from (select distinct Con# from #ActiveContracts ) con
    INNER join dbo.Contract on ContractNumber = con#
  group by Con#

  UPDATE #ActiveContracts SET conID = contract_id
  FROM #ContractIDs WHERE #ActiveContracts.Con# = #ContractIDs.Con#

------------------------------------------------------------------------------------------------------------------------
-- calculate rolling balance for each day of the month
------------------------------------------------------------------------------------------------------------------------
  INSERT  INTO #TransactionSummary( Contract#, RollingBalance, Day)
  select AC.Con#, SUM(CurrentTransactionAmount) , AC.BalanceDate
  from
    dbo.[Transaction] t
  INNER JOIN dbo.ContractCarrierUnitNumber ccu on t.ContractCarrierUnitNumberID = ccu.ContractCarrierUnitNumberID
  INNER join dbo.Contract con on con.ContractID = ccu.ContractID
  INNER JOIN #ActiveContracts  ac ON ac.Con# = con.ContractNumber AND t.PostDate <= ac.BalanceDate
  GROUP BY  ac.Con#, AC.BalanceDate ORDER BY  AC.BalanceDate DESC

update #TransactionSummary set conID = c.ConID
  from #ActiveContracts c where Contract# = Con#

update #TransactionSummary set CCUID = ContractCarrierUnitNumberID
  from dbo.ContractCarrierUnitNumber where conID = ContractID and CurrentRelationship = 1


 ------------------------------------------------------------------------------------------------------------------------
-- calculate interest using rolling balance
------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #InterestCalc(AvgDailyBalance, Contract#, IntCred, CCUID, TransType, MonthID)
  select AVG(RollingBalance), Contract#, AVG(RollingBalance) * @InterestRate, max(CCUID), 6, @MonthPeriodID
  from #TransactionSummary
  GROUP BY Contract#

------------------------------------------------------------------------------------------------------------------------
-- update interest values to zero on negative amounts or amounts < 0.01
------------------------------------------------------------------------------------------------------------------------
update #InterestCalc set IntCred = 0 where IntCred <= 0.01;
 ------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
  MERGE dbo.[Transaction]  as target
  using #InterestCalc as source
  on TransactionTypeID = TransType and MonthID = MonthEndPeriodID AND CCUID = ContractCarrierUnitNumberID
  when matched then UPDATE
    SET
      target.Comments = @CustomMessage,
      target.CurrentTransactionAmount = source.IntCred,
      UpdatedBy = 1,
      UpdatedDate = GETDATE()
  when not matched
    THEN INSERT (TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, TransactionException, MonthEndPeriodID, IsDeleted, CreatedBy, UpdatedBy, CreatedDate, UpdatedDate
  , Comments)
    VALUES (TransType, getdate(), getdate(), IntCred, CCUID, 0,MonthID, 0, 1, 1, GETDATE(), getdate(), @CustomMessage)
    ;

  UPDATE dbo.TransactionCloseOfMonth SET InterestCreditRunDate = GETDATE()
  WHERE MonthEndPeriodID = @MonthPeriodID
  DROP TABLE #ActiveContracts
  drop table #TransactionSummary
  drop table #DaysInMonth
  DROP TABLE #InterestCalc
  DROP TABLE #ContractIDs
end
;
go

