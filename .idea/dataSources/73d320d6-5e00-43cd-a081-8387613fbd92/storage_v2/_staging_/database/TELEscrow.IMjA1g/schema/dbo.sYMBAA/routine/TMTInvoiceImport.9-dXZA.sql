ALTER PROCEDURE dbo.TMTInvoiceImport @end_boundary DATE = NULL, @look_behind INTEGER = 60
AS BEGIN

  ----------------------------------------------------------------------------------------------------------------------
  -- Author: Vera Worri
  -- Purpose: Importing and updating invoices from the TMT system (Housed int he TFW DB) to the Tel Escrow system
  -- Example Execution:
  -- EXEC TMT_AUTOMATED_INVOICE_IMPORT
  --- Methodology:
  -- Collect all contract numbers that are active or pending final accounting
  -- Collect all invoices using those contract numbers as customers withing the arguments time span
  -- DELETE ALL INVOICES that have been removed by TEL in both Legit and exception table
  -- Collect ALL TMT invoices already in TEL Escrow
  -- Split transactions that are in other months but need to be updated
  -- Split the TFW invoices into valid and invalid
  -- Only invoices for the current open month are added into the transaction table
  ----------------------------------------------------------------------------------------------------------------------
  -- Handle default @end_boundary

  if @end_boundary is null
    SET @end_boundary = GETDATE()
  ----------------------------------------------------------------------------------------------------------------------
  --  declare the upper boundary for the look-behind
  ----------------------------------------------------------------------------------------------------------------------
  DECLARE @start_boundary DATE = DATEADD(DAY, @look_behind * -1, @end_boundary);

  ----------------------------------------------------------------------------------------------------------------------
  --  get current monthID
  ----------------------------------------------------------------------------------------------------------------------
  DECLARE @CurrentMonthID INTEGER;
  SELECT @CurrentMonthID = MonthEndPeriodID
  FROM TELEscrow.dbo.TransactionCloseOfMonth
  WHERE DATEPART(MONTH, getdate()) = DATEPART(MONTH, MonthEndPeriodDate) AND
        DATEPART(year, getdate()) = DATEPART(YEAR, MonthEndPeriodDate)

  ----------------------------------------------------------------------------------------------------------------------
  --  Create all needed temp tables
  ----------------------------------------------------------------------------------------------------------------------
  CREATE TABLE #Current_TelEscrow_Transactions (
    TFWINVID       INTEGER,
    TransactionID  INTEGER PRIMARY KEY,
    Amount         NUMERIC(9, 2),
    FirstTransdate DATETIME,
    MID            INTEGER
  )


  CREATE TABLE #Current_TelEscrow_TransactionExceptions (
    TFWINVID       INTEGER,
    TransactionID  INTEGER PRIMARY KEY,
    Amount         NUMERIC(9, 2),
    FirstTransdate DATETIME,
    MID            INTEGER
  )
  CREATE TABLE #Contracts (
    Contract#  INTEGER,
    ConID      INTEGER,
    CUSTOMERID INTEGER
  )


  CREATE table #ALL_TFW_INVOICES (
    TransactionDate             date           not null,
    PostDate                    date           not null,
    CurrentTransactionAmount    numeric(19, 2) not null,
    ContractCarrierUnitNumberID int            not null,
    AmountDiff                  numeric(9, 2),
    Invoice#                    varchar(50),
    ClosedDate                  datetimeoffset,
    Comments                    nvarchar(max),
    [OrderID]                   int,
    isSplit                     BIT default 0,
    isOpenMonth                 BIT default 0,
    VIN                         char(24),
    MonthID                     INTEGER
  )


  CREATE table #VALID_TFW_INVOICES (
    TransactionDate             date           not null,
    PostDate                    date           not null,
    CurrentTransactionAmount    numeric(19, 2) not null,
    ContractCarrierUnitNumberID int            not null,
    AmountDiff                  numeric(9, 2),
    Invoice#                    varchar(50),
    ClosedDate                  datetimeoffset,
    Comments                    nvarchar(max),
    [OrderID]                   int,
    isSplit                     BIT default 0,
    isOpenMonth                 BIT default 0,
    VIN                         char(24),
    MonthID                     INTEGER
  )

  CREATE table #INVALID_TFW_INVOICES (
    TransactionDate             date           not null,
    PostDate                    date           not null,
    CurrentTransactionAmount    numeric(19, 2) not null,
    ContractCarrierUnitNumberID int            not null,
    AmountDiff                  numeric(9, 2),
    Invoice#                    varchar(50),
    ClosedDate                  datetimeoffset,
    Comments                    nvarchar(max),
    [OrderID]                   int,
    isSplit                     BIT default 0,
    isOpenMonth                 BIT default 0,
    VIN                         char(24),
    MonthID                     INTEGER
  )

  ------------------------------------------------------------------------------------------------------------------------
  -- Get all Tel Escrow contract information where the contract is active or pending final accounting
  ------------------------------------------------------------------------------------------------------------------------

  INSERT INTO #Contracts (
    Contract#, ConID
  ) select distinct
      ContractNumber,
      ContractID
    FROM TELEscrow.dbo.Contract
    where ContractStatusID in (1, 2)
  ------------------------------------------------------------------------------------------------------------------------
  -- Get the TMT customer ID that corresponds to the contract number
  ------------------------------------------------------------------------------------------------------------------------
  UPDATE #Contracts
  SET CUSTOMERID = CUSTID
  from TFW.dbo.CUSTOMERS
  where CAST(Contract# AS VARCHAR(50)) = CUSTOMERNAME

  ------------------------------------------------------------------------------------------------------------------------
  -- Get all TMT invoices for all escrow accounts within the last 60 days or specified date range
  -- N.B. substring function takes a lot of time because it demands an inner query
  ------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #ALL_TFW_INVOICES (TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid)
    SELECT
      o.OPENED,
      o.OPENED,
      o.TOTALAMT,
      ccu.ContractCarrierUnitNumberID,
      o.ORDERNUM,
      todatetimeoffset(o.CLOSEDON, '-04:00'),
      o.ORDERID,
      u.SERIALNO,
      MonthEndPeriodID
    FROM
      TFW.dbo.ORDERS o
      INNER JOIN #Contracts c on o.CUSTID = cast(c.CUSTOMERID as varchar(50))
      INNER join TFW.dbo.UNITS u ON o.UNITID = u.UNITID
      INNER JOIN TELEscrow.dbo.ContractCarrierUnitNumber ccu
        on u.UNITNUMBER = ccu.UnitNumber and c.ConID = ccu.ContractID
           and
           o.OPENED >= @start_boundary and o.OPENED <= dateadd(day, 1, @end_boundary) and
           o.ORDERTYPE = 'INVOICE'
      left join TELEscrow.dbo.TransactionCloseOfMonth
        ON DATEPART(MONTH, MonthEndPeriodDate) = DATEPART(MONTH, o.OPENED) AND
           DATEPART(year, o.OPENED) = DATEPART(YEAR, MonthEndPeriodDate)
------------------------------------------------------------------------------------------------------------------------


  update #ALL_TFW_INVOICES
  set Comments = SUBSTRING(-- Needed to get more informative comments
      (
        SELECT TOP (3) ', ' + RTRIM(CMP1.DESCRIP) AS [text()]
        FROM
          TFW.dbo.ORDERS AS O1 ( NOLOCK )
          INNER JOIN TFW.dbo.UNITS AS U1 ( NOLOCK ) ON O1.UNITID = U1.UNITID
          INNER JOIN TFW.dbo.ORDERLN AS OLN1 ( NOLOCK ) ON O1.ORDERID = OLN1.ORDERID
          INNER JOIN TFW.dbo.ORDERSEC AS OS1 ( NOLOCK ) ON OLN1.SECTION = OS1.SECTION
          INNER JOIN TFW.dbo.CMPONENT AS CMP1 ( NOLOCK ) ON OS1.COMPCODE = CMP1.CODE
        WHERE
          O1.ORDERNUM = Invoice#
        FOR XML PATH (''), ROOT ('MyString'), TYPE
      ).value('/MyString[1]', 'varchar(max)'), 2, 1000
  )
  
   update #ALL_TFW_INVOICES
  set Comments = Comments + '( ' + SUBSTRING(-- Needed to get more informative comments
      (
       SELECT  CAST( (COUNT(*) - 3) AS varchar(20))
        FROM
          TFW.dbo.ORDERS AS O1 ( NOLOCK )
          INNER JOIN TFW.dbo.UNITS AS U1 ( NOLOCK ) ON O1.UNITID = U1.UNITID
          INNER JOIN TFW.dbo.ORDERLN AS OLN1 ( NOLOCK ) ON O1.ORDERID = OLN1.ORDERID
          INNER JOIN TFW.dbo.ORDERSEC AS OS1 ( NOLOCK ) ON OLN1.SECTION = OS1.SECTION
          INNER JOIN TFW.dbo.CMPONENT AS CMP1 ( NOLOCK ) ON OS1.COMPCODE = CMP1.CODE
        WHERE
          O1.ORDERNUM = Invoice#
           having count(*) > 3
        FOR XML PATH (''), ROOT ('MyString'), TYPE

      ).value('/MyString[1]', 'varchar(max)'), 2, 1000
  )+ ' additional item(s))'


  ----------------------------------------------------------------------------------------------------------------------
  -- drop contracts table
  ----------------------------------------------------------------------------------------------------------------------
    DROP TABLE #Contracts

  ------------------------------------------------------------------------------------------------------------------------
  -- DELETE ALL INVOICES that have been removed by tel either as qan exception or as an actual transaction
  ------------------------------------------------------------------------------------------------------------------------
  DELETE from #ALL_TFW_INVOICES
  WHERE Invoice# in (select InvoiceNumber
                     from TELEscrow.dbo.TransactionExceptions
                     where IsDeleted = 1 AND TransactionID IS NULL)
  ------------------------------------------------------------------------------------------------------------------------
  DELETE from #ALL_TFW_INVOICES
  WHERE Invoice# in (select InvoiceNumber
                     from TELEscrow.dbo.[Transaction]
                     where IsDeleted = 1)
  ------------------------------------------------------------------------------------------------------------------------
  -- Get all valid TMT invoices that already exist in  Tel Escrow DB and is not deleted
  ------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #Current_TelEscrow_Transactions (TFWINVID, TransactionID, Amount, FirstTransdate, MID)
    SELECT
      InvoiceNumber,
      MAX(TransactionID),
      SUM(CurrentTransactionAmount),
      Min(TransactionDate),
      MAX(MonthEndPeriodID)
    FROM TELEscrow.dbo.[Transaction]
    where TransactionTypeID = 8 and InvoiceClosedDate is null and IsDeleted = 0
    GROUP BY InvoiceNumber
  ------------------------------------------------------------------------------------------------------------------------
  -- Get all invalid TMT invoices that already exist in  Tel Escrow DB and is not deleted
  ------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #Current_TelEscrow_TransactionExceptions (TFWINVID, TransactionID, Amount, FirstTransdate, MID)
    SELECT
      InvoiceNumber,
      MAX(TransactionID),
      SUM(CurrentTransactionAmount),
      Min(TransactionDate),
      MAX(MonthEndPeriodID)
    FROM TELEscrow.dbo.[TransactionExceptions]
    where TransactionTypeID = 8 and InvoiceClosedDate is null and IsDeleted = 0
    GROUP BY InvoiceNumber

  ------------------------------------------------------------------------------------------------------------------------
  --For transactions that exist in TelEscrow AS VALID already, get the difference between TelEscrow and TMT/TFW and update the
  -- Month to the most recent month
  ------------------------------------------------------------------------------------------------------------------------
  update #ALL_TFW_INVOICES
  set CurrentTransactionAmount -= Amount , PostDate = getdate(), MonthID = @CurrentMonthID
  from
    #Current_TelEscrow_Transactions
  where TFWINVID = OrderID

------------------------------------------------------------------------------------------------------------------------
  --Update CCU from TelEscrow. In case of discrepency, deffer to TEL Escrow
------------------------------------------------------------------------------------------------------------------------
    UPDATE #ALL_TFW_INVOICES set ContractCarrierUnitNumberID = t.ContractCarrierUnitNumberID
  from TELEscrow.dbo.[Transaction] t WHERE Invoice# = InvoiceNumber
  and InvoiceNumber is not null
  
  ----------------------------------------------------------------------------------------------------------------------
-- Get all valid invoices
  ----------------------------------------------------------------------------------------------------------------------
   INSERT INTO #VALID_TFW_INVOICES (TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid)
     select TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid
    from #ALL_TFW_INVOICES where MonthID = @CurrentMonthID and ContractCarrierUnitNumberID is not null ;
  ----------------------------------------------------------------------------------------------------------------------
-- Get all invalid invoices
  ----------------------------------------------------------------------------------------------------------------------
   INSERT INTO #INVALID_TFW_INVOICES (TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid)
     select TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid
    from #ALL_TFW_INVOICES where MonthID <> @CurrentMonthID or ContractCarrierUnitNumberID is null;
  ----------------------------------------------------------------------------------------------------------------------
  --- Drop unneeded tables
  ----------------------------------------------------------------------------------------------------------------------
    DROP TABLE #ALL_TFW_INVOICES
   DROP TABLE   #Current_TelEscrow_Transactions
  ----------------------------------------------------------------------------------------------------------------------
  -- Merge TFW invoices into Tel Escrow DB for ONLY CURRENT OPEN MONTH: any manual rerunning will put transactions for
  -- previous months into exception for extra vetting
  ----------------------------------------------------------------------------------------------------------------------
  merge TELEscrow.dbo.[TransactionExceptions] as target
  using #INVALID_TFW_INVOICES as source
  ----------------------------------------------------------------------------------------------------------------------
  -- joining on Invoice number and Month Period ID
  ----------------------------------------------------------------------------------------------------------------------
  on Invoice# = InvoiceNumber and MonthID = MonthEndPeriodID
  ----------------------------------------------------------------------------------------------------------------------
  when not matched by target  then insert
  (TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber,
   InvoiceClosedDate, Comments, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, [TFWInvoiceOrderID], IsDeleted, TractorVIN, MonthID)
  VALUES (8, source.TransactionDate, source.PostDate,
             CASE WHEN source.isOpenMonth = 1
               then
                 source.CurrentTransactionAmount
             when source.isOpenMonth = 0
               then source.AmountDiff
             end, source.ContractCarrierUnitNumberID,
             source.Invoice#, source.ClosedDate,
             CASE WHEN source.AmountDiff = 0 and isSplit = 1
               then 'No amount changed: Invoice closed this month'
             ELSE source.Comments END, 1, getdate(), 1, getdate(), source.OrderID, 0, VIN, MonthID)
  when not matched by source
    and target.TransactionDate > @start_boundary  and target.TransactionDate < dateadd(day, 1, @end_boundary)
  then update set target.IsDeleted = 1
  when matched
    and (isOpenMonth = 0 or source.ContractCarrierUnitNumberID is null) then update
  set target.CurrentTransactionAmount = source.CurrentTransactionAmount,
    target.PostDate                   = source.PostDate,
    target.Comments                   = source.Comments,
    target.UpdatedDate                = getdate(),
    target.InvoiceClosedDate          = source.ClosedDate,
    target.UpdatedBy                  = 1
  ;



  ------------------------------------------------------------------------------------------------------------------------
-- drop invalid table
    DROP TABLE #INVALID_TFW_INVOICES

  ------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------
  -- Merge TFW invoices into Tel Escrow DB for ONLY CURRENT OPEN MONTH: any manual rerunning will put transactions for
  -- previous months into exception for extra vetting
  ----------------------------------------------------------------------------------------------------------------------
  merge TELEscrow.dbo.[Transaction] as target
  using #VALID_TFW_INVOICES as source
  ----------------------------------------------------------------------------------------------------------------------
  -- joining on Order ID where it is for an open month
  ----------------------------------------------------------------------------------------------------------------------
  on Invoice# = InvoiceNumber and MonthID = MonthEndPeriodID
  ----------------------------------------------------------------------------------------------------------------------
  -- when the invoice exists in TFW but not in TelEscrow, or the amounts do not match insert the amended invoice
  -- This should also take care of spit invoices and only insert when the invoice opened date is the CURRENT OPEN MONTH.
  --  There may be some inserts of amounts  = 0 when the amount has not changed but the invoice is closed in a closed
  --  month
  ----------------------------------------------------------------------------------------------------------------------
  when not matched by target then insert
  (TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber,
   InvoiceClosedDate, Comments, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, [TFWInvoiceOrderID], IsDeleted, TransactionException, MonthEndPeriodID)
  VALUES (8, source.TransactionDate, source.PostDate,
             CASE WHEN source.isOpenMonth = 1
               then
                 source.CurrentTransactionAmount
             when source.isOpenMonth = 0
               then source.AmountDiff
             end, source.ContractCarrierUnitNumberID,
             source.Invoice#, source.ClosedDate,
             CASE WHEN source.AmountDiff = 0 and isSplit = 1
               then 'No amount changed: Invoice closed this month'
             ELSE source.Comments END, 1, getdate(), 1, getdate(), source.OrderID, 0, 0, MonthID)
  when not matched by source
   and target.TransactionDate > @start_boundary  and target.TransactionDate < dateadd(day, 1, @end_boundary)
  then update set target.IsDeleted = 1
  when matched then update
  set target.CurrentTransactionAmount = source.CurrentTransactionAmount,
    target.PostDate                   = source.PostDate,
    target.Comments                   = source.Comments,
    target.UpdatedDate                = getdate(),
    target.InvoiceClosedDate          = source.ClosedDate,
    target.UpdatedBy                  = 1
  ;

  ----------------------------------------------------------------------------------------------------------------------
  -- final drop
  ----------------------------------------------------------------------------------------------------------------------

  DROP TABLE #VALID_TFW_INVOICES


END
go

