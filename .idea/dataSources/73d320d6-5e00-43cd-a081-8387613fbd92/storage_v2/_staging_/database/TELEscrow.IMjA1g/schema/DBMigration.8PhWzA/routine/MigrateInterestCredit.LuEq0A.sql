alter procedure DBMigration.MigrateInterestCredit
  as begin

  create table #InterestCreditRate
(
	InterestCreditRate numeric(19,4) ,
	InterestCreditRateMonth date,
	CreatedBy varchar(50) ,
  CreId   INTEGER,
	CreatedDate datetimeoffset,
	UpdatedBy varchar(50) ,
  UpId INTEGER,
	UpdatedDate datetimeoffset
)


INSERT INTO #InterestCreditRate(InterestCreditRate,
                                InterestCreditRateMonth,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate)
  SELECT IntrestPercantageRate, IntrestRateDate,
    CreatedBy,
    todatetimeoffset(CreateDate, '-04:00'),
    UpdatedBy,
    todatetimeoffset(UpdateDate, '-04:00')
  FROM TELCustom.dbo.InterestCredits


  update #InterestCreditRate set upId = TelEscrowID
  from DBMigration.AccountMapping WHERE UpdatedBy = TelCustom;

  update #InterestCreditRate set CreId = TelEscrowID
  from DBMigration.AccountMapping WHERE CreatedBy = TelCustom



  DELETE FROM TELEscrow.DBO.InterestCreditRate

  INSERT INTO TELEscrow.DBO.InterestCreditRate(InterestCreditRate,
                                     InterestCreditRateMonth,
                                     CreatedBy,
                                     CreatedDate,
                                     UpdatedBy,
                                     UpdatedDate)
    SELECT InterestCreditRate,
      InterestCreditRateMonth,
      ISNULL(CreId, 1),
      CreatedDate,
      ISNULL(UpId, 1),
      UpdatedDate
  from #InterestCreditRate

end
go

