alter  procedure DBMigration.BackFillClosedDate
as begin


Declare @StartDate DateTime ;

Declare @EndDate DateTime ;

select  @StartDate = DATEADD( MONTH, DATEDIFF (MONTH, 0, MIN(INVOICEOPENEDDATE) ), 0 )
  from TELCustom.dbo.Invoices

set @StartDate = dateadd(month, -1 , @StartDate)
-- Last day of month


select  @EndDate = DATEADD( MONTH, DATEDIFF (MONTH, 0, max(GETDATE()) ), 0 )
  from TELCustom.dbo.Invoices;

 CREATE TABLE #CloseDates(
   CloseD8 DATE,
   intCredRate numeric(9,8),
 );

WITH DaysInMonth_CTE AS
(
  SELECT  @StartDate DateValue
  UNION ALL
  SELECT  DATEADD(month,1,  DateValue)
  FROM    DaysInMonth_CTE
  WHERE   DATEADD(month,1,  DateValue) <= @EndDate
)
insert into #CloseDates(CloseD8)
SELECT DateValue
FROM    DaysInMonth_CTE
OPTION (MAXRECURSION 0)


update #CloseDates set intCredRate = IntrestPercantageRate
from TELCustom.dbo.InterestCredits
where DATEPART(MONTH,IntrestRateDate ) = DATEPART(MONTH, CloseD8)
AND DATEPART(YEAR, IntrestRateDate) = DATEPART(YEAR, CloseD8)




MERGE TELEscrow.dbo.TransactionCloseOfMonth AS target
  USING  #CloseDates AS SOURCE
  ON MonthEndPeriodDate = CloseD8
  WHEN NOT MATCHED
    THEN INSERT (MonthEndPeriodDate, PeriodClosed, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy, )
    VALUES (CloseD8,1, GETDATE(), 1, GETDATE(), 1 );

  drop table #CloseDates
  end
go

