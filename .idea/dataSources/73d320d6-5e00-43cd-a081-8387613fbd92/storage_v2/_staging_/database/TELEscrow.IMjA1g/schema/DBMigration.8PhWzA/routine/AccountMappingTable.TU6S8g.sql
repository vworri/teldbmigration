ALTER procedure DBMigration.AccountMappingTable
  as begin



  Create Table #DescAccount (
    AccountName Varchar(100),
    TelEscrowAccountName  VARCHAR(100),
    Email       VARCHAR(50),
    FirstName   Varchar(50),
    LastName    Varchar(50),
    Timezoneid  INTEGER DEFAULT 1,
    CreateDt    DATETIMEOFFSET,
    UpdateDt    DATETIMEOFFSET,
    id  INTEGER
  )


  Create Table #Accounts (
    AccountName Varchar(100),
    found_in    VARCHAR(100)
  )

 delete  from DBMigration.AccountMapping;
  INSERT INTO #Accounts (
    AccountName,
    found_in
  )
    SELECT
      UpdatedBy,
      'companies' AS [Users]
    FROM
      TelCustom.DBO.Companies WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'companies' AS [Users]
    FROM
      TelCustom.DBO.Companies WITH ( NOLOCK )


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'Contracts' AS [Users]
    FROM
      TelCustom.dbo.Contracts WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'contracts' AS [Users]
    FROM
      TelCustom.dbo.Contracts WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'drivers' AS [Users]
    FROM
      TelCustom.DBO.Drivers


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'drivers' AS [Users]
    FROM
      TelCustom.DBO.Drivers WITH ( NOLOCK )


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'IntCreds' AS [Users]
    FROM
      TelCustom.DBO.InterestCredits WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'IntCreds' AS [Users]
    FROM
      TelCustom.DBO.InterestCredits WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CREATEDBY,
      'Invoices' AS [Users]
    FROM
      TelCustom.DBO.Invoices WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UPDATEDBY,
      'Invoices' AS [Users]
    FROM
      TelCustom.DBO.Invoices WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'payments' AS [Users]
    FROM
      TelCustom.DBO.Payments WITH ( NOLOCK )

  insert into #Accounts (
    AccountName, found_in)
    SELECT
      CreatedBy,
      'payments' AS [Users]
    FROM
      TelCustom.DBO.Payments WITH ( NOLOCK )


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'tractors' AS [Users]
    FROM
      TelCustom.DBO.Tractors WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'tractors' AS [Users]
    FROM
      TelCustom.DBO.Tractors WITH ( NOLOCK )

  INSERT INTO #Accounts (
    AccountName)
    SELECT UserName
    from
      TelCustom.DBO.EscrowUserData


  INSERT INTO #DescAccount (
    AccountName)
    select DISTINCT AccountName
    from #Accounts
  DROP TABLE #Accounts



  UPDATE #DescAccount
  SET TelEscrowAccountName = ed.Username, Email = ed.Email, FirstName = ed.[First Name], LastName = ed.[Last Name],
    Timezoneid  = 1
  FROM CIP.DBO.AD_users ed
  where SUBSTRING(AccountName, 1, 6) = ed.UserName



  UPDATE #DescAccount SET TelEscrowAccountName = SUBSTRING(AccountName, 1, 6) from #DescAccount where
   LEN(LTRIM(RTRIM(AccountName))) >= 6 and
  SUBSTRING(AccountName, 1, 6) in (select AccountName AS ACC from #DescAccount)


  Update #DescAccount
  set TelEscrowAccountName = 'System'
  where Email is null
        AND (LEN(LTRIM(RTRIM(AccountName))) > 6  AND TelEscrowAccountName IS NULL) or (AccountName = 'DEV' or AccountName = 'Manual')
  ;


  Update #DescAccount
  set TelEscrowAccountName = 'System'
  where Email is null
        and  (TelEscrowAccountName = 'DEV' or TelEscrowAccountName = 'Manual')
  ;



  UPDATE #DescAccount SET id = AccountID FROM TELEscrow.DBO.Account ACC WHERE TelEscrowAccountName= ACC.AccountName

INSERT  INTO DBMigration.AccountMapping(TelCustom, TELESscrow, TelEscrowID)
    SELECT AccountName, TelEscrowAccountName, id FROM #DescAccount where AccountName is NOT NULL;
  DROP TABLE #DescAccount
  END
go

