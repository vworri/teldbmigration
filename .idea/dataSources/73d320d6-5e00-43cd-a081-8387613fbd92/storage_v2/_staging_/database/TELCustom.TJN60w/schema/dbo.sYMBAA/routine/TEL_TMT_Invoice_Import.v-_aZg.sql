
--##SVN Revision: 11676 E=N D=TM(RDX) 
CREATE PROCEDURE [dbo].[TEL_TMT_Invoice_Import]
AS

--------------------------------------------------------------------------------------------------------------------------------------
--
-- Created by: Larry Moore
-- Created on: 06/06/17
--
-- Import TMT invoices into the TELCUSTOM database.  
--
-- Changed By       Date      Ticket      Description
--------------------------------------------------------------------------------------------------------------------------------------
-- Larry Moore      06/06/17  57519       Created stored procedure using logic from Randy Peterson's original script.
--                                        This script will be scheduled to run on CTG-SQLAAG02.
-- Larry Moore      03/06/18  CTGIT-1343  TEL has two trucks with the same contract number so I changed the script to pull
--                                        the truck that was most recently updated.
--------------------------------------------------------------------------------------------------------------------------------------

-- EXEC TEL_TMT_Invoice_Import

DECLARE @UserName NVARCHAR(50) ='System'
DECLARE @ChangedDate DATETIME  = GETDATE()


-- Get invoices that were opened in TMT within the last 60 days and are missing from TELCUSTOM..Invoices

INSERT INTO TELCustom.dbo.Invoices
( InvoiceID
 ,InvoiceNumber
 ,UnitNumber
 ,SerialNumber
 ,TractorID
 ,UnitInvoiceID
 ,InvoiceOpenedDate
 ,InvoiceClosedDate    
 ,TotalAmount
 ,DriverID
 ,ContractID
 ,CarrierVendorID 
 ,InvoiceDescription
 ,IsVendor
 ,CreateDate
 ,CreatedBy
 ,UpdateDate
 ,UpdatedBy
)
SELECT 
  a.ORDERID     
 ,a.ORDERNUM
 ,b.UNITNUMBER
 ,b.SERIALNO
-- ,c.TractorID
 ,ISNULL((SELECT TOP 1 c.TractorID FROM TELCustom.dbo.Tractors c (NOLOCK) WHERE c.ContractNumber = con.ContractNumber ORDER BY c.UpdateDate DESC),'')  -- CTGIT-1343
 ,a.UNITID
 ,a.OPENED
 ,a.CLOSED   
 ,a.TOTALAMT 
 ,con.DriverID
 ,con.ContractID
 ,CAST(con.CarrierID AS NVARCHAR) 'CarrierID'
 ,SUBSTRING((SELECT TOP 3 ','+ [DESCRIP] AS [text()] FROM [TFW].[dbo].[ORDERLN] ln (NOLOCK)
		WHERE ln.[ORDERID] = a.[ORDERID] AND ln.[LINETYPE]<>'TAX'
		FOR XML PATH ('')), 2, 1000) AS Descriptions
0
FROM TFW.dbo.ORDERS a (NOLOCK)
INNER JOIN TFW.dbo.UNITS b (NOLOCK) ON a.UNITID = b.UNITID
INNER JOIN TFW.dbo.CUSTOMERS cust (NOLOCK) ON a.CUSTID = cust.CUSTID
INNER JOIN TELCustom.dbo.Contracts con (NOLOCK) ON CONVERT(VARCHAR, con.ContractNumber) = CUSTOMERNAME
INNER JOIN TELCustom.dbo.Drivers dr (NOLOCK) ON con.DriverID = dr.DriversID
--LEFT JOIN  TELCustom.dbo.Tractors c (NOLOCK) ON c.ContractNumber = con.ContractNumber							    -- CTGIT-1343				  
LEFT JOIN  TELCustom.dbo.Invoices cur (NOLOCK) ON cur.InvoiceID = a.ORDERID  
WHERE a.ORDERTYPE = 'INVOICE'
  AND dr.IsTerminated = 0
  AND cur.InvoiceID IS NULL                                -- Invoice does not exist in TELCustomer..Invoices
  AND a.OPENED > DATEADD(d,-60,GETDATE())


-- Update Invoice Closed date in TELCustom if the invoice is closed in TMT and open in TEL    

UPDATE TELCustom.dbo.Invoices 
SET TELCustom.dbo.Invoices.InvoiceClosedDate =  a.Closed    
   ,UpdateDate = @ChangedDate
   ,UpdatedBy  = @UserName
FROM TELCustom.dbo.Invoices ce 
INNER JOIN TFW.dbo.ORDERS a (NOLOCK) ON ce.InvoiceID = a.ORDERID 
LEFT OUTER JOIN TELCustom.dbo.Drivers dr (NOLOCK) ON ce.DriverID = dr.DriversID
WHERE ce.InvoiceClosedDate IS NULL
  AND dr.IsTerminated = 0 
  AND dr.DriversID IS NOT NULL 
  AND a.Closed IS NOT NULL  


-- Update the invoice amount in TEL if it does not match the total in TMT

UPDATE TELCustom.dbo.Invoices 
SET TELCustom.dbo.Invoices.TotalAmount = a.TOTALAMT  
   ,UpdateDate = @ChangedDate
   ,UpdatedBy = @UserName     
FROM TELCustom.dbo.Invoices ce 
INNER JOIN TFW.dbo.ORDERS a (NOLOCK) ON ce.InvoiceID = a.ORDERID
LEFT OUTER JOIN TELCustom.dbo.Drivers dr (NOLOCK) ON ce.DriverID = dr.DriversID
WHERE ce.TotalAmount <> a.TOTALAMT 
  AND dr.IsTerminated = 0
  AND dr.DriversID  IS NOT NULL 
  AND ce.CreateDate > DATEADD(d,-60,GETDATE())

go

