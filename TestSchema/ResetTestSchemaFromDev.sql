TRUNCATE TABLE TransactionHistory;
TRUNCATE table TransactionExceptions;
TRUNCATE TABLE ContractOperator;
TRUNCATE TABLE [AuditLog];
DELETE FROM [Transaction];
TRUNCATE TABLE TransactionCloseOfMonth;
DBCC CHECKIDENT ('dbo.Transaction',RESEED, 0);
DELETE FROM [ContractCarrierUnitNumber];
DBCC CHECKIDENT ('dbo.ContractCarrierUnitNumber',RESEED, 0);
DELETE FROM [Carrier];
DBCC CHECKIDENT ('dbo.Carrier',RESEED, 0);
DELETE FROM [Contract];
DBCC CHECKIDENT ('dbo.Contract',RESEED, 0);
DELETE FROM [Tractor];
DBCC CHECKIDENT ('dbo.Tractor',RESEED,0);
DELETE FROM dbo.[TractorMakeModel];
DBCC CHECKIDENT ('dbo.TractorMakeModel',RESEED,0);
DELETE FROM [Operator];
DBCC CHECKIDENT ('dbo.Operator',RESEED,0);
DELETE FROM dbo.[Account];
DBCC CHECKIDENT ('dbo.[Account]',RESEED,0);

SET IDENTITY_INSERT dbo.Account ON;
INSERT INTO dbo.Account (AccountID, AccountName, FirstName, LastName, Email, TimezoneID, CreatedDate, UpdatedDate)
  select
    AccountID,
    AccountName,
    FirstName,
    LastName,
    Email,
    TimezoneID,
    CreatedDate,
    UpdatedDate
  from TELEscrow.dbo.Account;
SET IDENTITY_INSERT dbo.Account OFF;

SET IDENTITY_INSERT dbo.TractorMakeModel ON;
INSERT INTO dbo.TractorMakeModel (TractorMakeModelID, Make, Model, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
  select
    TractorMakeModelID,
    Make,
    Model,
    CreatedBy,
    CreatedDate,
    UpdatedBy,
    UpdatedDate
  from TELEscrow.dbo.TractorMakeModel;
SET IDENTITY_INSERT dbo.TractorMakeModel OFF;


SET IDENTITY_INSERT dbo.TransactionCloseOfMonth ON;
INSERT INTO dbo.TransactionCloseOfMonth (MonthEndPeriodID, MonthEndPeriodDate, InterestCreditRate, InterestCreditRunDate, CreatedBy, UpdatedBy, UpdatedDate, CreatedDate, PeriodClosed)
  select
    MonthEndPeriodID,
    MonthEndPeriodDate,
    InterestCreditRate,
    InterestCreditRunDate,
    CreatedBy,
    UpdatedBy,
    UpdatedDate,
    CreatedDate,
    PeriodClosed
  from TELEscrow.dbo.TransactionCloseOfMonth;
SET IDENTITY_INSERT dbo.TransactionCloseOfMonth OFF;




SET IDENTITY_INSERT dbo.Tractor ON;
INSERT INTO dbo.Tractor (TractorID, TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage, MileageDate, WarrantyDate, InServiceDate)
  select
TractorID, TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage, MileageDate, WarrantyDate, InServiceDate
  from TELEscrow.dbo.Tractor;
SET IDENTITY_INSERT dbo.Tractor OFF;



SET IDENTITY_INSERT dbo.Contract ON;
INSERT INTO dbo.Contract(ContractID,ContractNumber, ContractStatusID, TractorID, ContractEffectiveDate, MaturityDate, ContractTerminatedDate, RelatedContract, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, EscrowRate)
  select
ContractID,ContractNumber, ContractStatusID, TractorID, ContractEffectiveDate, MaturityDate, ContractTerminatedDate, RelatedContract, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, EscrowRate
  from TELEscrow.dbo.Contract;
SET IDENTITY_INSERT dbo.Contract OFF;



SET IDENTITY_INSERT dbo.Operator ON;
INSERT INTO dbo.Operator(OperatorID, FirstName, LastName, StreetAddress, OperatorBusinessName, PhoneNumber, City, State, Zip, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, FederalIdentificationNumber, DisplayAsBusinessName, Email)
  select
OperatorID, FirstName, LastName, StreetAddress, OperatorBusinessName, PhoneNumber, City, State, Zip, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, FederalIdentificationNumber, DisplayAsBusinessName, Email
  from TELEscrow.dbo.Operator;
SET IDENTITY_INSERT dbo.Operator OFF;


SET IDENTITY_INSERT dbo.ContractOperator ON;
INSERT INTO dbo.ContractOperator(ContractOperatorID, ContractID, OperatorID, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
  select
ContractOperatorID, ContractID, OperatorID, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate
  from TELEscrow.dbo.ContractOperator;
SET IDENTITY_INSERT dbo.ContractOperator OFF;



SET IDENTITY_INSERT dbo.Carrier ON;
INSERT INTO dbo.Carrier(CarrierID, Name, ShortName, DivisionCode, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
  select
CarrierID, Name, ShortName, DivisionCode, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate
  from TELEscrow.dbo.Carrier;
SET IDENTITY_INSERT dbo.Carrier OFF;



SET IDENTITY_INSERT dbo.ContractCarrierUnitNumber ON;
INSERT INTO dbo.ContractCarrierUnitNumber(ContractCarrierUnitNumberID, ContractID, CarrierID, CurrentRelationship, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, UnitNumber, EffectiveDate)
  select
ContractCarrierUnitNumberID, ContractID, CarrierID, CurrentRelationship, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, UnitNumber, EffectiveDate
  from TELEscrow.dbo.ContractCarrierUnitNumber;
SET IDENTITY_INSERT dbo.ContractCarrierUnitNumber OFF;


SET IDENTITY_INSERT dbo.[Transaction] ON;
INSERT INTO dbo.[Transaction](TransactionID,TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate, Comments, TransactionException, CreatedBy, UpdatedBy,CreatedDate, UpdatedDate, TFWInvoiceOrderID, MonthEndPeriodID)
  select
TransactionID,TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate, Comments, TransactionException, CreatedBy, UpdatedBy,CreatedDate, UpdatedDate, TFWInvoiceOrderID, MonthEndPeriodID
  from TELEscrow.dbo.[Transaction];
SET IDENTITY_INSERT dbo.[Transaction] OFF;


SET IDENTITY_INSERT dbo.TransactionHistory ON;
INSERT INTO dbo.TransactionHistory(TransactionHistoryID, TransactionID, TransactionTypeID, TransactionDate, PostDate, TransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate, Comments, TransactionException, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
  select
TransactionHistoryID, TransactionID, TransactionTypeID, TransactionDate, PostDate, TransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate, Comments, TransactionException, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate
  from TELEscrow.dbo.TransactionHistory;
SET IDENTITY_INSERT dbo.TransactionHistory OFF;

SET IDENTITY_INSERT dbo.TransactionExceptions ON;
INSERT INTO dbo.TransactionExceptions(TransactionExceptionID,TransactionTypeID, TractorVIN, TransactionID, TransactionDate, ContractNumber, PostDate, UnitNumber, CarrierName, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate, TFWInvoiceOrderID, Comments, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, MonthEndPeriodID)
  select
TransactionExceptionID,TransactionTypeID, TractorVIN, TransactionID, TransactionDate, ContractNumber, PostDate, UnitNumber, CarrierName, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate, TFWInvoiceOrderID, Comments, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, MonthEndPeriodID
  from TELEscrow.dbo.TransactionExceptions;
SET IDENTITY_INSERT dbo.TransactionExceptions OFF;

