
create type TMWTable_varchar8 as table
(
	KeyField varchar(8)
)
go

create table [__RefactorLog]
(
	OperationKey uniqueidentifier not null
		primary key
)
go

create table ContractStatus
(
	ContractStatusID int identity
		primary key,
	Name varchar(25) not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores all possible contract s', 'SCHEMA', 'dbo', 'TABLE', 'ContractStatus'
go

create table Timezone
(
	TimezoneID int identity
		primary key,
	TimezoneOffset char(6) not null
)
go

exec sp_addextendedproperty 'MS_Description', ' Stores all possible timezones', 'SCHEMA', 'dbo', 'TABLE', 'Timezone'
go

create table TransactionType
(
	TransactionTypeID int identity
		primary key,
	Name nvarchar(50) not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores all possible transactio', 'SCHEMA', 'dbo', 'TABLE', 'TransactionType'
go

create table Account
(
	AccountID int identity
		primary key,
	AccountName nvarchar(25) not null,
	FirstName nvarchar(50) not null,
	LastName nvarchar(50) not null,
	Email nvarchar(254) not null,
	TimezoneID int not null
		constraint FK_Account_ToTimezone
			references Timezone,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores the people and systems ', 'SCHEMA', 'dbo', 'TABLE', 'Account'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'Account', 'COLUMN', 'CreatedDate'
go

create table Carrier
(
	CarrierID int identity
		primary key,
	Name nvarchar(100) not null,
	ShortName nvarchar(50) not null,
	DivisionCode int not null,
	CreatedBy int not null
		constraint FK_CarrierCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_CarrierUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Tracks basic name information ', 'SCHEMA', 'dbo', 'TABLE', 'Carrier'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'Carrier', 'COLUMN', 'CreatedDate'
go

create table TransactionCloseOfMonth
(
	MonthEndPeriodID int identity,
	MonthEndPeriodDate date default dateadd(month,1,getdate()) not null,
	InterestCreditRate numeric(9,8),
	PeriodClosed bit default 0 not null,
	InterestCreditRunDate date,
	CreatedBy int not null
		constraint FK_TransactionCloseOfMonthCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_TransactionCloseOfMonthBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null
)
go

create table ExceptionLog
(
	ExceptionID int identity
		primary key,
	AccountNameID int not null
		constraint FK_ExceptionLog_ToAccountName
			references Account,
	ExceptionDate datetimeoffset not null,
	ExceptionTypeName varchar(150) not null,
	ExceptionMessage varchar(max) not null,
	ExceptionStackTrace varchar(max),
	ExceptionURL nvarchar(max),
	ExceptionQueryString nvarchar(max),
	CreatedDate datetimeoffset default sysdatetimeoffset() not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'ExceptionLog', 'COLUMN', 'CreatedDate'
go

create table Operator
(
	OperatorID int identity
		primary key,
	FirstName nvarchar(50) not null,
	LastName nvarchar(50) not null,
	StreetAddress nvarchar(100),
	OperatorBusinessName nvarchar(50),
	PhoneNumber varchar(50),
	City nvarchar(50),
	State nvarchar(50),
	Zip char(5),
	CreatedBy int not null
		constraint FK_OperatorCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_OperatorUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null,
	FederalIdentificationNumber varchar(4),
	[DisplayAsBusinessName ] bit default 0 not null,
	Email varchar(320)
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores general operator inform', 'SCHEMA', 'dbo', 'TABLE', 'Operator'
go

exec sp_addextendedproperty 'MS_Description', 'Not the Carrier name', 'SCHEMA', 'dbo', 'TABLE', 'Operator', 'COLUMN', 'OperatorBusinessName'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'Operator', 'COLUMN', 'CreatedDate'
go

create table TractorMakeModel
(
	TractorMakeModelID int identity
		primary key,
	Make nvarchar(50) not null,
	Model nvarchar(50) not null,
	CreatedBy int not null
		constraint FK_TractorMKMDCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_TractorMKMDUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores all possible tractor ma', 'SCHEMA', 'dbo', 'TABLE', 'TractorMakeModel'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'TractorMakeModel', 'COLUMN', 'CreatedDate'
go

create table Tractor
(
	TractorID int identity
		primary key,
	TractorMakeModelID int not null
		constraint FK_Tractor_ToMKMD
			references TractorMakeModel,
	TractorVIN char(17) not null,
	TractorYear int not null,
	CreatedBy int not null
		constraint FK_TractorCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_TractorUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null,
	Mileage int,
	MileageDate datetimeoffset default sysdatetimeoffset() not null,
	WarrantyDate datetimeoffset default sysdatetimeoffset() not null,
	InServiceDate datetimeoffset default sysdatetimeoffset() not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores general tractor informa', 'SCHEMA', 'dbo', 'TABLE', 'Tractor'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'Tractor', 'COLUMN', 'CreatedDate'
go

create table Contract
(
	ContractID int identity
		primary key,
	ContractNumber int not null,
	ContractStatusID int not null
		constraint FK_Contract_ToContractStatus
			references ContractStatus,
	TractorID int not null
		constraint FK_Contract_ToTractor
			references Tractor,
	ContractEffectiveDate date not null,
	MaturityDate date not null,
	ContractTerminatedDate date,
	RelatedContract int,
	CreatedBy int not null
		constraint FK_ContractCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_ContractUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null,
	EscrowRate numeric(19,2) default 0.07 not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores general contract inform', 'SCHEMA', 'dbo', 'TABLE', 'Contract'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'Contract', 'COLUMN', 'CreatedDate'
go

create table AuditLog
(
	AuditID int identity
		primary key,
	AccountID int not null
		constraint FK_AuditLog_ToAccountName
			references Account,
	ContractID int
		constraint FK_AuditLog_ToContract
			references Contract,
	OperatorID int
		constraint FK_AuditLog_ToOperator
			references Operator,
	TractorID int
		constraint FK_AuditLog_ToTractorID
			references Tractor,
	CarrierID int
		constraint FK_AuditLog_ToCarrier
			references Carrier,
	ContractCarrierUnitNumberID int,
	AuditDescription nvarchar(max) not null,
	CreatedDate datetimeoffset default sysdatetimeoffset()
)
go

exec sp_addextendedproperty 'MS_Description', 'A generalized audit log intend', 'SCHEMA', 'dbo', 'TABLE', 'AuditLog'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'AuditLog', 'COLUMN', 'CreatedDate'
go

create table ContractCarrierUnitNumber
(
	ContractCarrierUnitNumberID int identity
		primary key,
	ContractID int not null
		constraint FK_CCUNContractID_ToContract
			references Contract,
	CarrierID int not null
		constraint FK_CCUNCarrierID_ToCarrier
			references Carrier,
	CurrentRelationship bit not null,
	CreatedBy int not null
		constraint FK_CCUNCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_CCUNUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null,
	UnitNumber varchar(50),
	EffectiveDate date default getdate() not null
)
go

create table ContractOperator
(
	ContractOperatorID int identity
		primary key,
	ContractID int not null
		constraint FK_ContractOperator_ToContract
			references Contract,
	OperatorID int not null
		constraint FK_ContractOperator_ToOperator
			references Operator,
	CreatedBy int not null
		constraint FK_ContractOperatorCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_ContractOperatorUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null
)
go

create table [Transaction]
(
	TransactionID int identity
		primary key,
	TransactionTypeID int not null
		constraint FK_Transaction_ToTransactionType
			references TransactionType,
	TransactionDate date not null,
	PostDate date not null,
	CurrentTransactionAmount numeric(19,2) not null,
	ContractCarrierUnitNumberID int not null
		constraint FK_Transaction_ToCCUN
			references ContractCarrierUnitNumber,
	InvoiceNumber varchar(50),
	InvoiceClosedDate datetimeoffset,
	Comments nvarchar(max),
	TransactionException bit not null,
	IsDeleted bit constraint Transaction_IsDeleted_default default 0 not null,
	CreatedBy int not null
		constraint FK_TransactionCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_TransactionUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null,
	TFWInvoiceOrderID int,
	MonthEndPeriodID int default 0 not null
)
go

exec sp_addextendedproperty 'MS_Description', 'Stores all possible transactio', 'SCHEMA', 'dbo', 'TABLE', 'Transaction'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'Transaction', 'COLUMN', 'CreatedDate'
go

create index IX_Cover_CurrentBallance
	on [Transaction] (ContractCarrierUnitNumberID, TransactionException, IsDeleted, PostDate, TransactionID, CurrentTransactionAmount)
go

create table TransactionHistory
(
	TransactionHistoryID int identity
		primary key,
	TransactionID int not null
		constraint FK_TransactionHistory_ToTransaction
			references [Transaction],
	TransactionTypeID int not null
		constraint FK_TransactionHistory_ToTransactionType
			references TransactionType,
	TransactionDate date not null,
	PostDate date not null,
	TransactionAmount numeric(19,2) not null,
	ContractCarrierUnitNumberID int not null
		constraint FK_Transaction_ToTractorUnit
			references ContractCarrierUnitNumber,
	InvoiceNumber varchar(50),
	InvoiceClosedDate datetimeoffset,
	Comments nvarchar(max),
	TransactionException bit not null,
	IsDeleted bit not null,
	CreatedBy int not null
		constraint FK_TransactionHistoryCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_TransactionHistoryUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null
)
go

exec sp_addextendedproperty 'MS_Description', 'A specialized type of audit lo', 'SCHEMA', 'dbo', 'TABLE', 'TransactionHistory'
go

exec sp_addextendedproperty 'MS_Description', 'Do not set, server will set.', 'SCHEMA', 'dbo', 'TABLE', 'TransactionHistory', 'COLUMN', 'CreatedDate'
go

create table TransactionExceptions
(
	TransactionExceptionID int identity
		primary key,
	TransactionTypeID int not null
		constraint FK_TransactionException_ToTransactionType
			references TransactionType,
	TractorVIN char(17),
	TransactionID int,
	TransactionDate date,
	ContractNumber int,
	PostDate date,
	UnitNumber nvarchar(50),
	CarrierName nvarchar(100),
	CurrentTransactionAmount numeric(19,2) not null,
	ContractCarrierUnitNumberID int,
	InvoiceNumber varchar(50),
	InvoiceClosedDate datetimeoffset,
	TFWInvoiceOrderID int not null,
	Comments nvarchar(max),
	IsDeleted bit not null,
	CreatedBy int not null
		constraint FK_TransactionExceptionCreatedBy_ToAccountName
			references Account,
	CreatedDate datetimeoffset default sysdatetimeoffset() not null,
	UpdatedBy int not null
		constraint FK_TransactionExceptionUpdatedBy_ToAccountName
			references Account,
	UpdatedDate datetimeoffset default sysdatetimeoffset() not null,
	MonthEndPeriodID int default 0
)
go

CREATE procedure dbo.AddNewMonth
  AS BEGIN

  DECLARE @EOMonth DATE = DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0))

  IF EXISTS( select MonthEndPeriodID FROM dbo.TransactionCloseOfMonth where MonthEndPeriodDate = @EOMonth)
    return
  else
    INSERT INTO dbo.TransactionCloseOfMonth(MonthEndPeriodDate, PeriodClosed ,  CreatedBy, UpdatedBy)
    VALUES (@EOMonth, 0,1,1 )

end
go

create procedure GetDaysInMonth_Helper @StartDate  DATETIME
  AS BEGIN
-- First day of month
Set @StartDate = DATEADD( MONTH, DATEDIFF (MONTH, 0, @StartDate ), 0 )

-- Last day of month
Declare @EndDate DateTime = DATEADD( MONTH, DATEDIFF( MONTH, 0, @StartDate ) + 1, -1 );

WITH DaysInMonth_CTE AS
(
  SELECT @StartDate DateValue
  UNION ALL
  SELECT  DateValue + 1
  FROM    DaysInMonth_CTE
  WHERE   DateValue + 1 <= @EndDate
)
SELECT  DateValue
FROM    DaysInMonth_CTE
OPTION (MAXRECURSION 32)

  END
go

CREATE PROCEDURE InterestCredit @MonthPeriodID INTEGER = NULL, @ContractID INTEGER = Null
  AS BEGIN

------------------------------------------------------------------------------------------------------------------------
-- Get the monthID of interest if none is given
------------------------------------------------------------------------------------------------------------------------

IF @MonthPeriodID IS NULL
    select  @MonthPeriodID = MAX(MonthEndPeriodID) FROM dbo.TransactionCloseOfMonth
    WHERE PeriodClosed = 0 AND MonthEndPeriodDate < getdate();

------------------------------------------------------------------------------------------------------------------------
-- check to see if the month is closed
------------------------------------------------------------------------------------------------------------------------
  DECLARE @MonthClosedStatus VARCHAR(3)
   select @MonthClosedStatus = PeriodClosed FROM dbo.TransactionCloseOfMonth WHERE MonthEndPeriodID = @MonthPeriodID

------------------------------------------------------------------------------------------------------------------------
-- Get the month end date and interest rate for the monthid of interest
------------------------------------------------------------------------------------------------------------------------
  DECLARE @InterestRate NUMERIC(9,8)
  DECLARE @RUNDATE DATETIME
   select @RUNDATE = MonthEndPeriodDate, @InterestRate = (InterestCreditRate / 12) from dbo.TransactionCloseOfMonth WHERE MonthEndPeriodID = @MonthPeriodID

------------------------------------------------------------------------------------------------------------------------
-- create custom transaction message for interest run
------------------------------------------------------------------------------------------------------------------------
  declare @CustomMessage NVARCHAR(max)
   SET @CustomMessage = 'Interest credit run for Month of '+ CAST(DATENAME(m,@RUNDATE) AS VARCHAR(10)) + ' ,' + CAST(datepart(YEAR, @RUNDATE) AS VARCHAR(4)) + ' on ' + CAST(GETDATE() AS VARCHAR(9)) + ' at an interest rate of ' + CAST(@InterestRate AS VARCHAR(10))
------------------------------------------------------------------------------------------------------------------------
-- create needed temp tables
------------------------------------------------------------------------------------------------------------------------

    CREATE TABLE #ContractIDs(
    contract_id  integer,
    Con# INTEGER,

  )
    CREATE TABLE #ActiveContracts(
    conID   INTEGER,
    Con# INTEGER,
    BalanceDate DATETIME
  )


  CREATE TABLE #TransactionSummary(
    conID   INTEGER,
    Contract# INTEGER,
    RollingBalance NUMERIC(9,2),
    Day     DATE,
    CCUID           INTEGER,
  )

  CREATE TABLE #DaysInMonth(
    BalanceDate DATETIME
  )


  CREATE TABLE #InterestCalc(
    AvgDailyBalance NUMERIC(9,2),
    Contract#        INTEGER,
    IntCred         NUMERIC(9,2),
    CCUID           INTEGER,
    TransType       INTEGER,
    MonthID         INTEGER,
  )
------------------------------------------------------------------------------------------------------------------------
-- Get all days in the month of interest
------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #DaysInMonth(BalanceDate)
    EXEC dbo.GetDaysInMonth_Helper  @RUNDATE

------------------------------------------------------------------------------------------------------------------------
-- get all active and pending final accounting contracts or for a specific contract
------------------------------------------------------------------------------------------------------------------------
if @ContractID is null
  INSERT  INTO #ActiveContracts( Con#, BalanceDate)
    SELECT  ContractNumber, BalanceDate FROM dbo.Contract
    left join  #DaysInMonth ON ContractID > 0
    WHERE  ContractStatusID in (1, 2)

else
  INSERT  INTO #ActiveContracts( Con#, BalanceDate)
     SELECT  ContractNumber, BalanceDate FROM dbo.Contract
    left join  #DaysInMonth ON ContractID > 0
    WHERE  ContractID = @ContractID



------------------------------------------------------------------------------------------------------------------------
  -- GET recent contract id
------------------------------------------------------------------------------------------------------------------------
  insert into #ContractIDs(contract_id, con#)
    select Max(ContractID), con#
  from (select distinct Con# from #ActiveContracts ) con
    INNER join dbo.Contract on ContractNumber = con#
  group by Con#

  UPDATE #ActiveContracts SET conID = contract_id
  FROM #ContractIDs WHERE #ActiveContracts.Con# = #ContractIDs.Con#

------------------------------------------------------------------------------------------------------------------------
-- calculate rolling balance for each day of the month
------------------------------------------------------------------------------------------------------------------------
  INSERT  INTO #TransactionSummary( Contract#, RollingBalance, Day)
  select AC.Con#, SUM(CurrentTransactionAmount) , AC.BalanceDate
  from
    dbo.[Transaction] t
  INNER JOIN dbo.ContractCarrierUnitNumber ccu on t.ContractCarrierUnitNumberID = ccu.ContractCarrierUnitNumberID
  INNER join dbo.Contract con on con.ContractID = ccu.ContractID
  INNER JOIN #ActiveContracts  ac ON ac.Con# = con.ContractNumber AND t.PostDate <= ac.BalanceDate
  GROUP BY  ac.Con#, AC.BalanceDate ORDER BY  AC.BalanceDate DESC

update #TransactionSummary set conID = c.ConID
  from #ActiveContracts c where Contract# = Con#

update #TransactionSummary set CCUID = ContractCarrierUnitNumberID
  from dbo.ContractCarrierUnitNumber where conID = ContractID and CurrentRelationship = 1


 ------------------------------------------------------------------------------------------------------------------------
-- calculate interest using rolling balance
------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #InterestCalc(AvgDailyBalance, Contract#, IntCred, CCUID, TransType, MonthID)
  select AVG(RollingBalance), Contract#, AVG(RollingBalance) * @InterestRate, max(CCUID), 6, @MonthPeriodID
  from #TransactionSummary
  GROUP BY Contract#

------------------------------------------------------------------------------------------------------------------------
-- update interest values to zero on negative amounts or amounts < 0.01
------------------------------------------------------------------------------------------------------------------------
update #InterestCalc set IntCred = 0 where IntCred <= 0.01;
 ------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
  MERGE dbo.[Transaction]  as target
  using #InterestCalc as source
  on TransactionTypeID = TransType and MonthID = MonthEndPeriodID AND CCUID = ContractCarrierUnitNumberID
  when matched then UPDATE
    SET
      target.Comments = @CustomMessage,
      target.CurrentTransactionAmount = source.IntCred,
      UpdatedBy = 1,
      UpdatedDate = GETDATE()
  when not matched
    THEN INSERT (TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, TransactionException, MonthEndPeriodID, IsDeleted, CreatedBy, UpdatedBy, CreatedDate, UpdatedDate
  , Comments)
    VALUES (TransType, getdate(), getdate(), IntCred, CCUID, 0,MonthID, 0, 1, 1, GETDATE(), getdate(), @CustomMessage)
    ;

  UPDATE dbo.TransactionCloseOfMonth SET InterestCreditRunDate = GETDATE()
  WHERE MonthEndPeriodID = @MonthPeriodID
  DROP TABLE #ActiveContracts
  drop table #TransactionSummary
  drop table #DaysInMonth
  DROP TABLE #InterestCalc
  DROP TABLE #ContractIDs
end
;
go

CREATE PROCEDURE  DBO.UpdateTractorMileage
  AS BEGIN

update dbo.Tractor set Mileage =
           CAST(M.LASTRDING
        AS INTEGER), MileageDate = LASTRDDAY
FROM TFW.dbo.UNITS U
JOIN TFW.dbo.UNITMTR M ON U.UNITID = M.UNITID  AND M.METERDEFID = 33 -- ECM Meter
WHERE  LEFT(u.SERIALNO, 17) = TractorVIN
end
go

CREATE PROCEDURE dbo.TMTInvoiceImport @end_boundary DATE = NULL, @look_behind INTEGER = 60
AS BEGIN

  ----------------------------------------------------------------------------------------------------------------------
  -- Author: Vera Worri
  -- Purpose: Importing and updating invoices from the TMT system (Housed int he TFW DB) to the Tel Escrow system
  -- Example Execution:
  -- EXEC TMT_AUTOMATED_INVOICE_IMPORT
  --- Methodology:
  -- Collect all contract numbers that are active or pending final accounting
  -- Collect all invoices using those contract numbers as customers withing the arguments time span
  -- DELETE ALL INVOICES that have been removed by TEL in both Legit and exception table
  -- Collect ALL TMT invoices already in TEL Escrow
  -- Split transactions that are in other months but need to be updated
  -- Split the TFW invoices into valid and invalid
  -- Only invoices for the current open month are added into the transaction table
  ----------------------------------------------------------------------------------------------------------------------
  -- Handle default @end_boundary

  if @end_boundary is null
    SET @end_boundary = GETDATE()
  ----------------------------------------------------------------------------------------------------------------------
  --  declare the upper boundary for the look-behind
  ----------------------------------------------------------------------------------------------------------------------
  DECLARE @start_boundary DATE = DATEADD(DAY, @look_behind * -1, @end_boundary);

  ----------------------------------------------------------------------------------------------------------------------
  --  get current monthID
  ----------------------------------------------------------------------------------------------------------------------
  DECLARE @CurrentMonthID INTEGER;
  SELECT @CurrentMonthID = MonthEndPeriodID
  FROM dbo.TransactionCloseOfMonth
  WHERE DATEPART(MONTH, getdate()) = DATEPART(MONTH, MonthEndPeriodDate) AND
        DATEPART(year, getdate()) = DATEPART(YEAR, MonthEndPeriodDate)

  ----------------------------------------------------------------------------------------------------------------------
  --  Create all needed temp tables
  ----------------------------------------------------------------------------------------------------------------------
  CREATE TABLE #Current_TelEscrow_Transactions (
    TFWINVID       INTEGER,
    TransactionID  INTEGER PRIMARY KEY,
    Amount         NUMERIC(9, 2),
    FirstTransdate DATETIME,
    MID            INTEGER
  )


  CREATE TABLE #Current_TelEscrow_TransactionExceptions (
    TFWINVID       INTEGER,
    TransactionID  INTEGER PRIMARY KEY,
    Amount         NUMERIC(9, 2),
    FirstTransdate DATETIME,
    MID            INTEGER
  )
  CREATE TABLE #Contracts (
    Contract#  INTEGER,
    ConID      INTEGER,
    CUSTOMERID INTEGER
  )


  CREATE table #ALL_TFW_INVOICES (
    TransactionDate             date           not null,
    PostDate                    date           not null,
    CurrentTransactionAmount    numeric(19, 2) not null,
    ContractCarrierUnitNumberID int            not null,
    AmountDiff                  numeric(9, 2),
    Invoice#                    varchar(50),
    ClosedDate                  datetimeoffset,
    Comments                    nvarchar(max),
    [OrderID]                   int,
    isSplit                     BIT default 0,
    isOpenMonth                 BIT default 0,
    VIN                         char(24),
    MonthID                     INTEGER
  )


  CREATE table #VALID_TFW_INVOICES (
    TransactionDate             date           not null,
    PostDate                    date           not null,
    CurrentTransactionAmount    numeric(19, 2) not null,
    ContractCarrierUnitNumberID int            not null,
    AmountDiff                  numeric(9, 2),
    Invoice#                    varchar(50),
    ClosedDate                  datetimeoffset,
    Comments                    nvarchar(max),
    [OrderID]                   int,
    isSplit                     BIT default 0,
    isOpenMonth                 BIT default 0,
    VIN                         char(24),
    MonthID                     INTEGER
  )

  CREATE table #INVALID_TFW_INVOICES (
    TransactionDate             date           not null,
    PostDate                    date           not null,
    CurrentTransactionAmount    numeric(19, 2) not null,
    ContractCarrierUnitNumberID int            not null,
    AmountDiff                  numeric(9, 2),
    Invoice#                    varchar(50),
    ClosedDate                  datetimeoffset,
    Comments                    nvarchar(max),
    [OrderID]                   int,
    isSplit                     BIT default 0,
    isOpenMonth                 BIT default 0,
    VIN                         char(24),
    MonthID                     INTEGER
  )

  ------------------------------------------------------------------------------------------------------------------------
  -- Get all Tel Escrow contract information where the contract is active or pending final accounting
  ------------------------------------------------------------------------------------------------------------------------

  INSERT INTO #Contracts (
    Contract#, ConID
  ) select distinct
      ContractNumber,
      ContractID
    FROM dbo.Contract
    where ContractStatusID in (1, 2)
  ------------------------------------------------------------------------------------------------------------------------
  -- Get the TMT customer ID that corresponds to the contract number
  ------------------------------------------------------------------------------------------------------------------------
  UPDATE #Contracts
  SET CUSTOMERID = CUSTID
  from TFW.dbo.CUSTOMERS
  where CAST(Contract# AS VARCHAR(50)) = CUSTOMERNAME

  ------------------------------------------------------------------------------------------------------------------------
  -- Get all TMT invoices for all escrow accounts within the last 60 days or specified date range
  -- N.B. substring function takes a lot of time because it demands an inner query
  ------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #ALL_TFW_INVOICES (TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid)
    SELECT
      o.OPENED,
      o.OPENED,
      o.TOTALAMT,
      ccu.ContractCarrierUnitNumberID,
      o.ORDERNUM,
      todatetimeoffset(o.CLOSEDON, '-04:00'),
      o.ORDERID,
      u.SERIALNO,
      MonthEndPeriodID
    FROM
      TFW.dbo.ORDERS o
      INNER JOIN #Contracts c on o.CUSTID = cast(c.CUSTOMERID as varchar(50))
      INNER join TFW.dbo.UNITS u ON o.UNITID = u.UNITID
      INNER JOIN dbo.ContractCarrierUnitNumber ccu
        on u.UNITNUMBER = ccu.UnitNumber and c.ConID = ccu.ContractID
           and
           o.OPENED >= @start_boundary and o.OPENED <= dateadd(day, 1, @end_boundary) and
           o.ORDERTYPE = 'INVOICE'
      left join dbo.TransactionCloseOfMonth
        ON DATEPART(MONTH, MonthEndPeriodDate) = DATEPART(MONTH, o.OPENED) AND
           DATEPART(year, o.OPENED) = DATEPART(YEAR, MonthEndPeriodDate)
------------------------------------------------------------------------------------------------------------------------


  update #ALL_TFW_INVOICES
  set Comments = SUBSTRING(-- Needed to get more informative comments
      (
        SELECT TOP (3) ', ' + RTRIM(CMP1.DESCRIP) AS [text()]
        FROM
          TFW.dbo.ORDERS AS O1 ( NOLOCK )
          INNER JOIN TFW.dbo.UNITS AS U1 ( NOLOCK ) ON O1.UNITID = U1.UNITID
          INNER JOIN TFW.dbo.ORDERLN AS OLN1 ( NOLOCK ) ON O1.ORDERID = OLN1.ORDERID
          INNER JOIN TFW.dbo.ORDERSEC AS OS1 ( NOLOCK ) ON OLN1.SECTION = OS1.SECTION
          INNER JOIN TFW.dbo.CMPONENT AS CMP1 ( NOLOCK ) ON OS1.COMPCODE = CMP1.CODE
        WHERE
          O1.ORDERNUM = Invoice#
        FOR XML PATH (''), ROOT ('MyString'), TYPE
      ).value('/MyString[1]', 'varchar(max)'), 2, 1000
  )

   update #ALL_TFW_INVOICES
  set Comments = Comments + '( ' + SUBSTRING(-- Needed to get more informative comments
      (
       SELECT  CAST( (COUNT(*) - 3) AS varchar(20))
        FROM
          TFW.dbo.ORDERS AS O1 ( NOLOCK )
          INNER JOIN TFW.dbo.UNITS AS U1 ( NOLOCK ) ON O1.UNITID = U1.UNITID
          INNER JOIN TFW.dbo.ORDERLN AS OLN1 ( NOLOCK ) ON O1.ORDERID = OLN1.ORDERID
          INNER JOIN TFW.dbo.ORDERSEC AS OS1 ( NOLOCK ) ON OLN1.SECTION = OS1.SECTION
          INNER JOIN TFW.dbo.CMPONENT AS CMP1 ( NOLOCK ) ON OS1.COMPCODE = CMP1.CODE
        WHERE
          O1.ORDERNUM = Invoice#
           having count(*) > 3
        FOR XML PATH (''), ROOT ('MyString'), TYPE

      ).value('/MyString[1]', 'varchar(max)'), 2, 1000
  )+ ' additional item(s))'


  ----------------------------------------------------------------------------------------------------------------------
  -- drop contracts table
  ----------------------------------------------------------------------------------------------------------------------
    DROP TABLE #Contracts

  ------------------------------------------------------------------------------------------------------------------------
  -- DELETE ALL INVOICES that have been removed by tel either as qan exception or as an actual transaction
  ------------------------------------------------------------------------------------------------------------------------
  DELETE from #ALL_TFW_INVOICES
  WHERE Invoice# in (select InvoiceNumber
                     from dbo.TransactionExceptions
                     where IsDeleted = 1 AND TransactionID IS NULL)
  ------------------------------------------------------------------------------------------------------------------------
  DELETE from #ALL_TFW_INVOICES
  WHERE Invoice# in (select InvoiceNumber
                     from dbo.[Transaction]
                     where IsDeleted = 1)
  ------------------------------------------------------------------------------------------------------------------------
  -- Get all valid TMT invoices that already exist in  Tel Escrow DB and is not deleted
  ------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #Current_TelEscrow_Transactions (TFWINVID, TransactionID, Amount, FirstTransdate, MID)
    SELECT
      InvoiceNumber,
      MAX(TransactionID),
      SUM(CurrentTransactionAmount),
      Min(TransactionDate),
      MAX(MonthEndPeriodID)
    FROM dbo.[Transaction]
    where TransactionTypeID = 8 and InvoiceClosedDate is null and IsDeleted = 0
    GROUP BY InvoiceNumber
  ------------------------------------------------------------------------------------------------------------------------
  -- Get all invalid TMT invoices that already exist in  Tel Escrow DB and is not deleted
  ------------------------------------------------------------------------------------------------------------------------
  INSERT INTO #Current_TelEscrow_TransactionExceptions (TFWINVID, TransactionID, Amount, FirstTransdate, MID)
    SELECT
      InvoiceNumber,
      MAX(TransactionID),
      SUM(CurrentTransactionAmount),
      Min(TransactionDate),
      MAX(MonthEndPeriodID)
    FROM dbo.[TransactionExceptions]
    where TransactionTypeID = 8 and InvoiceClosedDate is null and IsDeleted = 0
    GROUP BY InvoiceNumber

  ------------------------------------------------------------------------------------------------------------------------
  --For transactions that exist in TelEscrow AS VALID already, get the difference between TelEscrow and TMT/TFW and update the
  -- Month to the most recent month
  ------------------------------------------------------------------------------------------------------------------------
  update #ALL_TFW_INVOICES
  set CurrentTransactionAmount -= Amount , PostDate = getdate(), MonthID = @CurrentMonthID
  from
    #Current_TelEscrow_Transactions
  where TFWINVID = OrderID

------------------------------------------------------------------------------------------------------------------------
  --Update CCU from  In case of discrepency, deffer to TEL Escrow
------------------------------------------------------------------------------------------------------------------------
    UPDATE #ALL_TFW_INVOICES set ContractCarrierUnitNumberID = t.ContractCarrierUnitNumberID
  from dbo.[Transaction] t WHERE Invoice# = InvoiceNumber
  and InvoiceNumber is not null

  ----------------------------------------------------------------------------------------------------------------------
-- Get all valid invoices
  ----------------------------------------------------------------------------------------------------------------------
   INSERT INTO #VALID_TFW_INVOICES (TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid)
     select TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid
    from #ALL_TFW_INVOICES where MonthID = @CurrentMonthID and ContractCarrierUnitNumberID is not null ;
  ----------------------------------------------------------------------------------------------------------------------
-- Get all invalid invoices
  ----------------------------------------------------------------------------------------------------------------------
   INSERT INTO #INVALID_TFW_INVOICES (TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid)
     select TransactionDate, PostDate,comments, CurrentTransactionAmount, ContractCarrierUnitNumberID,
                                     Invoice#, ClosedDate, OrderID, VIN, Monthid
    from #ALL_TFW_INVOICES where MonthID <> @CurrentMonthID or ContractCarrierUnitNumberID is null;
  ----------------------------------------------------------------------------------------------------------------------
  --- Drop unneeded tables
  ----------------------------------------------------------------------------------------------------------------------
    DROP TABLE #ALL_TFW_INVOICES
   DROP TABLE   #Current_TelEscrow_Transactions
  ----------------------------------------------------------------------------------------------------------------------
  -- Merge TFW invoices into Tel Escrow DB for ONLY CURRENT OPEN MONTH: any manual rerunning will put transactions for
  -- previous months into exception for extra vetting
  ----------------------------------------------------------------------------------------------------------------------
  merge dbo.[TransactionExceptions] as target
  using #INVALID_TFW_INVOICES as source
  ----------------------------------------------------------------------------------------------------------------------
  -- joining on Invoice number and Month Period ID
  ----------------------------------------------------------------------------------------------------------------------
  on Invoice# = InvoiceNumber and MonthID = MonthEndPeriodID
  ----------------------------------------------------------------------------------------------------------------------
  when not matched by target  then insert
  (TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber,
   InvoiceClosedDate, Comments, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, [TFWInvoiceOrderID], IsDeleted, TractorVIN, MonthID)
  VALUES (8, source.TransactionDate, source.PostDate,
             CASE WHEN source.isOpenMonth = 1
               then
                 source.CurrentTransactionAmount
             when source.isOpenMonth = 0
               then source.AmountDiff
             end, source.ContractCarrierUnitNumberID,
             source.Invoice#, source.ClosedDate,
             CASE WHEN source.AmountDiff = 0 and isSplit = 1
               then 'No amount changed: Invoice closed this month'
             ELSE source.Comments END, 1, getdate(), 1, getdate(), source.OrderID, 0, VIN, MonthID)
  when not matched by source
    and target.TransactionDate > @start_boundary  and target.TransactionDate < dateadd(day, 1, @end_boundary)
  then update set target.IsDeleted = 1
  when matched
    and (isOpenMonth = 0 or source.ContractCarrierUnitNumberID is null) then update
  set target.CurrentTransactionAmount = source.CurrentTransactionAmount,
    target.PostDate                   = source.PostDate,
    target.Comments                   = source.Comments,
    target.UpdatedDate                = getdate(),
    target.InvoiceClosedDate          = source.ClosedDate,
    target.UpdatedBy                  = 1
  ;



  ------------------------------------------------------------------------------------------------------------------------
-- drop invalid table
    DROP TABLE #INVALID_TFW_INVOICES

  ------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------
  -- Merge TFW invoices into Tel Escrow DB for ONLY CURRENT OPEN MONTH: any manual rerunning will put transactions for
  -- previous months into exception for extra vetting
  ----------------------------------------------------------------------------------------------------------------------
  merge dbo.[Transaction] as target
  using #VALID_TFW_INVOICES as source
  ----------------------------------------------------------------------------------------------------------------------
  -- joining on Order ID where it is for an open month
  ----------------------------------------------------------------------------------------------------------------------
  on Invoice# = InvoiceNumber and MonthID = MonthEndPeriodID
  ----------------------------------------------------------------------------------------------------------------------
  -- when the invoice exists in TFW but not in TelEscrow, or the amounts do not match insert the amended invoice
  -- This should also take care of spit invoices and only insert when the invoice opened date is the CURRENT OPEN MONTH.
  --  There may be some inserts of amounts  = 0 when the amount has not changed but the invoice is closed in a closed
  --  month
  ----------------------------------------------------------------------------------------------------------------------
  when not matched by target then insert
  (TransactionTypeID, TransactionDate, PostDate, CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber,
   InvoiceClosedDate, Comments, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, [TFWInvoiceOrderID], IsDeleted, TransactionException, MonthEndPeriodID)
  VALUES (8, source.TransactionDate, source.PostDate,
             CASE WHEN source.isOpenMonth = 1
               then
                 source.CurrentTransactionAmount
             when source.isOpenMonth = 0
               then source.AmountDiff
             end, source.ContractCarrierUnitNumberID,
             source.Invoice#, source.ClosedDate,
             CASE WHEN source.AmountDiff = 0 and isSplit = 1
               then 'No amount changed: Invoice closed this month'
             ELSE source.Comments END, 1, getdate(), 1, getdate(), source.OrderID, 0, 0, MonthID)
  when not matched by source
   and target.TransactionDate > @start_boundary  and target.TransactionDate < dateadd(day, 1, @end_boundary)
  then update set target.IsDeleted = 1
  when matched then update
  set target.CurrentTransactionAmount = source.CurrentTransactionAmount,
    target.PostDate                   = source.PostDate,
    target.Comments                   = source.Comments,
    target.UpdatedDate                = getdate(),
    target.InvoiceClosedDate          = source.ClosedDate,
    target.UpdatedBy                  = 1
  ;

  ----------------------------------------------------------------------------------------------------------------------
  -- final drop
  ----------------------------------------------------------------------------------------------------------------------

  DROP TABLE #VALID_TFW_INVOICES


END
go
------------------------------------------------------------------------------------------------------------------------\
--INSERT data
------------------------------------------------------------------------------------------------------------------------
-- Insert Timezones
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-04:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-03:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-03:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-02:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-01:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+00:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+01:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ('+02:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+03:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+03:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+04:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+04:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+05:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+05:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+05:45');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+06:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+06:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+07:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+08:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+08:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+08:45');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+09:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+09:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+10:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+10:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+11:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+12:45');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+13:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '+14:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-12:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-11:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-10:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-09:30');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-08:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-07:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-06:00');
INSERT INTO dbo.Timezone ( TimezoneOffset) VALUES ( '-05:00');
------------------------------------------------------------------------------------------------------------------------
-- Insert Accounts
INSERT INTO dbo.Account ( AccountName, FirstName, LastName, Email, TimezoneID, CreatedDate, UpdatedDate) VALUES ('SYSTEM', 'System', 'System', 'System@badData.com', 1, '2018-07-20 14:09:51.1943498 -04:00', '2018-07-20 14:09:51.1943498 -04:00');
------------------------------------------------------------------------------------------------------------------------
-- Insert Tractor Make Model
INSERT INTO dbo.ContractStatus ( Name) VALUES ( 'Active');
INSERT INTO dbo.ContractStatus ( Name) VALUES ( 'Pending Final Accounting');
INSERT INTO dbo.ContractStatus ( Name) VALUES ( 'Terminated');


