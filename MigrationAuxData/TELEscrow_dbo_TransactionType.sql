INSERT INTO dbo.TransactionType ( Name) VALUES ('Escrow Deposit');
INSERT INTO dbo.TransactionType ( Name) VALUES ('Invoice');
INSERT INTO dbo.TransactionType ( Name) VALUES ('Settlement');
INSERT INTO dbo.TransactionType ( Name) VALUES ( 'Promissory Note');
INSERT INTO dbo.TransactionType ( Name) VALUES ( 'Transfer');
INSERT INTO dbo.TransactionType ( Name) VALUES ('Interest Credit');
INSERT INTO dbo.TransactionType ( Name) VALUES ('Other');
INSERT INTO dbo.TransactionType ( Name) VALUES ('TMT Invoice');