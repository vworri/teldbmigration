from db import *
import pandas
from sqlalchemy import create_engine
import pyodbc
class CreateDBMigrationSchema:
    def __init__(self):
        # self.file_path = input("please put path of desired AC ContractInfoDwn:\n")
        self.runCreationScript()
        self.fillInAuxData()
        self.MigrateData()
        # self.refreshTestSchema()
    @staticmethod
    def runCreationScript():
        with open('MigrationSchema\CreateDBMigrationSchema.sql', 'r') as f:
            sql = f.read().split('go')
            con = get_sqlserver(isTest=True)
            cur = con.cursor()
            for statement in sql:
                try:
                    cur.execute(statement)
                except:
                       print(statement)
                       con.rollback()
                       return
            cur.execute("{call DBMIGRATION.AccountMappingTable}")
            con.commit()
            cur.execute("{call DBMIGRATION.CarrierMappingTable}")
            con.commit()
            con.close()

    def fillInAuxData(self):
        engine = create_engine("mssql://", creator=get_sqlserver)
        leasworks_contract_info = pandas.read_csv(r"\\CTG-FILESVR\Sales-Used\John Pardue\IFS\2018\July 2018\Nightly Reports\TEL_Files_07-02-2018_12-08\AcContractInfoDwn180701.csv", usecols=["Contract Number", "Date Booked", "Commencement Date", "Maturity Date", "Schedule Number", "Termination Date"], parse_dates=["Date Booked", "Commencement Date", "Maturity Date", "Termination Date"])
        indx = leasworks_contract_info.groupby(["Contract Number"], sort=False)["Schedule Number"].transform(max) == leasworks_contract_info["Schedule Number"]
        Cleaned_leasworks_contract_info = leasworks_contract_info[indx]
        Cleaned_leasworks_contract_info.to_sql('LeaseWorksAccountInfo', engine,schema ='DBMigration', if_exists='replace', index=False, chunksize=100)
        leasworks_asset_info = pandas.read_csv(r"\\CTG-FILESVR\Sales-Used\John Pardue\IFS\2018\July 2018\Nightly Reports\TEL_Files_07-06-2018_12-06\BkAssetDwn180705.csv", usecols=["Contract Number", "Serial Number","Schedule Number", "Make", "Model"], encoding='latin')
        indx = leasworks_asset_info.groupby(["Contract Number"], sort=False)["Schedule Number"].transform(max) == leasworks_asset_info["Schedule Number"]
        Cleaned_leasworks_asset_info = leasworks_asset_info[indx]
        Cleaned_leasworks_asset_info.to_sql('LeaseWorksAssetInfo', engine, schema='DBMigration',
                                               if_exists='replace', index=False, chunksize=100)

    def truncateTables(self):
        with open('MigrationSchema/Truncate_DBO.sql', 'r') as f:
            sql = f.read().split(';')
            con = get_sqlserver()
            cur = con.cursor()
            for statement in sql:
                try:
                    cur.execute(statement)
                except:
                    print(statement[:100])
                    con.rollback()
                    return
            con.commit()
            con.close()
    def SetDefaultStaticData(self, cur):
        with open('TestSchema/addStaticData.sql', 'r') as f:
            sql = f.read().split(';')
            for statement in sql:
                cur.execute(statement)


    def refreshDEVSchema(self):
        print("resetting test")
        with open('DevSchema/ResetDev.sql', 'r') as f:
            sql = f.read().split(';')
            con = get_sqlserver(isTest=True)
            cur = con.cursor()
            for statement in sql:
                try:
                    cur.execute(statement)
                except:
                    print(statement[:100])
                    con.rollback()
                    return
            con.commit()
            con.close()


    def MigrateData(self):
        con = get_sqlserver(isTest=True)
        cur = con.cursor()
        print("Started: Account Migration")
        cur.execute("{call DBMIGRATION.AccountMigration}")
        con.commit()
        print("Started: inserting default static data")
        self.SetDefaultStaticData(cur)
        print("Started: Migrate Tractor")
        cur.execute("{call DBMIGRATION.MigrateTractor}")
        print("Started: Migrate Contracts")
        cur.execute("{call DBMIGRATION.MigrateContracts}")
        print("Started: ContractCarrier Unit Relationship")
        cur.execute("{call DBMIGRATION.ContractCarrierUnitRelationship}")
        print("Started: Migrate Drivers To Operator")
        cur.execute("{call DBMIGRATION.MigrateDriversToOperator}")
        print("Started: MaCting Contract To Operator")
        cur.execute("{call DBMIGRATION.ContractOperatorMatching}")
        print("Started: BackFill Closed Date")
        cur.execute("{call DBMigration.BackFillClosedDate}")
        print("Started: Migrate Transactions")
        cur.execute("{call DBMIGRATION.MigrateTransactions}")
        print("Started: Migrate Transaction History")
        cur.execute("{call DBMIGRATION.MigrateTransactionHistory}")
        con.commit()
        con.close()

           




if __name__ == '__main__':
    CreateDBMigrationSchema()




