import pyodbc
from collections import namedtuple

def get_sqlserver(isTest=True):
    """
    Creates a connection and cursor to the MS SQL Server database.
    :param as_dict: boolean
    :return: conn, cur
    """
    if isTest:
        conn_str = (
        r'Driver={SQL Server};'
        r'Server=CTG-SQLLAB01;'
        r'Trusted_Connection=yes;'
        r'Database=TelTestOnly'
        )
    else:
        conn_str = (
            r'Driver={SQL Server};'
            r'Server=CTG-SQLLAB01;'
            r'Trusted_Connection=yes;'
            r'Database=TelEscrow'
        )
    print(conn_str)
    cnxn = pyodbc.connect(conn_str)
    return cnxn



