CREATE PROCEDURE DBMigration.ContractCarrierUnitRelationship
as begin
CREATE TABLE EscrowUnitNumbers(
  unit# nvarchar(50),
  vin#   varchar(17),
)

CREATE TABLE RankedEscrowUnitNumbers(
  rowId INTEGER,
  unit# nvarchar(50),
  vin#   varchar(17),
)
CREATE TABLE #EscrowUnitNumberCarriers(
  Con#    integer,
  conID integer,
  carrier integer,
  unit# nvarchar(50),
  vin#   varchar(17),
  last_trans_date DATE,
  first_trans_date DATE,
  isCurrent BIT
)

  CREATE TABLE #EscrowContractMaxDate(
  Contract#    integer,
  max_date  DATE
)

CREATE TABLE #EscrowUnitNumberCarriers(
  Con#    integer,
  conID integer,
  carrier integer,
  unit# nvarchar(50),
  vin#   varchar(17),
  last_trans_date DATE,
  first_trans_date DATE,
  isCurrent BIT,
  Tractor_Make nvarchar(50),
  Tractor_Model nvarchar(50),
  Tractor_Year int

)


------------------------------------------------------------------------------------------------------------------------
  -- collect bear bones contract information
insert into EscrowUnitNumbers(unit#, vin#)
select distinct UNITNUMBER, left(SERIALNUMBER, 17) from TELCustom.dbo.Invoices
where SERIALNUMBER is not null

insert into EscrowUnitNumbers(unit#, vin#)
  select distinct UnitNumber, left(vin, 17) from TELCustom.dbo.Payments
  WHERE NOT EXISTS(select unit#, vin from EscrowUnitNumbers where unit# = UnitNumber )
and vin is not null

insert into RankedEscrowUnitNumbers(rowId, unit#, vin#)
  select row_number() over(PARTITION BY unit# order by vin#), unit#, vin#
  from EscrowUnitNumbers;
  DROP TABLE EscrowUnitNumbers;


 INSERT  INTO #EscrowUnitNumberCarriers(carrier, unit#, vin#)
   select dbo.UnitNumberToCarrier(unit#), unit#, vin#
   from RankedEscrowUnitNumbers where rowId =1
drop table RankedEscrowUnitNumbers;

update #EscrowUnitNumberCarriers set Con# = ContractNumber
  from TELCustom.dbo.Payments p,
    TELCustom.dbo.Contracts c where p.UnitNumber = unit#
  and c.ContractID = p.ContractID


update #EscrowUnitNumberCarriers set Con# = ContractNumber
  from TELCustom.dbo.Payments p,
    TELCustom.dbo.Drivers d where p.UnitNumber = unit#
  and d.DriversID = p.DriverID and Con# is null


update #EscrowUnitNumberCarriers set Con# = ContractNumber
  from TELCustom.dbo.Invoices i,
    TELCustom.dbo.Drivers d where i.UnitNumber = unit#
  and d.DriversID = i.DriverID and Con# is null

update #EscrowUnitNumberCarriers set  last_trans_date = max_d8,
  first_trans_date = min_d8
 from (select UnitNumber, min(WeekOfPayment) min_d8, max(WeekOfPayment) max_d8 from TELCustom.dbo.Payments
 group by UnitNumber) p
where p.UnitNumber = unit#

  INSERT INTO #EscrowContractMaxDate(Contract#, max_date)
    SELECT  Con#, max(last_trans_date) FROM #EscrowUnitNumberCarriers
  GROUP BY Con#

  update #EscrowUnitNumberCarriers SET  isCurrent = 1
  from #EscrowContractMaxDate e where e.Contract# = con#
  and e.max_date = last_trans_date
   update #EscrowUnitNumberCarriers SET  isCurrent = 0
  where isCurrent is null

------------------------------------------------------------------------------------------------------------------------
-- fill in tractor info
CREATE TABLE #Tractors(
  vin#   varchar(17),
  last_trans_date DATE,
  first_trans_date DATE,
  isCurrent BIT,
  Tractor_Make nvarchar(50),
  Tractor_Model nvarchar(50),
  TMMID INTEGER,
  Tractor_Year integer,
  Mileage INTEGER,
  MileageD8 DATETIME,
  CreBy   INTEGER,
  UpBy    integer,
  CreD8   DATETIME,
  UpD8    DATETIME
)

  INSERT  INTO #Tractors(vin#)
    select distinct vin# from #EscrowUnitNumberCarriers

update #Tractors set Tractor_Make = cast(Make as varchar(50)), Tractor_Model =  cast(Model as varchar(50))
  from DBMigration.LeaseWorksAssetInfo where vin# = LEFT(CAST([Serial Number] AS VARCHAR(20), 17))


  UPDATE #Tractors
  SET UpBy = isnull(t.UpdatedBy, 1), UpD8 = ISNULL(t.UpdateDate, getdate()), CreBy = isnull(CreatedBy,1), CreD8 = t.CreateDate
  FROM TELCustom.dbo.Tractors t
  WHERE vin# = left(VIN, 17)

    UPDATE #Tractors
  SET Tractor_Year = vy.TractorYear
  from TELCustom.dbo.VinYear vy
  WHERE SUBSTRING(vin#, 10, 1) = VINCharacter


  update #Tractors
  set TMMID = tmm.TractorMakeModelID
  from dbo.TractorMakeModel TMM
  WHERE
    CHARINDEX(SUBSTRING(REPLACE(Tractor_Model, 'new ', ''), 1, 2), TMM.Model) > 0
    AND TMM.Make = Tractor_Make

    update #Tractors
  set Mileage =
  CAST(M.LASTRDING
       AS INTEGER), MileageD8 = LASTRDDAY
  FROM TFW.dbo.UNITS U
    JOIN TFW.dbo.UNITMTR M ON U.UNITID = M.UNITID AND M.METERDEFID = 33 -- ECM Meter
  WHERE left(u.SERIALNO, 17) = vin#


  UPDATE #Tractors
  set UpBy = ISNULL(TELEscrowId, 1)
  from DBMigration.AccountMapping
  where TelCustom = UpBy


  UPDATE #Tractors
  set CreBy  = ISNULL(TELEscrowId, 1)
  from DBMigration.AccountMapping
  where TelCustom = CreBy



  MERGE dbo.Tractor as t
  using #Tractors as s
  on t.TractorVIN = s.vin#
  when not matched
  then insert (TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage, MileageDate)
  VALUES (s.TMMID, s.vin#, s.Tractor_Year, CreBy, CreD8,
         UpBy, UpD8, s.Mileage, MileageD8)
  ;

  drop TABLE #Tractors

-----------------------------------------------------------------------
--- Migrate Contracts
  CREATE TABLE #Contracts(
    Contract# INTEGER,
    ContractStatusName varchar(25),
    ContractStatus INTEGER,
    TractorID INTEGER,
    Eff_D8    DATE,
    Mat_d8 DATE,
    TermDate  DATE,
    CreBy       INTEGER,
    UpBy        INTEGER,
    Cre_d8      DATETIME,
    UP_D8       DATETIME
  )

insert into #Contracts(Contract#, TractorID)
  select distinct Con#, TractorID
  from #EscrowUnitNumberCarriers
  left join dbo.Tractor on vin# = TractorVIN

update #Contracts set Eff_D8 = [Commencement Date], Mat_d8 = [Maturity Date], TermDate = CASE WHEN [Termination Date] = '01/01/1800'
  THEN NULL
  ELSE [Termination Date]
  end
  from DBMigration.LeaseWorksAccountInfo
  where Contract# = [Contract Number]


  UPDATE #Contracts
  SET UpBy = ISNULL(t.UpdatedBy, 1), UP_D8 = ISNULL(t.UpdateDate, getdate()), CreBy = isnull(t.CreatedBy, 1), Cre_d8 = t.CreateDate
  FROM TELCustom.dbo.Contracts t
  WHERE ContractNumber = Contract#

  update #Contracts set ContractStatusName =
    CASE WHEN
      TermDate is not null then 'Active'
      when DATEADD(day,45, TermDate) < getdate()
      then 'Terminated'
      else 'Pending Final Accounting'
      end
  UPDATE #Contracts SET ContractStatus = ContractStatusID
    FROM dbo.ContractStatus WHERE Name = ContractStatusName

MERGE dbo.Contract AS T
  USING #Contracts as s
on T.ContractNumber = S.Contract#
when not matched by target then
insert
(ContractNumber, ContractStatusID, TractorID,
 ContractEffectiveDate, MaturityDate,
 ContractTerminatedDate, CreatedBy, CreatedDate,
UpdatedBy, UpdatedDate, EscrowRate)
VALUES (s.Contract#, s.ContractStatus, s.TractorID,
 s.Eff_D8,  s.Mat_d8,
 s.TermDate, CreBy,  Cre_d8,
UpBy, UP_D8, 0.07)
;

drop table #Contracts
--------------------------------------------------------------------------------------------------------------
  update #EscrowUnitNumberCarriers set conID = ContractID
  from dbo.Contract where con# = ContractNumber

---------------------------------------------------------------------------------------------------------
  merge dbo.ContractCarrierUnitNumber as t
  using #EscrowUnitNumberCarriers as s
  on t.UnitNumber = s.UNIT# and t.ContractID = s.ConId and t.CarrierID = s.carrier
  when not matched
  then insert (ContractID, CarrierID, CurrentRelationship, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, UnitNumber)
  values (s.ConId, s.carrier, s.isCurrent, 1, sysdatetimeoffset(), 1, sysdatetimeoffset(), s.UNIT#);


DROP TABLE #EscrowUnitNumberCarriers
end