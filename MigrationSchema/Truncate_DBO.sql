TRUNCATE TABLE TransactionHistory;
TRUNCATE table TransactionExceptions;
TRUNCATE TABLE ContractOperator;
TRUNCATE TABLE [AuditLog];
DELETE FROM [Transaction];
DBCC CHECKIDENT ('dbo.Transaction',RESEED, 0);
DELETE FROM [ContractCarrierUnitNumber];
DBCC CHECKIDENT ('dbo.ContractCarrierUnitNumber',RESEED, 0);
DELETE FROM [Contract];
DBCC CHECKIDENT ('dbo.Contract',RESEED, 0);
DELETE FROM [Tractor];
DBCC CHECKIDENT ('dbo.Tractor',RESEED,0);
DELETE FROM [Operator]
DBCC CHECKIDENT ('dbo.Operator',RESEED,0)
DELETE FROM dbo.[Account]
DBCC CHECKIDENT ('dbo.[Account]',RESEED,0)


