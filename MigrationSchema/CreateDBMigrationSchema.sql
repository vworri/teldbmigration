create SCHEMA DBMIGRATION

create table DBMIGRATION.AccountMapping
(
	TelCustom varchar(50),
	TELESscrow varchar(50),
	TelEscrowID int
)
go

create table DBMIGRATION.TractorCleanUpException
(
	TractorVIN char(20),
	TMake varchar(75),
	TModel varchar(75),
	TractorYear int,
	Mileage int,
	CreateDt datetimeoffset,
	UpdateDt datetimeoffset,
	TractorMakeModelID int,
	UpdatedBy varchar(75),
	CreateedBy varchar(75),
	ExecptionCode varchar(60)
)
go

create table DBMIGRATION.TelCustomDriversException
(
	Ranking int,
	FirstName nvarchar(50),
	LastName nvarchar(50),
	StreetAddress nvarchar(100),
	OperatorCompanyName nvarchar(50),
	PhoneNumber varchar(50),
	City nvarchar(50),
	State nvarchar(50),
	Zip char(5),
	CreatedBy nvarchar(50),
	CreatedDate datetimeoffset,
	UpdatedBy nvarchar(50),
	UpdatedDate datetimeoffset,
	FederalIdentificationNumber varchar(4),
	ExceptionDescription varchar(200)
)
go

create table DBMIGRATION.ContractExceptions
(
	Ranking int,
	driverid int,
	ContractNumber int not null,
	ContractStatusID int,
	TelCustomTractorID int,
	TelEscrowTractorId int,
	TVIN varchar(20),
	ContractEffectiveDate date,
	MaturityDate date,
	ContractTerminatedDate date,
	RelatedContract int,
	CreatedBy varchar(50),
	CreatedDate datetimeoffset,
	UpdatedBy varchar(500),
	UpdatedDate datetimeoffset,
	creid int,
	UPID int,
	ExceptionMessage varchar(100)
)
go

create table CarrierMapping
(
	TELCUSTID int,
	TELESC int
)
go

create table DBMIGRATION.TractorRelExceptions
(
	ssn4 varchar(4),
	Last varchar(50),
	tvin varchar(17),
	[UNIT#] varchar(580),
	[Con#] int,
	Old_CarrierId int,
	TelEscrowCarrierNumber int,
	MaxContract int,
	isTermiated bit,
	isCurrent bit,
	contractID int,
	ExceptionMessage varchar(50)
)
go

create table DBMIGRATION.TransactionsExceptions
(
	[Unit#] varchar(50),
	[Contract#] int,
	ssn4 int,
	Last varchar(50),
	TransactionDate datetimeoffset,
	InvoiceClosed datetimeoffset,
	InvoiceNumber nvarchar(100),
	TransactionDesc varchar(500),
	Comments varchar(500),
	CarrID int,
	Amount money,
	CreatedBy nvarchar(100),
	CreatedOn datetimeoffset,
	UpdatedBy nvarchar(100),
	UpdatedOn datetimeoffset,
	PayType int,
	RecID int identity,
	oPID int,
	ConID int,
	TransID int,
	ccuID int,
	upID int,
	CreID int,
	ExeptionMessage varchar(200)
)
go

create table DBMIGRATION.ConOpExceptions
(
	[FedID#] varchar(4),
	lastname varchar(50),
	driversLic varchar(50),
	vin varchar(20),
	[con#] int,
	ExceptionsMessage varchar(500)
)
go

create table DBMIGRATION.LeaseWorksAccountInfo
(
	[Contract Number] bigint,
	[Schedule Number] bigint,
	[Date Booked] datetime,
	[Commencement Date] datetime,
	[Maturity Date] datetime,
	[Termination Date] datetime
)
go

create table DBMIGRATION.LeaseWorksAssetInfo
(
	[Contract Number] bigint,
	[Schedule Number] bigint,
	[Serial Number] text,
	Make text,
	Model text
)
go



CREATE PROCEDURE DBMigration.AccountMigration
As BEGIN


  Create Table #AccountS (
    AccountName Varchar(100),
    AccountKey  VARCHAR(100),
    Email       VARCHAR(50),
    FirstName   Varchar(50),
    LastName    Varchar(50),
    Timezoneid  INTEGER DEFAULT 1,
    CreateDt    DATETIMEOFFSET,
    UpdateDt    DATETIMEOFFSET
  )

  Create Table #CleanedAccount (
    AccountName Varchar(100),
    Email       VARCHAR(50),
    FirstName   Varchar(50),
    LastName    Varchar(50),
    Timezoneid  INTEGER DEFAULT 1,
    CreateDt    DATETIMEOFFSET,
    UpdateDt    DATETIMEOFFSET
  )



  INSERT INTO #Accounts (
    AccountName,
    AccountKey)
    select DISTINCT TelCustom, TELESscrow
    from DBMigration.AccountMapping

  UPDATE #Accounts
  SET  Email = ed.Email, FirstName = ed.[First Name], LastName = ed.[Last Name], AccountName = ed.Username,
    Timezoneid  = 1
  FROM CIP.DBO.AD_users ed
  where SUBSTRING(AccountKey, 1, 6) = ed.UserName

  Update #Accounts
  set FirstName = AccountKey , LastName = AccountKey, Email = AccountKey + '@badData.com'
  where FirstName is Null;


  update #Accounts
  set CreateDt = TODATETIMEOFFSET(ud.CreateDate, '-04:00'), UpdateDt = TODATETIMEOFFSET(ud.UpdateDate, '-04:00')
  from TELCustom.dbo.EscrowUserData ud
  Where AccountKey = ud.UserName;

UPDATE #Accounts SET UpdateDt = CreateDt WHERE UpdateDt IS NULL

  UPDATE #Accounts SET CreateDt = SYSDATETIMEOFFSET ( ) , UpdateDt = SYSDATETIMEOFFSET ( )  where CreateDt IS NULL

UPDATE #AccountS set AccountName = 'SYSTEM' where AccountKey in ('SYSTEM', 'MANUAL')

insert into #CleanedAccount(AccountName, Email, FirstName, LastName, CreateDt, UpdateDt)
    select distinct AccountName, Email, FirstName, LastName, CreateDt, UpdateDt from #Accounts





  MERGE dbo.Account as target
  USING #CleanedAccount as source
  on target.AccountName = source.AccountName
  WHEN not matched  then
    INSERT (AccountName, FirstName, LastName, Email, Timezoneid, CreatedDate, UpdatedDate)
    values (source.AccountName, source.FirstName, source.LastName, source.Email, source.Timezoneid, source.CreateDt,
            source.UpdateDt)
  WHEN MATCHED THEN
    update
  set
    FirstName = source.FirstName,
    LastName = source.LastName,
    Email = source.Email,
    Timezoneid = source.TimezoneID,
    CreatedDate = source.CreateDt,
    UpdatedDate = source.UpdateDt;


DROP TABLE #Accounts
DROP TABLE #CleanedAccount




END
go



CREATE procedure DBMigration.AccountMappingTable
  as begin



  Create Table #DescAccount (
    AccountName Varchar(100),
    TelEscrowAccountName  VARCHAR(100),
    Email       VARCHAR(50),
    FirstName   Varchar(50),
    LastName    Varchar(50),
    Timezoneid  INTEGER DEFAULT 1,
    CreateDt    DATETIMEOFFSET,
    UpdateDt    DATETIMEOFFSET,
    id  INTEGER
  )


  Create Table #Accounts (
    AccountName Varchar(100),
    found_in    VARCHAR(100)
  )

 delete  from DBMigration.AccountMapping;
  INSERT INTO #Accounts (
    AccountName,
    found_in
  )
    SELECT
      UpdatedBy,
      'companies' AS [Users]
    FROM
      TelCustom.DBO.Companies WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'companies' AS [Users]
    FROM
      TelCustom.DBO.Companies WITH ( NOLOCK )


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'Contracts' AS [Users]
    FROM
      TelCustom.dbo.Contracts WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'contracts' AS [Users]
    FROM
      TelCustom.dbo.Contracts WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'drivers' AS [Users]
    FROM
      TelCustom.DBO.Drivers


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'drivers' AS [Users]
    FROM
      TelCustom.DBO.Drivers WITH ( NOLOCK )


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'IntCreds' AS [Users]
    FROM
      TelCustom.DBO.InterestCredits WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'IntCreds' AS [Users]
    FROM
      TelCustom.DBO.InterestCredits WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CREATEDBY,
      'Invoices' AS [Users]
    FROM
      TelCustom.DBO.Invoices WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UPDATEDBY,
      'Invoices' AS [Users]
    FROM
      TelCustom.DBO.Invoices WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'payments' AS [Users]
    FROM
      TelCustom.DBO.Payments WITH ( NOLOCK )

  insert into #Accounts (
    AccountName, found_in)
    SELECT
      CreatedBy,
      'payments' AS [Users]
    FROM
      TelCustom.DBO.Payments WITH ( NOLOCK )


  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      UpdatedBy,
      'tractors' AS [Users]
    FROM
      TelCustom.DBO.Tractors WITH ( NOLOCK )

  INSERT INTO #Accounts (AccountName, found_in)
    SELECT
      CreatedBy,
      'tractors' AS [Users]
    FROM
      TelCustom.DBO.Tractors WITH ( NOLOCK )

  INSERT INTO #Accounts (
    AccountName)
    SELECT UserName
    from
      TelCustom.DBO.EscrowUserData


  INSERT INTO #DescAccount (
    AccountName)
    select DISTINCT AccountName
    from #Accounts
  DROP TABLE #Accounts



  UPDATE #DescAccount
  SET TelEscrowAccountName = ed.Username, Email = ed.Email, FirstName = ed.[First Name], LastName = ed.[Last Name],
    Timezoneid  = 1
  FROM CIP.DBO.AD_users ed
  where SUBSTRING(AccountName, 1, 6) = ed.UserName



  UPDATE #DescAccount SET TelEscrowAccountName = SUBSTRING(AccountName, 1, 6) from #DescAccount where
   LEN(LTRIM(RTRIM(AccountName))) >= 6 and
  SUBSTRING(AccountName, 1, 6) in (select AccountName AS ACC from #DescAccount)


  Update #DescAccount
  set TelEscrowAccountName = 'System'
  where Email is null
        AND (LEN(LTRIM(RTRIM(AccountName))) > 6  AND TelEscrowAccountName IS NULL) or (AccountName = 'DEV' or AccountName = 'Manual')
  ;


  Update #DescAccount
  set TelEscrowAccountName = 'System'
  where Email is null
        and  (TelEscrowAccountName = 'DEV' or TelEscrowAccountName = 'Manual')
  ;



  UPDATE #DescAccount SET id = AccountID FROM dbo.Account ACC WHERE TelEscrowAccountName= ACC.AccountName

INSERT  INTO DBMigration.AccountMapping(TelCustom, TELESscrow, TelEscrowID)
    SELECT AccountName, TelEscrowAccountName, id FROM #DescAccount where AccountName is NOT NULL;
  DROP TABLE #DescAccount
  END
go



CREATE PROCEDURE DBMigration.MigrateTractor
AS BEGIN
  DELETE FROM DBMigration.TractorCleanUpException
  create table #TractorCleanUp
  (
    TractorVIN         varchar(17),
    TMake              VARCHAR(75),
    TModel             VARCHAR(75),
    TractorYear        int,
    Mileage            int,
    CreateDt           DATETIMEOFFSET,
    UpdateDt           DATETIMEOFFSET,
    TractorMakeModelID INTEGER,
    UpdatedBy          VARCHAR(75),
    CreatedBy          VARCHAR(75),
    UpID               INTEGER,
    CreID              INTEGER
  )


  create table #TractorCleanUpException
  (
    TractorVIN         char(20),
    TMake              VARCHAR(75),
    TModel             VARCHAR(75),
    TractorYear        int,
    Mileage            int,
    CreateDt           DATETIMEOFFSET,
    UpdateDt           DATETIMEOFFSET,
    TractorMakeModelID INTEGER,
    UpdatedBy          VARCHAR(75),
    CreateedBy         VARCHAR(75),
    ExecptionCode      VARCHAR(60)
  )


  INSERT INTO #TractorCleanUp (
    TractorVIN,
    tMake,
    tModel
  )
    SELECT
      DISTINCT
      (left(cast(lw.[Serial Number] as varchar(50)), 17)),
      cast(lw.Make as varchar(50)),
      cast(lw.Model as varchar(50))
    FROM TELCustom.dbo.Contracts c
      LEFT JOIN DBMigration.LeaseWorksAssetInfo lw ON c.ContractNumber = lw.[Contract Number]


  UPDATE #TractorCleanUp
  SET UpdatedBy = t.UpdatedBy, UpdateDt = t.UpdateDate, CreatedBy = t.CreatedBy, CreateDt = t.CreateDate
  FROM TELCustom.dbo.Tractors t
  WHERE TractorVIN = VIN

  INSERT INTO #TractorCleanUpExCeption (
    TractorVIN,
    TMake,
    TModel,
    UpdatedBy,
    UpdateDt,
    CreateDt,
    CreateedBy,
    ExecptionCode
  )
    SELECT
      LTRIM(RTRIM(TractorVIN)),
      tMake,
      tModel,
      UpdatedBy,
      TODATETIMEOFFSET(UpdateDt, '-04:00'),
      TODATETIMEOFFSET(CreateDt, '-04:00'),
      CreatedBy,
      'INCOMPLETE VIN NUMBER'
    FROM #TractorCleanUp
    WHERE LEN(LTRIM(RTRIM(TractorVIN))) < 17 or TractorVIN IS NULL
    ORDER BY TractorVIN

  delete from #TractorCleanUp
  where TractorVIN is null


  UPDATE #TractorCleanUp
  SET TractorYear = vy.TractorYear
  from TELCustom.dbo.VinYear vy
  WHERE SUBSTRING(TractorVIN, 10, 1) = VINCharacter


  update #TractorCleanUp
  set TractorMakeModelID = tmm.TractorMakeModelID
  from dbo.TractorMakeModel TMM
  WHERE
    CHARINDEX(SUBSTRING(REPLACE(TModel, 'new ', ''), 1, 2), TMM.Model) > 0
    AND TMM.Make = TMake


  update #TractorCleanUp
  set Mileage =
  CAST(M.LASTRDING
       AS INTEGER)
  FROM TFW.dbo.UNITS U
    JOIN TFW.dbo.UNITMTR M ON U.UNITID = M.UNITID AND M.METERDEFID = 33 -- ECM Meter
  WHERE u.SERIALNO = TractorVIN


  UPDATE #TractorCleanUp
  set UpID = TELEscrowId
  from DBMigration.AccountMapping
  where TelCustom = UpdatedBy


  UPDATE #TractorCleanUp
  set CreID = TELEscrowId
  from DBMigration.AccountMapping
  where TelCustom = CreatedBy

  INSERT INTO #TractorCleanUpException (
    TractorVIN,
    TMake,
    TModel,
    UpdatedBy,
    UpdateDt,
    CreateDt,
    CreateedBy,
    ExecptionCode
  )
    SELECT
      TFX.TractorVIN,
      TFX.tMake,
      TFX.tModel,
      TFX.UpdatedBy,
      TFX.UpdateDt,
      TFX.CreateDt,
      TFX.CreatedBy,
      'Make and model not found'
    FROM #TractorCleanUp TFX
    where TractorMakeModelID is null


  delete #TractorCleanUp
  where TractorMakeModelID is null;

  select * from #TractorCleanUp where TractorVIN = '1FUJGLDRXDLBS1564'



  MERGE dbo.Tractor as t
  using #TractorCleanUp as s
  on t.TractorVIN = s.TractorVIN
  when not matched by target
  then insert (TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage)
  VALUES (s.TractorMakeModelID, s.TractorVIN, s.TractorYear, ISNULL(s.CreID, 1), ISNULL(S.CreateDt, GETDATE()),
          ISNULL(s.UpID, 1), ISNULL(s.UpdateDt, GETDATE()), s.Mileage)
  when matched  and s.TractorMakeModelID = t.TractorMakeModelID then
    update
    set UpdatedDate = isnull(UpdateDt, getdate()), UpdatedBy = isnull(UpID, 1)
  ;

  merge DBMigration.TractorCleanUpException as t
  using #TractorCleanUpException as s
  on t.TractorVIN = s.TractorVIN
  when not matched then
    INSERT (TractorVIN, TMake, TModel, TractorYear, Mileage, CreateDt, UpdateDt, TractorMakeModelID, UpdatedBy, CreateedBy, ExecptionCode)
    VALUES
      (TractorVIN, TMake, TModel, TractorYear, Mileage, CreateDt, UpdateDt, TractorMakeModelID, UpdatedBy, CreateedBy,
        ExecptionCode);


  drop table #TractorCleanUp

  drop table #TractorCleanUpException

end
go



CREATE  PROCEDURE DBMigration.MigrateContracts
  as begin
delete  from DBMigration.ContractExceptions

create table #Contract
(
  Ranking INTEGER,
	Contract# int not null,
	ContractStatusID int ,
  TelEscrowTractorId int,
  TVIN VARCHAR(20),
	ContractEffectiveDate datetimeoffset ,
	MaturityDate datetimeoffset ,
	ContractTerminatedDate datetimeoffset,
	RelatedContract int,
	CreatedBy varchar(50) ,
	CreatedDate datetimeoffset,
	UpdatedBy varchar(50),
	UpdatedDate datetimeoffset,
  creid INTEGER,
  UPID integer,
  isTerm BIT
)



INSERT INTO #Contract( Contract#,
                      TVIN,  ContractEffectiveDate, MaturityDate)

SELECT  con.ContractNumber, LEFT(cast(L.[Serial Number] as varchar(19)), 17),
  CON.EffectiveDate, con.ExpirationDate
FROM TELCustom.dbo.Contracts con
LEFT JOIN DBMigration.LeaseWorksAssetInfo L
ON "Contract Number" = ContractNumber



 update #Contract  set #Contract.CreatedBy = c.CreatedBy, #Contract.UpdatedBy = c.UpdatedBy, #Contract.CreatedDate = TODATETIMEOFFSET(c.CreateDate, '-04:00'), #Contract.UpdatedDate = TODATETIMEOFFSET(c.UpdateDate, '-04:00')
 from TELCustom.dbo.Contracts c where Contract# = ContractNumber

update #Contract set TVIN = left(t.VIN, 17)
  from TELCustom.dbo.Contracts con
  inner join TELCustom.dbo.Tractors t on con.TractorID = t.TractorID
  where Contract# = con.ContractNumber
  AND TVIN IS NULL



update #Contract set TelEscrowTractorId = TractorID
from dbo.Tractor where CHARINDEX(TVIN, TractorVIN) > 0
  OR CHARINDEX( TractorVIN, TVIN) > 0

update #Contract set creid = TelEscrowID
from DBMigration.AccountMapping where CreatedBy = TelCustom


update #Contract set UPID = TelEscrowID
from DBMigration.AccountMapping where UpdatedBy = TelCustom


  update #Contract set CreatedDate  = ContractEffectiveDate
where CreatedDate is null






INSERT INTO DBMigration.ContractExceptions(Ranking, ContractNumber,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, ExceptionMessage)
  SELECT Ranking, Contract#,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, 'No Vin number in telcustom OR leaseworks DB' FROM #Contract WHERE TVIN IS NULL

delete  from #Contract where TVIN IS NULL


------------------------------------------------------------------------------------------------------------------------
  Create Table #MissingTractors(
    Tmake varchar(50),
    Tmodel varchar(50),
    tvin varchar(50),
    tyear   INTEGER,
    Mileage INTEGER,
    MileageDate DATETIMEOFFSET,
    WarrantyDate  DATETIMEOFFSET,
    InServiceDate DATETIMEOFFSET,
    mmyID       INTEGER
  )

  INSERT INTO #MissingTractors(Tmake, Tmodel, tvin, tyear, Mileage, MileageDate, WarrantyDate, InServiceDate)
    select
    CASE WHEN MAKE = 'FRGHT'
      THEN 'Freightliner'
      WHEN MAKE = 'PTRBL'
        then
          'Peterbilt'
       WHEN MAKE = 'KNWRT'
        then
          'Kenworth'
       WHEN MAKE = 'INTNL'
        then
          'International'
      else MAKE
      end,
       MODEL, LEFT(SERIALNO, 17), MODELYEAR,0, SYSDATETIMEOFFSET(), WARRANTYINSERVICE, INSERVICE
  from #Contract
  left join TFW.dbo.UNITS on (
      CHARINDEX(TVIN, SERIALNO) > 0
  OR CHARINDEX( SERIALNO, TVIN) > 0
      )
  where TYPE = 'Tractor' and TelEscrowTractorId is null


update #MissingTractors set mmyID = tmm.TractorMakeModelID
from dbo.TractorMakeModel TMM WHERE
Tmake = make and charindex(Make, Tmake) > 0


  delete #MissingTractors
  where mmyID is null;

MERGE dbo.Tractor as t
  using #MissingTractors as s
on t.TractorVIN = s.tvin
when not matched by target
 then insert (TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage)
VALUES (s.mmyID, s.tvin, s.tyear, 1,sysdatetimeoffset(), 1 , sysdatetimeoffset(), s.Mileage)

;
drop table #MissingTractors
------------------------------------------------------------------------------------------------------------------------


update #Contract set TelEscrowTractorId = TractorID
from dbo.Tractor where CHARINDEX(TVIN, TractorVIN) > 0
  OR CHARINDEX( TractorVIN, TVIN) > 0


INSERT INTO DBMigration.ContractExceptions(Ranking, ContractNumber,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, ExceptionMessage)
  SELECT Ranking, Contract#,
                                ContractStatusID,
                                TelEscrowTractorId,
                                TVIN,
                                ContractEffectiveDate,
                                MaturityDate,
                                ContractTerminatedDate,
                                RelatedContract,
                                CreatedBy,
                                CreatedDate,
                                UpdatedBy,
                                UpdatedDate,
                                creid,
                                UPID, 'No Tractor id in TelEscrow DB: Check tractor exceptions table' FROM #Contract WHERE
 TelEscrowTractorId is null


DELETE  FROM #Contract WHERE
 TelEscrowTractorId is null






UPDATE #Contract SET ContractStatusID = case when isTerm = 1 and ContractTerminatedDate is not null
  then 3
  when isTerm = 1 and ContractTerminatedDate is  null
  then 2
  else 1
  end


update #Contract set MaturityDate = [Maturity Date], ContractEffectiveDate= [Commencement Date], ContractTerminatedDate = CASE WHEN [Termination Date] = '01/01/1800'
  THEN NULL
  ELSE [Termination Date]
  end
  from DBMigration.[LeaseWorksAccountInfo]
where [Contract Number] = Contract#


MERGE dbo.Contract AS T
  USING #Contract as s
on T.ContractNumber = S.Contract#
when not matched by target then
insert
(ContractNumber, ContractStatusID, TractorID,
 ContractEffectiveDate, MaturityDate,
 ContractTerminatedDate, CreatedBy, CreatedDate,
UpdatedBy, UpdatedDate, EscrowRate)
VALUES (s.Contract#, s.ContractStatusID, s.TelEscrowTractorId,
 s.ContractEffectiveDate,  s.MaturityDate,
 s.ContractTerminatedDate, isnull(s.creid,1 ),  isnull(s.CreatedDate, getdate()),
 isnull(s.UPID,1 ),  isnull(s.UpdatedDate, getdate()), 0.07)
when matched then
update set
  UpdatedDate = isnull(s.UpdatedDate, getdate()),
  UpdatedBy = isnull(s.UPID,1 ),
  ContractStatusID = s.ContractStatusID,
  ContractTerminatedDate = s.ContractTerminatedDate
;

DROP TABLE #Contract
  end
go

CREATE PROCEDURE DBMigration.MigrateDriversToOperator
  as begin

  delete from  DBMigration.TelCustomDriversException
CREATE TABLE #TelCustomDrivers (
  Ranking             INTEGER,
  FirstName           nvarchar(50)   not null,
  LastName            nvarchar(50)   not null,
  StreetAddress       nvarchar(100),
  OperatorBusinessName nvarchar(50),
  PhoneNumber         varchar(50),
  City                nvarchar(50),
  State               nvarchar(50),
  Zip                 char(5),
  CreatedBy            nvarchar(50)           ,
  CreatedDate         datetimeoffset,
  UpdatedBy            nvarchar(50)   ,
  UpdatedDate         datetimeoffset not null,
  FederalIdentificationNumber    varchar(4),
  UPid        integer,
  creid INTEGER
)




insert into #TelCustomDrivers (Ranking,
                               FirstName,
                               LastName,
                               StreetAddress,
                               OperatorBusinessName,
                               PhoneNumber,
                               City,
                               State,
                               Zip,
                               CreatedBy,
                               CreatedDate,
                               UpdatedBy,
                               UpdatedDate, FederalIdentificationNumber)
  SELECT
    ROW_NUMBER() OVER(PARTITION BY   FirstName,LastName ORDER BY CreateDate asc ),
    FirstName,
    LastName,
    Address,
    DriversCompany,
    PhoneNumber,
    City,
    State,
    Zip,
    case when CreatedBy is null
      then 'SYSTEM'
    else
    CreatedBy
      END ,
    TODATETIMEOFFSET(CreateDate, '-04:00'),
    case when UpdatedBy is null
      then 'SYSTEM'
    else
    UpdatedBy
      END ,
    TODATETIMEOFFSET(Updatedate, '-04:00'),
    right(SSN , 4)
  FROM TELCustom.dbo.Drivers
  where FirstName NOT LIKE '% and %' and FirstName not LIKE '% & %' and left(FirstName, 4) <> 'and '
        and LastName not LIKE '%and %' and LastName not LIKE '% & %'and left(LastName, 4) <> 'and '
 AND FirstName <> 'NULL' AND LastName <> 'NULL'


delete from #TelCustomDrivers where Ranking > 1



UPDATE #TelCustomDrivers SET UPid = TelEscrowID
FROM DBMigration.AccountMapping WHERE TelCustom = UpdatedBy


UPDATE #TelCustomDrivers SET creid = TelEscrowID
FROM DBMigration.AccountMapping WHERE TelCustom = CreatedBy


MERGE dbo.Operator as t
  using  #TelCustomDrivers as s
on t.FirstName = s.FirstName AND T.LastName = S.LastName
when not matched by target
then insert (FirstName, LastName, StreetAddress, OperatorBusinessName, PhoneNumber, City, Zip, State, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, FederalIdentificationNumber)
VALUES (FirstName, LastName, StreetAddress, OperatorBusinessName, PhoneNumber, City, Zip,State, isnull(creid, 1), isnull(CreatedDate, getdate()), isnull(UPid, 1), isnull(UpdatedDate, getdate()), FederalIdentificationNumber);


insert into DBMigration.TelCustomDriversException (
                               FirstName,
                               LastName,
                               StreetAddress,
                               PhoneNumber,
                               City,
                               State,
                               Zip,
                               CreatedBy,
                               CreatedDate,
                               UpdatedBy,
                               UpdatedDate, FederalIdentificationNumber, ExceptionDescription)
  SELECT
    FirstName,
    LastName,
    Address,
    PhoneNumber,
    City,
    State,
    Zip,
    case when CreatedBy is null
      then 'SYSTEM'
    else
    CreatedBy
      END ,
    TODATETIMEOFFSET(CreateDate, '-04:00'),
    case when UpdatedBy is null
      then 'SYSTEM'
    else
    UpdatedBy
      END ,
    TODATETIMEOFFSET(Updatedate, '-04:00'),
    right(SSN , 4),
    'Name format not proper. (Probably a joint contract)'
  FROM TELCustom.dbo.Drivers
  where FirstName  LIKE '% and %' or FirstName  LIKE '% & %' or left(FirstName, 4) = 'and '
        or LastName  LIKE '%and %' or LastName  LIKE '% & %'or left(LastName, 4) = 'and '
 or FirstName = 'NULL' or LastName = 'NULL'

DROP TABLE #TelCustomDrivers
END
go



create procedure DBMigration.CarrierMappingTable
  as begin
CREATE TABLE #CarrierMapping(
  telCustID integer,
  TelCustName VARCHAR(50),
  TelEscName varchar(50),
  telescID INTEGER
)

INSERT INTO #CarrierMapping(
telCustID,
  TelCustName,
  TelEscName,
  telescID
)

select  CUSTTABLEID, CUSTOMERNAME, c.name, c.CarrierID from
  TELCustom.dbo.Companies
left join dbo.Carrier c on charindex(CUSTOMERNAME, c.name)  > 0


update #CarrierMapping set telescID = CarrierID, TelEscName = name
  from dbo.Carrier
where (charindex(TelCustName, name)  > 0 or TelCustName = ShortName)
and telescID is null


update #CarrierMapping set telescID = CarrierID, TelEscName = name
 from dbo.Carrier where ShortName like '%MAINT%' and
  TelCustName like '%MAINT%'
select *
from #CarrierMapping;



INSERT INTO DBMigration.CarrierMapping(TELCUSTID, TELESC)
    SELECT telCustID, telescID FROM #CarrierMapping
WHERE TelEscName IS NOT NULL
drop table #CarrierMapping;

  end
go



CREATE PROCEDURE DBMigration.ContractCarrierUnitRelationship
as begin



  delete from DBMigration.TractorRelExceptions

  CREATE TABLE #TractorRel (
    UNIT#                  VARCHAR(580),
    Con#                   integer,
    TelEscrowCarrierNumber INTEGER,
    isTermiated            bit,
    isCurrent              bit,
    telcustCarrId          integer
  )


  CREATE TABLE #Ranked_TractorRel (
    row_id                 integer,
    UNIT#                  VARCHAR(50),
    Con#                   integer,
    TelEscrowCarrierNumber INTEGER,
    isTermiated            bit,
    isCurrent              bit,
    ConId                  INTEGER
  )

  CREATE TABLE #Ranked_TractorRelID (
    row_id                 integer,
    UNIT#                  VARCHAR(50),
    Con#                   integer,
    TelEscrowCarrierNumber INTEGER,
    isTermiated            bit,
    isCurrent              bit,
    ConId                  INTEGER
  )


  create table #current_unit_owners (
    unit      varchar(51),
    contract# INTEGER
  )


  insert into #TractorRel (
    Con#, UNIT#, TelEscrowCarrierNumber, isTermiated, isCurrent, telcustCarrId
  )
    SELECT
      ContractNumber,
      UnitNumber,
       CASE WHEN
        LEFT(UNITNUMBER, 1) = 'T' and isnumeric(substring(UNITNUMBER, 2, 4)) = 1 and len(UNITNUMBER) = 5
        then 5
      when LEFT(UNITNUMBER, 1) = 'T' and len(UNITNUMBER) = 6 and isnumeric(substring(UNITNUMBER, 2, 5)) = 1
        then 11
      when LEFT(UNITNUMBER, 2) = '11' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
           substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7)
        then 1
      when (left(UNITNUMBER, 2) = '23' or left(UNITNUMBER, 2) = '24') and len(UNITNUMBER) = 5
        then 1
      when (len(UNITNUMBER) = 4) or (left(UNITNUMBER, 3) = '159' and len(UNITNUMBER) = 6) or (
        LEFT(UNITNUMBER, 2) = '21' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
        substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) = 7)
        then 2
      when (LEFT(UNITNUMBER, 2) = '31' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
            substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) = 7)
        then 3
      when len(UNITNUMBER) = 6 and left(UNITNUMBER, 3) = '395'
        then 15
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 2)) = 1 and len(UNITNUMBER) = 4
        then 16
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 4)) = 1 and len(UNITNUMBER) = 6
        then 7
      when left(UNITNUMBER, 2) = '91'  and len(UNITNUMBER) = 5
        then 10
      end,
      IsTerminated,
      case when IsTerminated = 0
        then 1
      else 0
      end,
      Carrier
    FROM TELCustom.dbo.drivers


  insert into #TractorRel (Con#, TelEscrowCarrierNumber, UNIT#)
    SELECT
       left(CUSTOMERNAME, 8),
       CASE WHEN
        LEFT(UNITNUMBER, 1) = 'T' and isnumeric(substring(UNITNUMBER, 2, 4)) = 1 and len(UNITNUMBER) = 5
        then 5
      when LEFT(UNITNUMBER, 1) = 'T' and len(UNITNUMBER) = 6 and isnumeric(substring(UNITNUMBER, 2, 5)) = 1
        then 11
      when LEFT(UNITNUMBER, 2) = '11' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
           substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7)
        then 1
      when (left(UNITNUMBER, 2) = '23' or left(UNITNUMBER, 2) = '24') and len(UNITNUMBER) = 5
        then 1
      when (len(UNITNUMBER) = 4) or (left(UNITNUMBER, 3) = '159' and len(UNITNUMBER) = 6) or (
        LEFT(UNITNUMBER, 2) = '21' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
        substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7))
        then 2
      when (LEFT(UNITNUMBER, 2) = '31' and substring(UNITNUMBER, 3, 1) in ('7', '8', '9') and
            substring(UNITNUMBER, 4, 1) = '9' and len(UNITNUMBER) in (6, 7))
        then 3
      when len(UNITNUMBER) = 6 and left(UNITNUMBER, 3) = '395'
        then 15
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 2)) = 1 and len(UNITNUMBER) = 4
        then 16
      when isnumeric(left(UNITNUMBER, 2)) = 0 and isnumeric(right(UNITNUMBER, 4)) = 1 and len(UNITNUMBER) = 6
        then 7
      when left(UNITNUMBER, 2) = '91'  and len(UNITNUMBER) = 5
        then 10
      end
        as Carrier,
      UNITNUMBER
     FROM
      TFW.[dbo].[INVORDER] I ( NOLOCK )
      JOIN TFW.[dbo].[ORDERS] O ( NOLOCK ) ON
                                         O.[ORDERID] = I.[ORDERID] AND
                                         O.[ORDERTYPE] = 'INVOICE'
      JOIN  TFW.[dbo].[VIEW_INVOICE_TOTALS] V ( NOLOCK ) ON
                                                      V.[ORDERID] = I.[ORDERID]
      LEFT JOIN  TFW.[dbo].[UNITS] U ( NOLOCK ) ON U.UNITID = O.UNITID
      LEFT JOIN TFW.[dbo].[CUSTOMERS] C ( NOLOCK ) ON C.CUSTID = O.CUSTID
    WHERE
      I.[INVTYPE] IN ('REPAIR', 'DIRECTSALE', 'CREDIT', 'FUEL TICKET') and
     left(CUSTOMERNAME, 8) in (select cast(ContractNumber as char(12))
                           from dbo.Contract) and UNITNUMBER is not null
    GROUP BY left(CUSTOMERNAME, 8) , UNITNUMBER, UNITNUMBER


  insert into #TractorRel (Con#, TelEscrowCarrierNumber, UNIT#)
    SELECT
      Drivers.ContractNumber,
      TELESC,
      COALESCE(Payments.UnitNumber, Drivers.UnitNumber)
    FROM TELCustom.dbo.Payments
      JOIN TELCustom.dbo.Drivers ON Drivers.DriversID = Payments.DriverID
      JOIN TELCustom.dbo.PaymentTypes ON Payments.PaymentTypeID = PaymentTypes.PaymentTypeId
      left join DBMigration.CarrierMapping on Payments.CarrierID = TELESC
    WHERE ISNULL(Payments.IsRemoved, 0) = 0
          and
          FirstName NOT LIKE '% and %' and FirstName not LIKE '% & %' and left(FirstName, 4) <> 'and '
          and LastName not LIKE '%and %' and LastName not LIKE '% & %' and left(LastName, 4) <> 'and '
          AND FirstName <> 'NULL' AND LastName <> 'NULL'


  update #TractorRel
  set TelEscrowCarrierNumber = TELESC
  from DBMigration.CarrierMapping
  where TELCUSTID = telcustCarrId
        and TelEscrowCarrierNumber is null;

  insert into #Ranked_TractorRel (
    row_id, UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent
  ) select
      row_number()
      over ( partition by UNIT#, Con#, TelEscrowCarrierNumber
        order by isTermiated ),
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent
    from #TractorRel


  DELETE FROM #Ranked_TractorRel
  WHERE row_id > 1

  insert into #current_unit_owners
  (unit, contract#)
    select
      UNITNUMBER,
      left(CUSTOMERNAME, 8)
    from tfw.dbo.UNITS u
      join TFW.dbo.CUSTOMERS c on u.CUSTID = c.CUSTID
    where isnumeric(left(CUSTOMERNAME, 8) ) = 1

  update #Ranked_TractorRel
  set isTermiated = CASE
                    WHEN COALESCE(ContractTerminatedDate, MaturityDate) < getdate()
                      THEN 1
                    ELSE 0
                    END
  from dbo.Contract
  where Con# = ContractNumber


  update #Ranked_TractorRel
  set isCurrent = CASE WHEN UNIT# = UNIT
    THEN 1
                  ELSE 0
                  END
  from #current_unit_owners
  where contract# = Con#

  UPDATE #Ranked_TractorRel
  SET isCurrent = 0
  WHERE isCurrent IS NULL


  UPDATE #Ranked_TractorRel
  set ConId = c.contractID
  from dbo.Contract c
  where ContractNumber = Con#


  update #Ranked_TractorRel
  set TelEscrowCarrierNumber = CASE WHEN
        LEFT(UNIT#, 1) = 'T' and isnumeric(substring(UNIT#, 2, 4)) = 1 and len(UNIT#) = 5
        then 5
      when LEFT(UNIT#, 1) = 'T' and len(UNIT#) = 6 and isnumeric(substring(UNIT#, 2, 5)) = 1
        then 11
      when LEFT(UNIT#, 2) = '11' and substring(UNIT#, 3, 1) in ('7', '8', '9') and
           substring(UNIT#, 4, 1) = '9' and len(UNIT#) in (6, 7)
        then 1
      when (left(UNIT#, 2) = '23' or left(UNIT#, 2) = '24') and len(UNIT#) = 5
        then 1
      when (len(UNIT#) = 4) or (left(UNIT#, 3) = '159' and len(UNIT#) = 6) or (
        LEFT(UNIT#, 2) = '21' and substring(UNIT#, 3, 1) in ('7', '8', '9') and
        substring(UNIT#, 4, 1) = '9' and len(UNIT#) in (6, 7))
        then 2
      when (LEFT(UNIT#, 2) = '31' and substring(UNIT#, 3, 1) in ('7', '8', '9') and
            substring(UNIT#, 4, 1) = '9'  and len(UNIT#) in (6, 7))
        then 3
      when len(UNIT#) = 6 and left(UNIT#, 3) = '395'
        then 15
      when isnumeric(left(UNIT#, 2)) = 0 and isnumeric(right(UNIT#, 2)) = 1 and len(UNIT#) = 4
        then 16
      when isnumeric(left(UNIT#, 2)) = 0 and isnumeric(right(UNIT#, 4)) = 1 and len(UNIT#) = 6
        then 7
      when left(UNIT#, 2) = '91'  and len(UNIT#) = 5
        then 10
      end


  INSERT INTO #Ranked_TractorRelID (row_id, UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, ConId)
    SELECT
      ROW_NUMBER()
      OVER ( PARTITION BY UNIT#, ConID, TelEscrowCarrierNumber
        ORDER BY isCurrent ),
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      Conid
    FROM #Ranked_TractorRel

  DELETE FROM #Ranked_TractorRelID
  where row_id > 1


  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'Contract in Exceptions table'
    from #Ranked_TractorRelid
    where Con# in (select contractNumber
                   from DBMigration.ContractExceptions)
          and Con# is not null

  delete from #Ranked_TractorRelid
  where Con# in (select con#
                 from DBMigration.TractorRelExceptions)


  delete from #Ranked_TractorRelID
  where con# is null

  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'No unit number associated with contract'
    from #Ranked_TractorRelID
    where UNIT# is null

  delete from #Ranked_TractorRelid
  where UNIT# is null


  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'Carrier unknown'
    from #Ranked_TractorRelid
    where TelEscrowCarrierNumber is null

  delete from #Ranked_TractorRelid
  where TelEscrowCarrierNumber is null

  INSERT INTO DBMigration.TractorRelExceptions (UNIT#, Con#, TelEscrowCarrierNumber, isTermiated, isCurrent, contractID, ExceptionMessage)
    select
      UNIT#,
      Con#,
      TelEscrowCarrierNumber,
      isTermiated,
      isCurrent,
      ConId,
      'Contract to Migrated'
    from #Ranked_TractorRelid
    where ConId is null

delete from #Ranked_TractorRelID where ConId is null

  merge dbo.ContractCarrierUnitNumber as t
  using #Ranked_TractorRelid as s
  on t.UnitNumber = s.UNIT# and t.ContractID = s.ConId and t.CarrierID = s.TelEscrowCarrierNumber
  when matched then update
  set t.CurrentRelationship = s.isCurrent, UnitNumber = s.UNIT#
  when not matched
  then insert (ContractID, CarrierID, CurrentRelationship, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, UnitNumber)
  values (s.ConId, s.TelEscrowCarrierNumber, s.isCurrent, 1, sysdatetimeoffset(), 1, sysdatetimeoffset(), s.UNIT#);


  drop table #TractorRel

  drop table #Ranked_TractorRel

  drop table #current_unit_owners
  drop table #Ranked_TractorRelID
end
go

CREATE PROCEDURE DBMigration.MigrateTransactions
  as begin
IF OBJECT_ID('tempdb.dbo.#Transactions', 'U') IS NOT NULL
  BEGIN
    DROP TABLE #Transactions;
  END;


CREATE TABLE #Transactions
(
  Unit#   VARCHAR(50),
  Contract#  INT
  ,
  ssn4            INT,
  Last            VARCHAR(50)
  ,
  TransactionDate DATETIMEOFFSET
  ,
  InvoiceClosed   DATETIMEOFFSET
  ,
  InvoiceNumber   NVARCHAR(100)
  ,
  TransactionDesc VARCHAR(500)
  ,
  Comments        VARCHAR(500)
  ,
  CarrID       INT
  ,
  Amount          MONEY
  ,
  CreatedBy       NVARCHAR(100)
  ,
  CreatedOn       DATETIMEOFFSET
  ,
  UpdatedBy       NVARCHAR(100)
  ,
  UpdatedOn       DATETIMEOFFSET
  ,
  PayType         INT  ,
  RecID           INT IDENTITY,
  oPID            INTEGER,
  ConID      INTEGER,
  TransID         INTEGER,
  ccuID           INTEGER,
  upID            INTEGER,
  CreID           integer,
  MonthID           integer

);


INSERT INTO #Transactions
(Contract#,
 Unit#
  , Last,
 ssn4
  , TransactionDate
  , InvoiceClosed
  , InvoiceNumber
  , TransactionDesc
  , Comments
  , CarrID
  , Amount
  , CreatedBy
  , CreatedOn
  , UpdatedBy
  , UpdatedOn
  , PayType
)

  SELECT
    Drivers.ContractNumber,
    Invoices.UNITNUMBER,
    Drivers.LastName,
    right(Drivers.ssn, 4),
    TODATETIMEOFFSET(Invoices.InvoiceOpenedDate, '-04:00'),
    TODATETIMEOFFSET(Invoices.InvoiceClosedDate, '-04:00'),
    Invoices.InvoiceNumber,
    Invoices.InvoiceDescription,
    '',
     CASE WHEN
        LEFT(Invoices.UNITNUMBER, 1) = 'T' and isnumeric(substring(Invoices.UNITNUMBER, 2, 4)) = 1 and len(Invoices.UNITNUMBER) = 5
        then 5
      when LEFT(Invoices.UNITNUMBER, 1) = 'T' and len(Invoices.UNITNUMBER) = 6 and isnumeric(substring(Invoices.UNITNUMBER, 2, 5)) = 1
        then 11
      when LEFT(Invoices.UNITNUMBER, 2) = '11' and substring(Invoices.UNITNUMBER, 3, 1) in ('7', '8', '9') and
           substring(Invoices.UNITNUMBER, 4, 1) = '9' and len(Invoices.UNITNUMBER) in (6, 7)
        then 1
      when (left(Invoices.UNITNUMBER, 2) = '23' or left(Invoices.UNITNUMBER, 2) = '24') and len(Invoices.UNITNUMBER) = 5
        then 1
      when (len(Invoices.UNITNUMBER) = 4) or (left(Invoices.UNITNUMBER, 3) = '159' and len(Invoices.UNITNUMBER) = 6) or (
        LEFT(Invoices.UNITNUMBER, 2) = '21' and substring(Invoices.UNITNUMBER, 3, 1) in ('7', '8', '9') and
        substring(Invoices.UNITNUMBER, 4, 1) = '9' and len(Invoices.UNITNUMBER) in (6, 7))
        then 2
      when (LEFT(Invoices.UNITNUMBER, 2) = '31' and substring(Invoices.UNITNUMBER, 3, 1) in ('7', '8', '9') and
            substring(Invoices.UNITNUMBER, 4, 1) = '9' and len(Invoices.UNITNUMBER) in (6, 7))
        then 3
      when len(Invoices.UNITNUMBER) = 6 and left(Invoices.UNITNUMBER, 3) = '395'
        then 15
      when isnumeric(left(Invoices.UNITNUMBER, 2)) = 0 and isnumeric(right(Invoices.UNITNUMBER, 2)) = 1 and len(Invoices.UNITNUMBER) = 4
        then 16
      when isnumeric(left(Invoices.UNITNUMBER, 2)) = 0 and isnumeric(right(Invoices.UNITNUMBER, 4)) = 1 and len(Invoices.UNITNUMBER) = 6
        then 7
      when left(Invoices.UNITNUMBER, 2) = '91'  and len(Invoices.UNITNUMBER) = 5
        then 10
      end,
    isnull(Invoices.TotalAmount * -1,0),
    Invoices.CreatedBy,
    TODATETIMEOFFSET(Invoices.CreateDate, '-04:00'),
    Invoices.UpdatedBy,
    TODATETIMEOFFSET(Invoices.UpdateDate, '-04:00'),
    11
  FROM TELCustom.dbo.Invoices
    JOIN TELCustom.dbo.Drivers ON Drivers.DriversID = Invoices.DriverID
  WHERE ISNULL(Invoices.IsRemoved, 0) = 0 and
FirstName NOT LIKE '% and %' and FirstName not LIKE '% & %'
        and LastName not LIKE '% and %' and LastName not LIKE '% & %'
 AND FirstName <> 'NULL' AND LastName <> 'NULL'


INSERT INTO #Transactions
(Contract#
  , Last,
 ssn4,
 Unit#
  , TransactionDate
  , InvoiceClosed
  , InvoiceNumber
  , TransactionDesc
  , Comments
  , CarrID
  , Amount
  , CreatedBy
  , CreatedOn
  , UpdatedBy
  , UpdatedOn
  , PayType

)

  SELECT
    Drivers.ContractNumber,
    Drivers.LastName,
    right(drivers.SSN, 4),
    COALESCE(Payments.UnitNumber,Drivers.UnitNumber),
    Payments.WeekOfPayment,
    NULL,
    'PAYMENT'+ CAST(PaymentID AS VARCHAR(30)),
    ISNULL(PaymentTypes.PaymentTypeDescription, ''),
    ISNULL(Payments.Comments, ''),
    TELESC,
    isnull(Payments.Amount,0),
    Payments.CreatedBy,
    todatetimeoffset(Payments.CreateDate, '-04:00'),
    Payments.UpdatedBy,
    todatetimeoffset(Payments.UpdateDate, '-04:00'),
    Payments.PaymentTypeID
  FROM TELCustom.dbo.Payments
    JOIN TELCustom.dbo.Drivers ON Drivers.DriversID = Payments.DriverID
    JOIN TELCustom.dbo.PaymentTypes ON Payments.PaymentTypeID = PaymentTypes.PaymentTypeId
    left join DBMigration.CarrierMapping on Payments.CarrierID = TELCUSTID
  WHERE ISNULL(Payments.IsRemoved, 0) = 0
and
FirstName NOT LIKE '% and %' and FirstName not LIKE '% & %' and left(FirstName, 4) <> 'and '
        and LastName not LIKE '%and %' and LastName not LIKE '% & %'and left(LastName, 4) <> 'and '
 AND FirstName <> 'NULL' AND LastName <> 'NULL'






UPDATE #Transactions set  oPID = OperatorID
from dbo.Operator where ssn4 = FederalIdentificationNumber and LastName = Last


update #Transactions set ConID = t.ContractID
FROM dbo.Contract t where Contract# = t.ContractNumber



update #Transactions set CarrID =         CASE WHEN
        LEFT(Unit#, 1) = 'T' and isnumeric(substring(Unit#, 2, 4)) = 1 and len(Unit#) = 5
        then 5
      when LEFT(Unit#, 1) = 'T' and len(Unit#) = 6 and isnumeric(substring(Unit#, 2, 5)) = 1
        then 11
      when LEFT(Unit#, 2) = '11' and substring(Unit#, 3, 1) in ('7', '8', '9') and
           substring(Unit#, 4, 1) = '9' and len(Unit#) in (6, 7)
        then 1
      when (left(Unit#, 2) = '23' or left(Unit#, 2) = '24') and len(Unit#) = 5
        then 1
      when (len(Unit#) = 4) or (left(Unit#, 3) = '159' and len(Unit#) = 6) or (
        LEFT(Unit#, 2) = '21' and substring(Unit#, 3, 1) in ('7', '8', '9') and
        substring(Unit#, 4, 1) = '9' and len(Unit#) in (6, 7))
        then 2
      when (LEFT(Unit#, 2) = '31' and substring(Unit#, 3, 1) in ('7', '8', '9') and
            substring(Unit#, 4, 1) = '9' and len(Unit#) in (6, 7))
        then 3
      when len(Unit#) = 6 and left(Unit#, 3) = '395'
        then 15
      when isnumeric(left(Unit#, 2)) = 0 and isnumeric(right(Unit#, 2)) = 1 and len(Unit#) = 4
        then 16
      when isnumeric(left(Unit#, 2)) = 0 and isnumeric(right(Unit#, 4)) = 1 and len(Unit#) = 6
        then 7
      when left(Unit#, 2) = '91' and isnumeric(right(Unit#, 5)) = 1 and len(Unit#) = 8
        then 10
      end
   WHERE CarrID IS NULL




UPDATE #Transactions SET ccuID = ContractCarrierUnitNumberid
FROM ContractCarrierUnitNumber c WHERE ConID = c.ContractID and
  c.CarrierID = CarrID and Unit# = UnitNumber

update #Transactions set upID = TelEscrowID
from DBMigration.AccountMapping where UpdatedBy = TelCustom

update #Transactions set CreID = TelEscrowID
from DBMigration.AccountMapping where CreatedBy = TelCustom

UPDATE #Transactions set TransID = CASE WHEN PayType in (2, 14)
    THEN 1
    when PayType = 11
    then 2
    when PayType = 9
    then 3
    when  PayType = 12
    then 6
    else
    7
    end



update #Transactions set MonthID = MonthEndPeriodID
  from dbo.TransactionCloseOfMonth
  WHERE datepart(month, TransactionDate) = datepart(month, MonthEndPeriodDate)
and  datepart(year, TransactionDate) = datepart(year, MonthEndPeriodDate)

INSERT  INTO dbo.TransactionExceptions(TransactionTypeID,  TransactionDate, ContractNumber, PostDate, UnitNumber,  CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate,  Comments, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, MonthEndPeriodID, TFWInvoiceOrderID)
SELECT TransID, TransactionDate, Contract#, TransactionDate, Unit#, Amount,  ccuID,  InvoiceNumber, InvoiceClosed,  CASE WHEN Comments = ''
  THEN TransactionDesc
  ELSE Comments
  END,0, ISNULL(CreID, 1), isnull(CreatedOn, getdate()), ISNULL(upID, 1), isnull(UpdatedOn, getdate()), MonthID, 0
from #Transactions tr
  WHERE NOT EXISTS (
  SELECT * FROM dbo.TransactionExceptions t where t.InvoiceNumber = tr.InvoiceNumber
) and ConID is null


delete from #Transactions where ConID is null;


INSERT  INTO dbo.TransactionExceptions(TransactionTypeID,  TransactionDate, ContractNumber, PostDate, UnitNumber,  CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate,  Comments, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, MonthEndPeriodID, TFWInvoiceOrderID)
SELECT TransID, TransactionDate, Contract#, TransactionDate, Unit#, Amount,  ccuID,  InvoiceNumber, InvoiceClosed,  CASE WHEN Comments = ''
  THEN TransactionDesc
  ELSE Comments
  END,0, ISNULL(CreID, 1), isnull(CreatedOn, getdate()), ISNULL(upID, 1), isnull(UpdatedOn, getdate()), MonthID, 0
from #Transactions tr
  WHERE NOT EXISTS (
  SELECT * FROM dbo.TransactionExceptions t where t.InvoiceNumber = tr.InvoiceNumber
) and oPID is null

delete from #Transactions where oPID is null;


INSERT  INTO dbo.TransactionExceptions(TransactionTypeID,  TransactionDate, ContractNumber, PostDate, UnitNumber,  CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate,  Comments, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, MonthEndPeriodID, TFWInvoiceOrderID)
SELECT TransID, TransactionDate, Contract#, TransactionDate, Unit#, Amount,  ccuID,  InvoiceNumber, InvoiceClosed,  CASE WHEN Comments = ''
  THEN TransactionDesc
  ELSE Comments
  END,0, ISNULL(CreID, 1), isnull(CreatedOn, getdate()), ISNULL(upID, 1), isnull(UpdatedOn, getdate()), MonthID, 0
from #Transactions tr
  WHERE NOT EXISTS (
  SELECT * FROM dbo.TransactionExceptions t where t.InvoiceNumber = tr.InvoiceNumber
)  and CarrID is null

delete from #Transactions where CarrID is null;



INSERT  INTO dbo.TransactionExceptions(TransactionTypeID,  TransactionDate, ContractNumber, PostDate, UnitNumber,  CurrentTransactionAmount, ContractCarrierUnitNumberID, InvoiceNumber, InvoiceClosedDate,  Comments, IsDeleted, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, MonthEndPeriodID, TFWInvoiceOrderID)
SELECT TransID, TransactionDate, Contract#, TransactionDate, Unit#, Amount,  ccuID,  InvoiceNumber, InvoiceClosed,  CASE WHEN Comments = ''
  THEN TransactionDesc
  ELSE Comments
  END,0, ISNULL(CreID, 1), isnull(CreatedOn, getdate()), ISNULL(upID, 1), isnull(UpdatedOn, getdate()), MonthID, 0
from #Transactions tr
  WHERE NOT EXISTS (
  SELECT * FROM dbo.TransactionExceptions t where t.InvoiceNumber = tr.InvoiceNumber
)  and  ccuID is null

delete from #Transactions where ccuID is null;

UPDATE #Transactions SET upID = 1
WHERE UpdatedBy IS NULL

UPDATE #Transactions SET CreID = 1
where CreatedBy is null

  UPDATE #Transactions SET CreatedOn = sysdatetimeoffset()
  where CreatedOn is null

  update #Transactions set UpdatedOn = sysdatetimeoffset()
  where UpdatedOn is null



select Name, count(*) from #Transactions
join Carrier on CarrID = CarrierID group by name


MERGE dbo.[Transaction] AS t
USING
#Transactions as s
  on s.InvoiceNumber = t.InvoiceNumber
  when not matched then
  INSERT (TransactionTypeID,
                              TransactionDate, PostDate,
                              CurrentTransactionAmount,
                              ContractCarrierUnitNumberID,
                              InvoiceNumber,
                              InvoiceClosedDate, Comments,
                              TransactionException, IsDeleted,
                              CreatedBy, CreatedDate, UpdatedBy,
                              UpdatedDate, MonthEndPeriodID)
  VALUES (s.TransID, s.TransactionDate, s.TransactionDate, s.Amount, s.ccuID, s.InvoiceNumber,  s.InvoiceClosed,
  CASE WHEN s.Comments = ''
  THEN s.TransactionDesc
  ELSE s.Comments
  END, 0, 0, ISNULL(CreID, 1), ISNULL(CreatedOn, GETDATE()), ISNULL(upID, 1), ISNULL(UpdatedOn, GETDATE()), MonthID)
;




DROP TABLE #Transactions

end
go

CREATE procedure DBMigration.MigrateTransactionHistory
  as begin
IF OBJECT_ID('tempdb.dbo.#Transactions', 'U') IS NOT NULL
  BEGIN
    DROP TABLE #Transactions;
  END;


CREATE TABLE #Transactions
(
  Unit#   VARCHAR(50),
  Contract#  INT
  ,
  ssn4            INT,
  Last            VARCHAR(50)
  ,
  TransactionDate DATETIMEOFFSET
  ,
  InvoiceClosed   DATETIMEOFFSET
  ,
  InvoiceNum   NVARCHAR(100)
  ,
  TransactionDesc VARCHAR(500)
  ,
  Comments        VARCHAR(500)
  ,
  CarrID       INT
  ,
  Amount          MONEY
  ,
  CreatedBy       NVARCHAR(100)
  ,
  CreatedOn       DATETIMEOFFSET
  ,
  UpdatedBy       NVARCHAR(100)
  ,
  UpdatedOn       DATETIMEOFFSET
  ,
  PayType         INT  ,
  RecID           INT IDENTITY,
  oPID            INTEGER,
  ConID      INTEGER,
  TransTypeID         INTEGER,
  ccuID           INTEGER,
  upID            INTEGER,
  CreID           integer,
  TranID          integer,
  MonthID           INTEGER,

);


INSERT INTO #Transactions
(Contract#,
 Unit#
  , Last,
 ssn4
  , TransactionDate
  , InvoiceClosed
  , InvoiceNum
  , TransactionDesc
  , Comments
  , CarrID
  , Amount
  , CreatedBy
  , CreatedOn
  , UpdatedBy
  , UpdatedOn
  , PayType
)

  SELECT
    Drivers.ContractNumber,
    COALESCE(Invoices.UNITNUMBER,Drivers.UnitNumber),
    Drivers.LastName,
    right(Drivers.ssn, 4),
    TODATETIMEOFFSET(Invoices.InvoiceOpenedDate, '-04:00'),
    TODATETIMEOFFSET(Invoices.InvoiceClosedDate, '-04:00'),
    Invoices.InvoiceNumber,
    Invoices.InvoiceDescription,
    '',
     CASE WHEN
        LEFT(Invoices.UNITNUMBER, 1) = 'T' and isnumeric(substring(Invoices.UNITNUMBER, 2, 4)) = 1 and len(Invoices.UNITNUMBER) = 5
        then 5
      when LEFT(Invoices.UNITNUMBER, 1) = 'T' and len(Invoices.UNITNUMBER) = 6 and isnumeric(substring(Invoices.UNITNUMBER, 2, 5)) = 1
        then 11
      when LEFT(Invoices.UNITNUMBER, 2) = '11' and substring(Invoices.UNITNUMBER, 3, 1) in ('7', '8', '9') and
           substring(Invoices.UNITNUMBER, 4, 1) = '9' and len(Invoices.UNITNUMBER) in (6, 7)
        then 1
      when (left(Invoices.UNITNUMBER, 2) = '23' or left(Invoices.UNITNUMBER, 2) = '24') and len(Invoices.UNITNUMBER) = 5
        then 1
      when (len(Invoices.UNITNUMBER) = 4) or (left(Invoices.UNITNUMBER, 3) = '159' and len(Invoices.UNITNUMBER) = 6) or (
        LEFT(Invoices.UNITNUMBER, 2) = '21' and substring(Invoices.UNITNUMBER, 3, 1) in ('7', '8', '9') and
        substring(Invoices.UNITNUMBER, 4, 1) = '9' and len(Invoices.UNITNUMBER) in (6, 7))
        then 2
      when (LEFT(Invoices.UNITNUMBER, 2) = '31' and substring(Invoices.UNITNUMBER, 3, 1) in ('7', '8', '9') and
            substring(Invoices.UNITNUMBER, 4, 1) = '9' and len(Invoices.UNITNUMBER) in (6, 7))
        then 3
      when len(Invoices.UNITNUMBER) = 6 and left(Invoices.UNITNUMBER, 3) = '395'
        then 15
      when isnumeric(left(Invoices.UNITNUMBER, 2)) = 0 and isnumeric(right(Invoices.UNITNUMBER, 2)) = 1 and len(Invoices.UNITNUMBER) = 4
        then 16
      when isnumeric(left(Invoices.UNITNUMBER, 2)) = 0 and isnumeric(right(Invoices.UNITNUMBER, 4)) = 1 and len(Invoices.UNITNUMBER) = 6
        then 7
      when left(Invoices.UNITNUMBER, 2) = '91' and isnumeric(right(Invoices.UNITNUMBER, 5)) = 1 and len(Invoices.UNITNUMBER) = 8
        then 10
      end,
    isnull(Invoices.TotalAmount * -1,0),
    Invoices.CreatedBy,
    TODATETIMEOFFSET(Invoices.CreateDate, '-04:00'),
    Invoices.UpdatedBy,
    TODATETIMEOFFSET(Invoices.UpdateDate, '-04:00'),
    11
  FROM TELCustom.dbo.InvoiceHistory invoices
    JOIN TELCustom.dbo.Drivers ON Drivers.DriversID = Invoices.DriverID
  WHERE ISNULL(Invoices.IsRemoved, 0) = 0 and
FirstName NOT LIKE '% and %' and FirstName not LIKE '% & %'
        and LastName not LIKE '% and %' and LastName not LIKE '% & %'
 AND FirstName <> 'NULL' AND LastName <> 'NULL'








UPDATE #Transactions set  oPID = OperatorID
from dbo.Operator where ssn4 = FederalIdentificationNumber and LastName = Last


update #Transactions set ConID = t.ContractID
FROM dbo.Contract t where Contract# = t.ContractNumber



UPDATE #Transactions SET ccuID = ContractCarrierUnitNumberid
FROM ContractCarrierUnitNumber c WHERE ConID = c.ContractID and
  c.CarrierID = CarrID and Unit# = UnitNumber

update #Transactions set upID = TelEscrowID
from DBMigration.AccountMapping where UpdatedBy = TelCustom

update #Transactions set CreID = TelEscrowID
from DBMigration.AccountMapping where CreatedBy = TelCustom

UPDATE #Transactions set TransTypeID = CASE WHEN PayType in (2, 14)
    THEN 1
    when PayType = 11
    then 2
    when PayType = 9
    then 3
    when  PayType = 12
    then 6
    else
    7
    end





update #Transactions set TranID = TransactionID
from dbo.[Transaction] t where t.InvoiceNumber = InvoiceNum




delete from #Transactions where TranID is null;



UPDATE #Transactions SET upID = 1
WHERE UpdatedBy IS NULL

UPDATE #Transactions SET CreID = 1
where CreatedBy is null

  UPDATE #Transactions SET CreatedOn = sysdatetimeoffset()
  where CreatedOn is null

  update #Transactions set UpdatedOn = sysdatetimeoffset()
  where UpdatedOn is null

DELETE FROM #Transactions WHERE ccuID IS NULL


--
-- UPDATE #Transactions set MonthID = MonthEndPeriodID
--   from dbo.TransactionCloseOfMonth
--   where DATEPART(MONTH, TransactionDate) = DATEPART(MONTH, MonthEndPeriodDate)
-- AND DATEPART(YEAR, TransactionDate) = DATEPART(YEAR, MonthEndPeriodDate)



MERGE dbo.[TransactionHistory] AS t
USING
#Transactions as s
  on s.TranID = t.TransactionID and UpdatedOn = UpdatedDate and Amount = TransactionAmount
  when not matched then
  INSERT (TransactionTypeID,TransactionID,
                              TransactionDate, PostDate,
                              TransactionAmount,
                              ContractCarrierUnitNumberID,
                              InvoiceClosedDate, Comments,
                              TransactionException, IsDeleted,
                              CreatedBy, CreatedDate, UpdatedBy,
                              UpdatedDate)
  VALUES (s.TransTypeID, TranID,  s.TransactionDate, s.TransactionDate, s.Amount, s.ccuID,  s.InvoiceClosed,
  CASE WHEN s.Comments = ''
  THEN s.TransactionDesc
  ELSE s.Comments
  END, 0, 0, ISNULL(CreID, 1), ISNULL(CreatedOn, GETDATE()), ISNULL(upID, 1), ISNULL(UpdatedOn, GETDATE()));

DROP TABLE #Transactions

end
go



CREATE  procedure DBMigration.BackFillClosedDate
as begin


Declare @StartDate DateTime ;

Declare @EndDate DateTime ;

select  @StartDate = DATEADD( MONTH, DATEDIFF (MONTH, 0, MIN(WeekOfPayment) ), 0 )
  from TELCustom.dbo.Payments

set @StartDate = dateadd(month, -1 , @StartDate)
-- Last day of month


select  @EndDate = DATEADD( MONTH, DATEDIFF (MONTH, 0, max(GETDATE()) ), 0 )
  from TELCustom.dbo.Invoices;

 CREATE TABLE #CloseDates(
   CloseD8 DATE,
   intCredRate numeric(9,8),
 );

WITH DaysInMonth_CTE AS
(
  SELECT  @StartDate DateValue
  UNION ALL
  SELECT  DATEADD(month,1,  DateValue)
  FROM    DaysInMonth_CTE
  WHERE   DATEADD(month,1,  DateValue) <= @EndDate
)
insert into #CloseDates(CloseD8)
SELECT DateValue
FROM    DaysInMonth_CTE
OPTION (MAXRECURSION 0)


update #CloseDates set intCredRate = IntrestPercantageRate
from TELCustom.dbo.InterestCredits
where DATEPART(MONTH,IntrestRateDate ) = DATEPART(MONTH, CloseD8)
AND DATEPART(YEAR, IntrestRateDate) = DATEPART(YEAR, CloseD8)




MERGE dbo.TransactionCloseOfMonth AS target
  USING  #CloseDates AS SOURCE
  ON MonthEndPeriodDate = CloseD8
  WHEN NOT MATCHED
    THEN INSERT (MonthEndPeriodDate, PeriodClosed, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy, InterestCreditRate)
    VALUES (CloseD8,1, GETDATE(), 1, GETDATE(), 1, intCredRate );

  drop table #CloseDates
  end
go



CREATE PROCEDURE DBMigration.ContractOperatorMatching
  AS BEGIN
delete from DBMigration.ConOpExceptions

CREATE TABLE #OpConRel(
  ssn4 varchar(4),
  Last VARCHAR(50),
  first VARCHAR(50),
  driversLic nvarchar(50),
  tvin VARCHAR(17),
  opID integer,
  Con# integer,
  contractID integer
)


insert into  #OpConRel(
 ssn4, Last, tvin, Con#, driversLic, First
)
SELECT  right(ssn,4),lastNAME,
  CASE WHEN [Serial Number] IS NULL
    THEN TractorVIN
      else [Serial Number]
        end, ContractNumber,
 DriverLicenseNumber,
  FirstName
FROM TELCustom.dbo.drivers
    LEFT join
  DBMigration.LeaseWorksAssetInfo on "Contract Number" = ContractNumber

  DELETE from #OpConRel WHERE con# in (select ContractNumber from DBMigration.ContractExceptions)

  insert into DBMigration.ConOpExceptions(FedID#, lastname, driversLic, vin, con#, ExceptionsMessage)
    select ssn4, Last, driversLic, tvin, Con#, 'contract is null and not in contract exception'
  from #OpConRel where Con# is null


delete from #OpConRel where Con# is null


UPDATE #OpConRel set contractID = c.contractID
from dbo.Contract c where ContractNumber = Con#

UPDATE #OpConRel set opID = RTRIM(LTRIM(OperatorID))
from dbo.Operator where FirstName = First
  and Last = LastName



 insert into DBMigration.ConOpExceptions(FedID#, lastname, driversLic, vin, con#, ExceptionsMessage)
    select ssn4, Last, driversLic, tvin, Con#, 'operator not found check operator/driver exceptions'
  from #OpConRel where opID is null

delete from #OpConRel where opID is null





merge dbo.ContractOperator as t
  using #OpConRel as s
  on t.ContractID = s.contractID and t.OperatorID = s.opID
  when not matched then insert
  (contractID, OperatorID, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate)
values (s.contractID, s.opID, 1, sysdatetimeoffset(), 1, sysdatetimeoffset());


drop table #OpConRel
end
go

