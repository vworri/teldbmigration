alter PROCEDURE DBMigration.TransactionWorker
as begin
  CREATE TABLE #EscrowUnitNumbers (
    unit# nvarchar(50),
    vin#  varchar(17),
  )

  CREATE TABLE #RankedEscrowUnitNumbers (
    rowId INTEGER,
    unit# nvarchar(50),
    vin#  varchar(17),
  )


  CREATE TABLE #EscrowContractMaxDate (
    Contract# integer,
    max_date  DATE
  )

  CREATE  TABLE #EscrowUnitNumberCarriers (
    Con#             integer,
    conID            integer,
    carrier          integer,
    unit#            nvarchar(50),
    vin#             varchar(17),
    last_trans_date  DATE,
    first_trans_date DATE,
    isCurrent        BIT,
    Tractor_Make     nvarchar(50),
    Tractor_Model    nvarchar(50),
    Tractor_Year     int,
    ccuID            integer,
    TransactionCount  INTEGER,

  )


  ------------------------------------------------------------------------------------------------------------------------
  -- collect bear bones contract information
  insert into #EscrowUnitNumbers (unit#, vin#)
    select distinct
      UNITNUMBER,
      left(SERIALNUMBER, 17)
    from TELCustom.dbo.Invoices
    where SERIALNUMBER is not null

  insert into #EscrowUnitNumbers (unit#, vin#)
    select distinct
      UnitNumber,
      left(vin, 17)
    from TELCustom.dbo.Payments
    WHERE NOT EXISTS(select
                       unit#,
                       vin
                     from #EscrowUnitNumbers
                     where unit# = UnitNumber)
          and vin is not null



  insert into #RankedEscrowUnitNumbers (rowId, unit#, vin#)
    select
      row_number()
      over ( PARTITION BY unit#
        order by vin# ),
      unit#,
      vin#
    from #EscrowUnitNumbers;
  DROP TABLE #EscrowUnitNumbers;


  INSERT INTO #EscrowUnitNumberCarriers (carrier, unit#, vin#)
    select
      dbo.UnitNumberToCarrier(unit#),
      unit#,
      vin#
    from #RankedEscrowUnitNumbers
    where rowId = 1
  drop table #RankedEscrowUnitNumbers;

  update #EscrowUnitNumberCarriers
  set Con# = ContractNumber
  from TELCustom.dbo.Payments p,
    TELCustom.dbo.Contracts c
  where p.UnitNumber = unit#
        and c.ContractID = p.ContractID

UPDATE #EscrowUnitNumberCarriers SET Con# = c.CUSTOMERNAME, vin# = left(SERIALNO,17)
  FROM TFW.dbo.UNITS u,
    TFW.dbo.CUSTOMERS c   WHERE UNITNUMBER = unit#
  and c.CUSTID = u.CUSTID
  AND Con# IS NULL and isnumeric(c.CUSTOMERNAME) = 1

  update #EscrowUnitNumberCarriers
  set Con# = ContractNumber
  from TELCustom.dbo.Payments p,
    TELCustom.dbo.Drivers d
  where p.UnitNumber = unit#
        and d.DriversID = p.DriverID and Con# is null


  update #EscrowUnitNumberCarriers
  set Con# = ContractNumber
  from TELCustom.dbo.Invoices i,
    TELCustom.dbo.Drivers d
  where i.UnitNumber = unit#
        and d.DriversID = i.DriverID and Con# is null

  update #EscrowUnitNumberCarriers
  set last_trans_date = max_d8,
    first_trans_date  = min_d8
  from (select
          UnitNumber,
          min(WeekOfPayment) min_d8,
          max(WeekOfPayment) max_d8
        from TELCustom.dbo.Payments
        group by UnitNumber) p
  where p.UnitNumber = unit#

  INSERT INTO #EscrowContractMaxDate (Contract#, max_date)
    SELECT
      Con#,
      max(last_trans_date)
    FROM #EscrowUnitNumberCarriers
    GROUP BY Con#

  update #EscrowUnitNumberCarriers
  SET isCurrent = 1
  from #EscrowContractMaxDate e
  where e.Contract# = con#
        and e.max_date = last_trans_date
  update #EscrowUnitNumberCarriers
  SET isCurrent = 0
  where isCurrent is null
drop table #EscrowContractMaxDate

  ------------------------------------------------------------------------------------------------------------------------
  -- fill in tractor info
  CREATE TABLE #Tractors (
    vin#             varchar(17),
    isCurrent        BIT,
    Tractor_Make     nvarchar(50),
    Tractor_Model    nvarchar(50),
    TMMID            INTEGER,
    Tractor_Year     integer,
    Mileage          INTEGER,
    MileageD8        DATETIME,
    CreUsr            VARCHAR(25),
    UpUsr           VARCHAR(25),
    CreBy            INTEGER,
    UpBy             integer,
    CreD8            DATETIME,
    UpD8             DATETIME
  )
  INSERT INTO #Tractors (vin#)
    select distinct vin#
    from #EscrowUnitNumberCarriers

  update #Tractors
  set Tractor_Make = cast(Make as varchar(50)), Tractor_Model = cast(Model as varchar(50))
  from DBMigration.LeaseWorksAssetInfo
  where vin# = LEFT(CAST([Serial Number] AS VARCHAR(20)), 17)


  UPDATE #Tractors
  SET UpUsr = isnull(t.UpdatedBy, 'SYSTEM'), UpD8 = ISNULL(t.UpdateDate, getdate()), CreUsr = isnull(t.CreatedBy, 'SYSTEM'),
    CreD8  = t.CreateDate
  FROM TELCustom.dbo.Tractors t
  WHERE vin# = left(VIN, 17)

  UPDATE #Tractors SET  UpUsr = 'SYSTEM'
  WHERE UpUsr IS NULL ;
    UPDATE #Tractors SET  CreUsr = 'SYSTEM'
  WHERE CreUsr IS NULL ;

  UPDATE #Tractors
  SET Tractor_Year = vy.TractorYear
  from TELCustom.dbo.VinYear vy
  WHERE SUBSTRING(vin#, 10, 1) = VINCharacter


  update #Tractors
  set TMMID = tmm.TractorMakeModelID
  from dbo.TractorMakeModel TMM
  WHERE
    CHARINDEX(SUBSTRING(REPLACE(Tractor_Model, 'new ', ''), 1, 2), TMM.Model) > 0
    AND TMM.Make = Tractor_Make

  update #Tractors
  set Mileage                 =
  CAST(M.LASTRDING
       AS INTEGER), MileageD8 = LASTRDDAY
  FROM TFW.dbo.UNITS U
    JOIN TFW.dbo.UNITMTR M ON U.UNITID = M.UNITID AND M.METERDEFID = 33 -- ECM Meter
  WHERE left(u.SERIALNO, 17) = vin#


  UPDATE #Tractors
  set UpBy = TELEscrowId
  from DBMigration.AccountMapping
  where TelCustom = UpUsr


  UPDATE #Tractors
  set CreBy = TELEscrowId
  from DBMigration.AccountMapping
  where TelCustom = CreUsr


  MERGE dbo.Tractor as t
  using #Tractors as s
  on t.TractorVIN = s.vin#
  when not matched and Tractor_Year is not null and TMMID is not null and len(vin#) = 17
  then insert (TractorMakeModelID, TractorVIN, TractorYear, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, Mileage, MileageDate)
  VALUES (s.TMMID, s.vin#, s.Tractor_Year, CreBy, isnull(CreD8, getdate()),
          UpBy, isnull(UpD8, getdate()), s.Mileage, ISNULL(MileageD8, GETDATE()))
  ;


  drop TABLE #Tractors

  -----------------------------------------------------------------------
  --- Migrate Contracts

  CREATE TABLE #Contracts (
    Contract#          INTEGER,
    ContractStatusName varchar(25),
    vin             VARCHAR(17),
    ContractStatus     INTEGER,
    TractorID          INTEGER,
    Eff_D8             DATE,
    Mat_d8             DATE,
    TermDate           DATE,
    CreUsr              VARCHAR(50),
    UpUsr               varchar(50),
    CreBy              INTEGER,
    UpBy               INTEGER,
    Cre_d8             DATETIME,
    UP_D8              DATETIME
  )

  insert into #Contracts (Contract#, TractorID, vin)
    select distinct
      Con#,
      TractorID,
      vin#
    from #EscrowUnitNumberCarriers
      left join dbo.Tractor on vin# = TractorVIN

  update #Contracts
  set Eff_D8 = [Commencement Date], Mat_d8 = [Maturity Date], TermDate = CASE WHEN [Termination Date] = '01/01/1800'
    THEN NULL
                                                                         ELSE [Termination Date]
                                                                         end
  from DBMigration.LeaseWorksAccountInfo
  where Contract# = [Contract Number]


  UPDATE #Contracts
  SET UpUsr = ISNULL(t.UpdatedBy, 'System'), UP_D8 = ISNULL(t.UpdateDate, getdate()), CreUsr = isnull(t.CreatedBy, 'system'),
    Cre_d8 = t.CreateDate
  FROM TELCustom.dbo.Contracts t
  WHERE ContractNumber = Contract#


    UPDATE #Contracts SET  UpUsr = 'SYSTEM'
  WHERE UpUsr IS NULL ;
    UPDATE #Contracts SET  CreUsr = 'SYSTEM'
  WHERE CreUsr IS NULL ;

   UPDATE #Contracts
  set UpBy = TELEscrowId
  from DBMigration.AccountMapping
  where TelCustom = UpUsr


  UPDATE #Contracts
  set CreBy = TELEscrowId
  from DBMigration.AccountMapping
  where TelCustom = CreUsr

  update #Contracts
  set ContractStatusName =
  CASE WHEN
    TermDate is not null
    then 'Active'
  when DATEADD(day, 45, TermDate) < getdate()
    then 'Terminated'
  else 'Pending Final Accounting'
  end
  UPDATE #Contracts
  SET ContractStatus = ContractStatusID
  FROM dbo.ContractStatus
  WHERE Name = ContractStatusName

  MERGE dbo.Contract AS T
  USING #Contracts as s
  on T.ContractNumber = S.Contract#
  when not matched and Eff_D8 is not null and s.TractorID is not null and Mat_d8 is not null
    then
    insert
    (ContractNumber, ContractStatusID, TractorID,
     ContractEffectiveDate, MaturityDate,
     ContractTerminatedDate, CreatedBy, CreatedDate,
     UpdatedBy, UpdatedDate, EscrowRate)
    VALUES (s.Contract#, s.ContractStatus, s.TractorID,
      s.Eff_D8, s.Mat_d8,
      s.TermDate, CreBy, isnull(Cre_d8, getdate()),
      UpBy, isnull(UP_D8, getdate()), 0.07)
  ;

    MERGE DBMigration.ContractFailureDetail AS T
  USING #Contracts as s
  on T.ContractNumber = S.Contract# and s.vin = t.TractorVin
  when not matched  by target and Eff_D8 is  null or s.TractorID is  null or Mat_d8 is null
    then
    insert
    (ContractNumber, ContractStatus, TractorVin,
     ContractEffectiveDate, MaturityDate,
     ContractTerminatedDate, CreatedUser, CreatedDate,
     UpdatedUser, UpdatedDate)
    VALUES (s.Contract#, s.ContractStatusName, s.vin,
      s.Eff_D8, s.Mat_d8,
      s.TermDate, CreUsr, isnull(Cre_d8, getdate()),
      UpUsr, isnull(UP_D8, getdate()))
      when not matched by source
       then  delete ;

  drop table #Contracts
  --------------------------------------------------------------------------------------------------------------
  update #EscrowUnitNumberCarriers
  set conID = ContractID
  from dbo.Contract
  where con# = ContractNumber

  ---------------------------------------------------------------------------------------------------------
  merge dbo.ContractCarrierUnitNumber as t
  using #EscrowUnitNumberCarriers as s
  on t.UnitNumber = s.UNIT# and t.ContractID = s.ConId and t.CarrierID = s.carrier
  when not matched and s.conID is not null and carrier is not null and unit# is not null
  then insert (ContractID, CarrierID, CurrentRelationship, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, UnitNumber)
  values (s.ConId, s.carrier, s.isCurrent, 1, sysdatetimeoffset(), 1, sysdatetimeoffset(), s.UNIT#);

  UPDATE #EscrowUnitNumberCarriers
  set ccuID = ContractCarrierUnitNumberID
  from dbo.ContractCarrierUnitNumber
  where
    conID = ContractID and unit# = UnitNumber and CarrierID = carrier

  ------------------------------------------------------------------------------------------------------------------------
  -- collect all transactions
  CREATE TABLE #Transactions
  (
    Unit#           VARCHAR(50),
    Contract#       INT
    ,
    TransactionDate DATETIMEOFFSET
    ,
    InvoiceClosed   DATETIMEOFFSET
    ,
    InvoiceNumber   NVARCHAR(100)
    ,
    Comments        VARCHAR(500)
    ,
    Amount          numeric(19, 2)
    ,
    CreatedBy       NVARCHAR(100)
    ,
    CreatedOn       DATETIMEOFFSET
    ,
    UpdatedBy       NVARCHAR(100)
    ,
    UpdatedOn       DATETIMEOFFSET
    ,
    TranTypeId      integer,
    ccuID           INTEGER,
    upID            INTEGER,
    CreID           integer,
    MonthID         integer,
    TFWID           INTEGER,
    isDeleted       BIT,
    vin#            varchar(17)

  );


  INSERT INTO #Transactions (Unit#, Contract#, TransactionDate, InvoiceClosed,
                             InvoiceNumber, Comments, Amount, CreatedBy,
                             CreatedOn, UpdatedBy, UpdatedOn,
                             TranTypeId, ccuID, isDeleted, vin#)
    SELECT
      UNITNUMBER,
      e.Con#,
      TODATETIMEOFFSET(i.InvoiceOpenedDate, '-04:00'),
      TODATETIMEOFFSET(i.InvoiceClosedDate, '-04:00'),
      i.INVOICENUMBER,
      i.INVOICEDESCRIPTION,
      i.TOTALAMOUNT*-1,
      i.CREATEDBY,
      i.CREATEDATE,
      i.UPDATEDBY,
      i.UPDATEDATE,
      TransactionTypeID,
      e.ccuID,
      i.ISREMOVED,
      e.vin#
    FROM TELCustom.dbo.Invoices i
      left join #EscrowUnitNumberCarriers e on e.unit# = i.UNITNUMBER
      LEFT join dbo.TransactionType on Name = 'TMT Invoice'


  INSERT INTO #Transactions (Unit#, Contract#, TransactionDate, InvoiceClosed,
                             InvoiceNumber, Comments, Amount, CreatedBy,
                             CreatedOn, UpdatedBy, UpdatedOn,
                             TranTypeId, ccuID, isDeleted, vin#)
    SELECT
      UNITNUMBER,
      e.Con#,
      TODATETIMEOFFSET(p.WeekOfPayment, '-04:00'),
      TODATETIMEOFFSET(p.WeekOfPayment, '-04:00'),
       'PAYMENT'+ CAST(p.PaymentID AS VARCHAR(30)),
       ISNULL(PT.PaymentTypeDescription, ''),
      p.AMOUNT,
      p.CREATEDBY,
      p.CREATEDATE,
      p.UPDATEDBY,
      p.UPDATEDATE,
      TransactionTypeID,
      e.ccuID,
      p.ISREMOVED,
      vin#
    FROM TELCustom.dbo.Payments P
      left join #EscrowUnitNumberCarriers e on e.unit# = p.UNITNUMBER
       left  JOIN TELCustom.dbo.PaymentTypes pt ON p.PaymentTypeID = pt.PaymentTypeId
      left  join dbo.TransactionType on Name = pt.PaymentTypeDescription


UPDATE #Transactions SET TranTypeId = TransactionTypeID
  from dbo.TransactionType where Name = 'Escrow Deposit'
  and TranTypeId is null  and InvoiceNumber LIKE 'PAYMENT%'

  UPDATE #Transactions
  SET UPid = AccountID
  FROM DBO.Account
  WHERE AccountName = UpdatedBy

  UPDATE #Transactions
  SET CreID = AccountID
  FROM DBO.Account
  WHERE AccountName = CreatedBy

  UPDATE #Transactions
  SET upID = 1
  WHERE upID IS NULL

  UPDATE #Transactions
  SET CreID = 1
  where CreID is null

  UPDATE #Transactions
  SET CreatedOn = sysdatetimeoffset()
  where CreatedOn is null

  update #Transactions
  set UpdatedOn = sysdatetimeoffset()
  where UpdatedOn is null

  update #Transactions
  set MonthID = MonthEndPeriodID
  from dbo.TransactionCloseOfMonth
  WHERE datepart(month, TransactionDate) = datepart(month, MonthEndPeriodDate)
        and datepart(year, TransactionDate) = datepart(year, MonthEndPeriodDate)

  UPDATE #Transactions SET TFWID = ORDERID
    FROM TFW.dbo.ORDERS WHERE InvoiceNumber = ORDERNUM


  MERGE dbo.[Transaction] AS t
USING
#Transactions as s
  on s.InvoiceNumber = t.InvoiceNumber
  when not matched and s.ccuID is not null  then
  INSERT (TransactionTypeID,
                              TransactionDate, PostDate,
                              CurrentTransactionAmount,
                              ContractCarrierUnitNumberID,
                              InvoiceNumber,
                              InvoiceClosedDate, Comments,
                              CreatedBy, CreatedDate, UpdatedBy,
                              UpdatedDate, MonthEndPeriodID, isDeleted, TFWInvoiceOrderID, TransactionException)
  VALUES (s.TranTypeId, s.TransactionDate, s.TransactionDate, ISNULL(s.Amount, 0), s.ccuID, s.InvoiceNumber,  s.InvoiceClosed,
  CASE WHEN s.Comments = ''
  THEN s.Comments
  ELSE s.Comments
  END, CreID, ISNULL(CreatedOn, GETDATE()), upID, ISNULL(UpdatedOn, GETDATE()), MonthID,
  isDeleted, TFWID, 0)
;


  MERGE dbo.[TransactionExceptions] AS t
USING
#Transactions as s
  on s.InvoiceNumber = t.InvoiceNumber
  when not matched and s.ccuID is  null  then
  INSERT (TransactionTypeID,
                              TransactionDate, PostDate, TractorVIN,
                              CurrentTransactionAmount,
                              ContractCarrierUnitNumberID,
                              InvoiceNumber,
                              InvoiceClosedDate, Comments,
                              CreatedBy, CreatedDate, UpdatedBy,
                              UpdatedDate, MonthEndPeriodID, isDeleted, TFWInvoiceOrderID)
  VALUES (s.TranTypeId, s.TransactionDate,  s.TransactionDate,vin#, isnull(s.Amount, 0), s.ccuID, s.InvoiceNumber,  s.InvoiceClosed,
  CASE WHEN s.Comments = ''
  THEN s.Comments
  ELSE s.Comments
  END, CreID, ISNULL(CreatedOn, GETDATE()), upID, ISNULL(UpdatedOn, GETDATE()), MonthID,
  isDeleted, TFWID)
;

  UPDATE dbo.[TransactionExceptions] set CarrierName = Name
    from dbo.ContractCarrierUnitNumber ccu,
      dbo.Carrier c where ccu.ContractCarrierUnitNumberID = ccu.ContractCarrierUnitNumberID
  and c.CarrierID = ccu.CarrierID
------------------------------------------------------------------------------------------------------------------------
  -- Create Reporting Table

    CREATE TABLE #ContractCarrierUnitNumberMigrationReport (
      Contract#           INTEGER,
      unit#               nvarchar(50),
      carrier             NVARCHAR(100),
      vin#                varchar(17),
      Relation_Start_Date DATE,
      Relation_End_Date   DATE,
      Transaction_Count   INTEGER,
      Amount              NUMERIC(19, 2),
      Contract_Status     varchar(25),
      Tractor_Needed    Bit default 0,
      Carrier_Unknown       Bit default 0,
      Contract_Dereferenced BIT default 0,
    )

INSERT INTO #ContractCarrierUnitNumberMigrationReport(Contract#, unit#, carrier, vin#, Relation_Start_Date,
                                                      Relation_End_Date, Transaction_Count, Amount)
  select Contract#, t.unit#, Name, t.vin#, Min(TransactionDate), max(TransactionDate),
    COUNT(t.InvoiceNumber) ,
    SUM(t.Amount)
  FROM #Transactions t
  left join  dbo.Carrier on dbo.UnitNumberToCarrier(t.Unit#) = CarrierID
  group by Contract#, Unit#, vin#, Name



update #ContractCarrierUnitNumberMigrationReport set Tractor_Needed = 1
  where vin# is null
  UPDATE #ContractCarrierUnitNumberMigrationReport SET Contract_Status = Name
    FROM dbo.Contract c ,
      dbo.ContractStatus cs where Contract# = ContractNumber
   and c.ContractStatusID = cs.ContractStatusID
UPDATE #ContractCarrierUnitNumberMigrationReport set Tractor_Needed = 1
  where vin# not in (select vin#
                     from dbo.Tractor)
update #ContractCarrierUnitNumberMigrationReport set Carrier_Unknown = 1
  where carrier is null;


update #ContractCarrierUnitNumberMigrationReport set Contract_Dereferenced = 1
  where Contract# is null

delete from DBMigration.MigrationInitialReport

  INSERT INTO DBMigration.MigrationInitialReport([Contract Number], [Unit Number], [Assumed Carrier], [Vin Number], [First Transaction Date], [Last Transaction Date], [Number of Trasaction], [Sum of Transactions], [Assumed Contract Status],  [Tractor Needs Import], [Carrier Needs Mapping], [Contract Number Unknown])
    SELECT Contract#, Unit#, CARRIER, VIN#, Relation_Start_Date, Relation_End_Date, Transaction_Count, Amount, Contract_Status, Tractor_Needed, Carrier_Unknown, Contract_Dereferenced
  from #ContractCarrierUnitNumberMigrationReport

drop table #ContractCarrierUnitNumberMigrationReport

drop table #Transactions

  DROP TABLE #EscrowUnitNumberCarriers
  update DBMigration.MigrationInitialReport
    SET isCurrent = isCurrent
    from dbo.ContractCarrierUnitNumber where UnitNumber = [Unit Number]

UPDATE DBMigration.MigrationInitialReport SET IsValid = 1
from
  dbo.ContractCarrierUnitNumber ccu,
  Contract con
where UnitNumber = [Unit Number]
and con.ContractNumber = [Contract Number]
and ccu.ContractID = con.ContractID
end
go

